//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FollowTaskIt.AccesoBaseDatos
{
    using System;
    using System.Collections.Generic;
    
    public partial class MINUTA
    {
        public MINUTA()
        {
            this.TAREAS = new HashSet<TAREAS>();
        }
    
        public Nullable<int> reunionId { get; set; }
        public int minutaId { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public string agenda { get; set; }
        public string puntos_pendientes { get; set; }
        public string usuario_abrio { get; set; }
        public string nombre { get; set; }
        public bool abierta { get; set; }
        public bool Activo { get; set; }
    
        public virtual ICollection<TAREAS> TAREAS { get; set; }
        public virtual REUNION REUNION { get; set; }
    }
}
