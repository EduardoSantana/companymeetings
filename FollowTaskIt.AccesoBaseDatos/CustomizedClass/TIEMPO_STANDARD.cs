﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.AccesoBaseDatos
{
    public partial class TIEMPO_STANDARD
    {
        public SelectList DepartamentoList { get; set; }
        public SelectList EmpleadoList { get; set; }

    }
}
