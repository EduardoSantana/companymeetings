﻿using FollowTaskIt.AccesoBaseDatos;
using FollowTaskIt.Enumeradores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.AccesoBaseDatos
{
    public class ViewModelBase
    {
        private Entities db = new Entities();
        private string empleadoId { get; set; }
    
        public List<DEPARTAMENTO> listaDepartamento
        {
            get 
            {
                return db.DEPARTAMENTO.Where(p => p.Activo == true).ToList();
            }
        }
        public int cantidadServicios(string currentUserId)
        {
            if (String.IsNullOrEmpty(empleadoId))
            {
                empleadoId = db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.empleadoId).FirstOrDefault();
            }
            return db.SERVICIOS.Where(p => p.asignadoid == empleadoId && p.estadoId != ServiciosEstados.Cerrada).Count();
            
        }
        public bool accesoMantenimiento(string currentUserId)
        {
            return (db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.accesomant).FirstOrDefault());
        }
    }

}
