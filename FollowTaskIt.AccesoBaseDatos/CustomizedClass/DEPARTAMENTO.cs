﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatos;
using System.ComponentModel.DataAnnotations;

namespace FollowTaskIt.AccesoBaseDatos
{
    public partial class DEPARTAMENTO
    {
        public SelectList EstadoList { get; set; }
        public int departamentoId { get; set; }
        [Required]
        public string nombre_depto { get; set; }
        public string noticias { get; set; }
        public string empleadoid { get; set; }
        public bool Activo { get; set; }
    }
}
