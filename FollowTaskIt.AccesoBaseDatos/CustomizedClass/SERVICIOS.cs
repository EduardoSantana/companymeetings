﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace FollowTaskIt.AccesoBaseDatos
{
    public partial class SERVICIOS
    {
        public SelectList DepartamentoList { get; set; }
        public SelectList TiempoList { get; set; }
        public SelectList EstadoList { get; set; }
    
    }
}
