﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace FollowTaskIt.AccesoBaseDatos
{
    public partial class REUNION
    {
                [DisplayName("Departamento")]
        public int departamentoId { get; set; }
        public int reunionId { get; set; }
        [DisplayName("Nombre")]
        public string nombre_reunion { get; set; }
        [DisplayName("Administrador1")]
        public string usuario1 { get; set; }
        [DisplayName("Administrador2")]
        public string usuario2 { get; set; }
        [DisplayName("Administrador3")]
        public string usuario3 { get; set; }

        [DisplayName("Fecha Inicio")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> fecha_inicio { get; set; }
        [DisplayName("Fecha Final")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> fecha_final { get; set; }
        [DisplayName("Fecha Cierre")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> fecha_cierre { get; set; }
        [DisplayName("Tiempo")]
        public Nullable<int> tiempo { get; set; }
        public string password { get; set; }
        public bool usopassword { get; set; }
                [DisplayName("Estatus")]
        public int estatusId { get; set; }
                [DisplayName("Tipo")]
        public int tipoId { get; set; }


    
    }
}
