﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace FollowTaskIt.Presentacion
{
    public static class ApplicationHelpers
    {
        public static string BuildBreadcrumbNavigation(this HtmlHelper helper)
        {
            // optional condition: I didn't wanted it to show on home and account controller
            if (helper.ViewContext.RouteData.Values["controller"].ToString() == "Home" ||
                helper.ViewContext.RouteData.Values["controller"].ToString() == "Account")
            {
                return string.Empty;
            }

            StringBuilder breadcrumb = new StringBuilder();
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            
            breadcrumb.Append("<div id=\"breadcrumb\"><li> ");

            breadcrumb.Append("<a href=\"" + urlHelper.Action("Index", "Home").ToString() + "\"><span class=\"icon icon-home\"> </span> </a>");

            //breadcrumb.Append(helper.ActionLink("Home", "Index", "Home").ToHtmlString());

            breadcrumb.Append(" </li>");
            breadcrumb.Append("<li>");
            breadcrumb.Append("<a href=\"" + urlHelper.Action("Index", helper.ViewContext.RouteData.Values["controller"].ToString()).ToString() + "\"> <span class=\"icon icon-double-angle-right\"></span> " + helper.ViewContext.RouteData.Values["controller"].ToString() + " </a>");
            //breadcrumb.Append(helper.ActionLink(helper.ViewContext.RouteData.Values["controller"].ToString().Titleize(),
            //                                   "Index",
            //                                   helper.ViewContext.RouteData.Values["controller"].ToString()));
            breadcrumb.Append("</li>");

            if (helper.ViewContext.RouteData.Values["action"].ToString() != "Index")
            {
                breadcrumb.Append("<li>");
                //breadcrumb.Append(helper.ActionLink(helper.ViewContext.RouteData.Values["action"].ToString().Titleize(),
                //                                    helper.ViewContext.RouteData.Values["action"].ToString(),
                //                                    helper.ViewContext.RouteData.Values["controller"].ToString()));
                breadcrumb.Append("<a href=\"" + urlHelper.Action(helper.ViewContext.RouteData.Values["action"].ToString(), helper.ViewContext.RouteData.Values["controller"].ToString()).ToString() + "\"> <span class=\"icon icon-arrow-down\"></span> " + helper.ViewContext.RouteData.Values["action"].ToString() + " </a>");
                breadcrumb.Append("</li>");
            }

            return breadcrumb.Append("</div>").ToString();
        }
    }

}