﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FollowTaskIt.Presentacion
{
    public static class ExtencionAuditoria
    {
        // Nombre de los campos en Base de Datos
        private const string _creadoPor = "CreadoPor";
        private const string _fechaCreacion = "FechaCreacion";
        private const string _modificadoPor = "ModificadoPor";
        private const string _fechaModificacion = "FechaModificacion";
        public static bool AuditoriaCrear(this object obj, string codigoEmpleado)
        {
            bool retVal = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty(_creadoPor);
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, codigoEmpleado, null);
                    retVal = true;
                }
            }
            catch (Exception) { }
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty(_fechaCreacion);
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, DateTime.Now, null);
                    retVal = true;
                }
            }
            catch (Exception) { }

            return retVal;
        }
        public static bool AuditoriaEditar(this object obj, string codigoEmpleado)
        {
            bool retVal = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty(_modificadoPor);
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, codigoEmpleado, null);
                    retVal = true;
                }
            }
            catch (Exception) { }
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty(_fechaModificacion);
                if (myPropInfo != null)
                {
                    myPropInfo.SetValue(obj, DateTime.Now, null);
                    retVal = true;
                }
            }
            catch (Exception) { }

            bool sobreEscribirCreacion = false;
            try
            {
                System.Reflection.PropertyInfo myPropInfo = obj.GetType().GetProperty(_fechaCreacion);
                if (myPropInfo != null)
                {
                    DateTime mivalor = (DateTime)myPropInfo.GetValue(obj);
                    DateTime comparar = new DateTime(1753, 1, 1);
                    if (mivalor < comparar)
                    {
                        sobreEscribirCreacion = true;
                    }
                }
            }
            catch (Exception) { }

            if (sobreEscribirCreacion)
            {
                obj.AuditoriaCrear(codigoEmpleado);
            }

            return retVal;
        }
    }
}