﻿$(document).ready(function () {
    $('#tiempoid').change(function () {
        var typeSelected = $('#tiempoid').val();
        typeSelected = typeSelected == "" ? 0 : typeSelected;
        var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
        //alert(typeSelected);
        if (typeSelected > 0) {
            var formData = {  //Array 
                TiempoId: typeSelected
            };

            $.ajax({
                url: divRoot + "ServicesJson/GetTiempoAsignado",
                type: "POST",
                data: formData,
                success: function (data, textStatus, jqXHR) {
                    if (data != null) {

                        var fecha = data.TiempoStandard.FechaVence;
                        $('#fecha_vence').val(fecha);

                        var nombre = data.TiempoStandard.Nombre;
                        $('#asignadoid').val(nombre);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
        else {
            $('#fecha_vence').empty();
        }

    });
});

