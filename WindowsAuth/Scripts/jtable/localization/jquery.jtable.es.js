﻿/*
    jTable localization file for 'Spanish' language.
    Author: Guillermo Bisheimer
*/
(function ($) {

    $.extend(true, $.hik.jtable.prototype.options.messages, {
        serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
        loadingMessage: 'Cargando registros...',
        noDataAvailable: 'No hay datos disponibles!',
        addNewRecord: 'Nuevo',
        editRecord: 'Editar registro',
        areYouSure: '¿Está seguro?',
        deleteConfirmation: 'El registro será eliminado. ¿Está seguro?',
        save: 'Guardar',
        saving: 'Guardando',
        cancel: 'Cancelar',
        deleteText: 'Eliminar',
        deleting: 'Eliminando',
        error: 'Error',
        close: 'Cerrar',
        cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
        pagingInfo: 'Mostrando registros {0} a {1} de {2}',
        canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
        deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
        pageSizeChangeLabel: 'Registros por página',
        gotoPageLabel: 'Ir a página'
    });

})(jQuery);



var compromisosMensajes = {
    serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
    loadingMessage: 'Cargando compromisos...',
    noDataAvailable: 'No hay compromisos disponibles!',
    addNewRecord: 'Nuevo compromiso',
    editRecord: 'Editar compromiso',
    areYouSure: '¿Está seguro?',
    deleteConfirmation: 'El compromiso será eliminado. ¿Está seguro?',
    save: 'Guardar',
    saving: 'Guardando',
    cancel: 'Cancelar',
    deleteText: 'Eliminar',
    deleting: 'Eliminando',
    error: 'Error',
    close: 'Cerrar',
    cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
    pagingInfo: 'Mostrando registros {0} a {1} de {2}',
    canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
    deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
    pageSizeChangeLabel: 'Registros por página',
    gotoPageLabel: 'Ir a página'
};


var tareasPeriodicasMensajes = {
    serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
    loadingMessage: 'Cargando tareas periodicas...',
    noDataAvailable: 'No hay tareas periodicas disponibles!',
    addNewRecord: 'Nueva tarea periodica',
    editRecord: 'Editar tarea periodica',
    areYouSure: '¿Está seguro?',
    deleteConfirmation: 'La tarea periodica será eliminada. ¿Está seguro?',
    save: 'Guardar',
    saving: 'Guardando',
    cancel: 'Cancelar',
    deleteText: 'Eliminar',
    deleting: 'Eliminando',
    error: 'Error',
    close: 'Cerrar',
    cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
    pagingInfo: 'Mostrando registros {0} a {1} de {2}',
    canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
    deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
    pageSizeChangeLabel: 'Registros por página',
    gotoPageLabel: 'Ir a página'
};

var participantesMensajes = {
    serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
    loadingMessage: 'Cargando registros...',
    noDataAvailable: 'No hay datos disponibles!',
    addNewRecord: 'Nuevo Participante',
    editRecord: 'Editar registro',
    areYouSure: '¿Está seguro?',
    deleteConfirmation: 'El registro será eliminado. ¿Está seguro?',
    save: 'Guardar',
    saving: 'Guardando',
    cancel: 'Cancelar',
    deleteText: 'Eliminar',
    deleting: 'Eliminando',
    error: 'Error',
    close: 'Cerrar',
    cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
    pagingInfo: 'Mostrando registros {0} a {1} de {2}',
    canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
    deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
    pageSizeChangeLabel: 'Registros por página',
    gotoPageLabel: 'Ir a página'
};

var tareasMensajes = {
    serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
    loadingMessage: 'Cargando registros...',
    noDataAvailable: 'No hay datos disponibles!',
    addNewRecord: '',
    editRecord: 'Editar registro',
    areYouSure: '¿Está seguro?',
    deleteConfirmation: 'El registro será eliminado. ¿Está seguro?',
    save: 'Guardar',
    saving: 'Guardando',
    cancel: 'Cancelar',
    deleteText: 'Eliminar',
    deleting: 'Eliminando',
    error: 'Error',
    close: 'Cerrar',
    cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
    pagingInfo: 'Mostrando registros {0} a {1} de {2}',
    canNotDeletedRecords: 'No se puede borrar registro(s) {0} de {1}!',
    deleteProggress: 'Eliminando {0} de {1} registros, procesando...',
    pageSizeChangeLabel: 'Registros por página',
    gotoPageLabel: 'Ir a página'
};

var refechasMensajes = {
    serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
    loadingMessage: 'Cargando registros...',
    noDataAvailable: 'No hay refechas disponibles!',
    addNewRecord: '',
    editRecord: 'Editar refecha',
    areYouSure: '¿Está seguro?',
    deleteConfirmation: 'La refecha será eliminada. ¿Está seguro?',
    save: 'Guardar',
    saving: 'Guardando',
    cancel: 'Cancelar',
    deleteText: 'Eliminar',
    deleting: 'Eliminando',
    error: 'Error',
    close: 'Cerrar',
    cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
    pagingInfo: 'Mostrando registros {0} a {1} de {2}',
    canNotDeletedRecords: 'No se puede borrar refecha {0} de {1}!',
    deleteProggress: 'Eliminando {0} de {1} refechas, procesando...',
    pageSizeChangeLabel: 'Registros por página',
    gotoPageLabel: 'Ir a página'
};