﻿(function () {

    //var offsetFromTop = window.innerHeight / 2; // number of pixels of the widget should be from top of the window
    var offsetFromTop = 300; // number of pixels of the widget should be from top of the window
    var updateFrequency = 50; //milisecond. The smaller the value, smooth the animation.
    var chaseFactor = .05; // the closing-in factor. Smaller makes it smoother.

    var yMoveTo = 0;
    var yDiff = 0;

    var movingWidget = document.getElementById("divFlotanteTarea");
    movingWidget.style.position = "absolute";
    movingWidget.style.zIndex = "2";
    movingWidget.style.top = offsetFromTop.toString() + "px";
    movingWidget.style.left = "-7ex";

    function ff() {
        // compute the distance user has scrolled the window
        yDiff = (navigator.appName === "Microsoft Internet Explorer") ? (yMoveTo - document.documentElement.scrollTop) : (yMoveTo - window.pageYOffset);

        if (Math.abs(yDiff) > 9) {

            // turn off now, prevent the event repeatedly fired when user scroll repeatedly
            window.removeEventListener("scroll", ff);

            yMoveTo -= yDiff * chaseFactor;
            movingWidget.style.top = (yMoveTo + offsetFromTop).toString() + "px";
            setTimeout(ff, updateFrequency); // calls itself again
        } else {
            window.addEventListener("scroll", ff, false); // turn back on
        }
    }

    window.addEventListener("scroll", ff, false);

})();