﻿function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function isEmpty(str) {
    return (!str || 0 === str.length);
}

function cargarModal(url, numero) {

    var nombreDelModal = '#page-modal';
    if (isNumber(numero) && numero == 2) {
        nombreDelModal = '#page-modal2';
    }
 
    var $modal = $(nombreDelModal);

    if (isNumber(numero) && numero == 3) {
        $modal.addClass("anchoModal");
    }

    if (!$modal.hasClass('hide')) {
        $modal.modal('hide');
    }

    $.get(url)
        .done(function (html) {
            $modal.find('.modal-body').html(html);
            var texto = $modal.find('.titulo').text();
            $modal.find('#myModalLabel').html(texto);
            $modal.find('.titulo').hide();
            $modal.find('.titulo-hr').hide();

            //$(".hello").remove();
            //$modal.find('.region').removeClass("region");
            $modal.find('.form-actions').addClass("modal-footer");
            $modal.modal('show');
        });
}

function Redirect(url) {
    var ua = navigator.userAgent.toLowerCase(),
        isIE = ua.indexOf('msie') !== -1,
        version = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
        var link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click();
    }

        // All other browsers
    else { window.location.href = url; }
}

