﻿function MyFunctionGetServicios1() {
    {
        //$('#departmentList').change(function () {
        var typeSelected = $('#DropdowntiempoList :selected').val();
        typeSelected = typeSelected == "" ? 0 : typeSelected;
        //alert($('input[name=__RequestVerificationToken]').val());

        $.ajax({
            type: "POST",
            url: "/Servicios/GetTiempoAsignado",                           
            data: "{'TiempoId':" + typeSelected + "}", 
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        }).done(function (data) {
           
            if (data != null) {
                var Dias = data.dias;
                var today = new Date();
                var tomorrow = new Date(today);
                tomorrow.setDate(tomorrow.getDate() + +parseInt(Dias));
                var dd = tomorrow.getDate();
                var mm = tomorrow.getMonth() + 1;
                var yy = tomorrow.getFullYear();


                $('#txtnombre_depto').empty();
                $('#txtnombre_depto').val(data.depto_nombre);
                $('#txtAsignadoId').val(data.empleadoid);
                $('#txtAsignadoNombre').val(data.empleadoNombre);
                $('#txtFechaVence').val(tomorrow.getMonth());
                $('#txtFechaVence').val(dd + "/" + mm + "/" + yy);
                
            }
        }).fail(function (response) {
            if (response.status != 0) {
                alert(response.responseText);
            }
        });

    };
}
