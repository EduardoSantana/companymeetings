﻿function MyFunction() {
    {
        var typeSelected = $('#DropdowndepartmentList :selected').val();
        typeSelected = typeSelected == "" ? 0 : typeSelected;
        if (typeSelected == "" || typeSelected == "0") {
            $('#DropdowntiempoList').empty();
            $('#DropdowntiempoList').append('<option value=""> Select </option>');
            return;
        };

        //This is where the dropdownlist cascading main function
        $.ajax({
            type: "POST",
            url: "/Servicios/GetTiempoList",                            //Your Action name in the DropDownListConstroller.cs
            data: "{'Departametoid':" + typeSelected + "}",  //Parameter in this function, Is cast sensitive and also type must be string
            contentType: "application/json; charset=utf-8",
            dataType: "json"

        }).done(function (data) {
            //When succeed get data from server construct the dropdownlist here.
            if (data != null) {

                $('#DropdowntiempoList').empty();
                $('#DropdowntiempoList').append('<option value=""> Select </option>');
                $.each(data.TiempoList, function (index, data) {
                    $('#DropdowntiempoList').append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            }
        }).fail(function (response) {
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        });

    };
}