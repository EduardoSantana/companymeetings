﻿(function ($) {
    var defaultOptions = {
        validClass: 'has-success',
        errorClass: 'has-error',
        highlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group")
                .removeClass(validClass)
                .addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest(".form-group")
            .removeClass('has-error')
            .addClass(validClass);
        }
    };

    $.validator.setDefaults(defaultOptions);

    $.validator.unobtrusive.options = {
        errorClass: defaultOptions.errorClass,
        validClass: defaultOptions.validClass,
    };
})(jQuery);


$(document).ready(function () {

    var $filtro = $('div.validation-summary-errors');
    if ($filtro.find('li').is(":visible")) {
        $('div.validation-summary-errors').show();
        $filtro.addClass("alert alert-danger");
        $filtro.find('li').prepend('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>  ');
    } else {
        $('div.validation-summary-errors').hide();
    }

    var $controlInput = $(".text-box");
    $controlInput.addClass("form-control");

    var $controlInput = $(".check-box");
    $controlInput.addClass("form-control");

    var $controlInput = $("textarea");
    $controlInput.addClass("form-control");

    //var $controlInput = $(".text-box .check-box");
    //$controlInput.addClass("form-control");
    //Edit-empleadoId
});

