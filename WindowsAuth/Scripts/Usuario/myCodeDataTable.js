﻿$(function () {
    $('table.display').dataTable({
        "bLengthChange": false,
        language: {
            processing: "Procesando",
            search: "Buscar:",
            lengthMenu: "Ver _MENU_ Filas",
            info: "_START_ - _END_ de _TOTAL_ elementos",
            infoEmpty: "0 - 0 de 0 elementos",
            infoFiltered: "(Filtro de _MAX_ entradas en total)",
            infoPostFix: "",
            loadingRecords: "Cargando datos.",
            zeroRecords: "No se encontraron datos",
            emptyTable: "No hay datos disponibles",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
});

$(document).ready(function () {

    var $filtro = $('#DataTables_Table_0_filter');
    $filtro.find('input').addClass("form-control");
    $filtro.find('input').addClass("buscadorTablaTextBox");
    $filtro.find('label').addClass("buscadorTablaLabel");

});

