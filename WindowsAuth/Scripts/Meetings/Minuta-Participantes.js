﻿$.extend(true, $.hik.jtable.prototype.options, {
    dialogShowEffect: 'clip',
    dialogHideEffect: 'clip',
    jqueryuiTheme: false
});

function CargarTablaListaParticipantes(nombreDivContenedor, div) {
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var soloLectura = div.getAttribute('data-soloLectura');
    var id1 = div.getAttribute('data-id');

    $(nombreDivContenedor).jtable({
        title: 'Participantes',
        paging: false,
        sorting: false,
        messages: participantesMensajes,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        toolbar: {
            items: [{
                icon: divRoot + 'Content/images/Misc/nuevo.png',
                text: '',
                tooltip: 'Nuevo Invitado',
                click: function () {
                    cargarModal(divRoot + 'Empleados/Create/' + id1);
                }
            }]
        },
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/Listado' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&minutaId=' + id1,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                            //Poner aqui algun codigo cuando esta cargada la tabla sobre algun registro.
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            },
            deleteAction: urlBase + '/Delete?minutaId=' + id1,
            createAction: urlBase + '/Create?minutaId=' + id1,
            updateAction: urlBase + '/Update?minutaId=' + id1
        },
        fields: {
            minutaId: {
                key: true,
                create: false,
                list: false
            },
            asistencia: {
                title: 'Asistencia',
                width: '1%',
                create: false
            },
            empleadoId: {
                key: true,
                title: 'Nombre',
                width: '80%',
                create: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1;
                }
            }
        },
        recordAdded: function (event, data) {
            var empleadoId = data.record.empleadoId;
            var nombre_empleado = data.record.nombre_empleado;
            $('#empleadoId').append('<option value="' + empleadoId + '">' + nombre_empleado + '</option>');
            $('#solicitante').append('<option value="' + nombre_empleado + '">' + nombre_empleado + '</option>');
            //$(nombreDivContenedor).jtable('reload');
        },
        recordDeleted: function (event, data) {
            var empleadoId = data.record.empleadoId;
            var nombre_empleado = data.record.nombre_empleado;
            var deletedOption1 = $('.agenda-AsignarTarea option[value="' + empleadoId + '"]');
            var deletedOption2 = $('.agenda-AsignarTarea option[value="' + nombre_empleado + '"]');
            deletedOption1.remove();
            deletedOption2.remove();
        }
    });

    $(nombreDivContenedor).jtable('load');

}

function CargarTablaListaParticipantesSoloLectura(nombreDivContenedor, div) {

    var urlBase = div.getAttribute('data-url');
    var soloLectura = div.getAttribute('data-soloLectura');
    var id1 = div.getAttribute('data-id');

    $(nombreDivContenedor).jtable({
        title: 'Participantes',
        paging: false,
        sorting: false,
        messages: participantesMensajes,
        pageSize: 10,
        gotoPageArea: 'none',
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/Listado' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&minutaId=' + id1,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                            //Poner aqui algun codigo cuando esta cargada la tabla sobre algun registro.
                            if (data.TotalRecordCount > 10) {

                                //$(selector).css("-moz-user-select", "");
                                //$('.agenda-Participantes .jtable-bottom-panel').show();
                                //var $asdf = $('.agenda-Participantes .jtable-bottom-panel');
                                //$('.agenda-Participantes .jtable-bottom-panel').css("display", "");

                            }

                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            }
        },
        fields: {
            minutaId: {
                key: true,
                create: false,
                list: false
            },
            asistencia: {
                title: 'Asistencia',
                width: '1%',
                create: true
            },
            empleadoId: {
                key: true,
                title: 'Nombre',
                width: '80%',
                create: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1;
                }
            }
        }
    });

    $(nombreDivContenedor).jtable('load');

}

$(document).ready(function () {
    var nombreDivContenedor = '#divContenedorParticipantes';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var soloLectura = div.getAttribute('data-sololectura');
    if (soloLectura == "False") {
        CargarTablaListaParticipantes(nombreDivContenedor, div);
    } else {
        CargarTablaListaParticipantesSoloLectura(nombreDivContenedor, div);
    }
});
