﻿function isEmpty(str) {
    return (!str || 0 === str.length);
}

$(document).ready(function () {

    var IdBoton = '#btnInsertarTarea';
    var nombreDivContenedor = '#divContenedorTareas';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');
    var id1 = div.getAttribute('data-id');


    $("#fechaVence").click(function () {
        $(this).get(0).type = 'date';
    });


    $(IdBoton).click(function (event) {
        event.preventDefault();

        $("#frmInsert").validate({
            debug: true
        });

        var validacion = true;
        var textoTarea = $("#textoTarea").val();
        var empleadoId = $("#empleadoId").val();
        var solicitante = $("#solicitante").val();
        var fechaVence = $("#fechaVence").val();

        if (isEmpty(textoTarea) || isEmpty(empleadoId) || isEmpty(solicitante)) {
            validacion = false;
        }

        if (validacion) {
            var formData = {  //Array 
                tarea: textoTarea,
                fechaVence: fechaVence,
                empleadoId: empleadoId,
                minutaId: id1,
                solicitante: solicitante
            };

            $.ajax({
                url: urlBase + "/Create",
                type: "POST",
                data: formData,
                success: function (data, textStatus, jqXHR) {
                    $(nombreDivContenedor).jtable('reload');
                    $("#textoTarea").val("");
                    $("#fechaVence").get(0).type = 'text';
                    $("#fechaVence").val("");
                    $("#empleadoId").val("");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(nombreDivContenedor).jtable('error', errorThrown);
                }
            });

        } else {
            $(nombreDivContenedor).jtable('error', "Valor en Blanco. Por favor escribir un valor.");
        }

    });

});
