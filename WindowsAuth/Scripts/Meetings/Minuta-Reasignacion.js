﻿$(document).ready(function () {
    var nombreDivContenedor = '#divContenedorReasignacion';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var urlBase = div.getAttribute('data-url');
    CargarTablaListaReasignacion(nombreDivContenedor, div);
    btnReasignarClick(nombreDivContenedor, urlBase);
    minutaIdSeleccionChange(nombreDivContenedor, urlBase);
    $("#btnReasignar").hide();
});

function btnReasignarClick(nombreDivContenedor, urlBase) {

    $("#btnReasignar").click(function () {
        var datosArray = "";
        var $selectedRows = $(nombreDivContenedor).jtable('selectedRows');
        $selectedRows.each(function () {
            datosArray += $(this)[0].getAttribute('data-record-key') + ';';
        });
        if (datosArray.length == 0) {
            $(nombreDivContenedor).jtable('error', "No hay ninguna tarea seleccionada.");
            return false;
        }
        if (isEmpty($('#minutaIdSeleccion').val())) {
            $(nombreDivContenedor).jtable('error', "No hay ninguna minuta seleccionada.");
            return false;
        }
        if (isEmpty($('#empleadoIdHasta').val())) {
            $(nombreDivContenedor).jtable('error', "No hay ningun empleado seleccionado.");
            return false;
        }
        if (datosArray.length > 0) {
            var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
            cargarModal(divRoot + 'FollowTaskIt/ReasignacionConfirmar/?datosArray=' + datosArray + '&minutaId=' + $('#minutaIdSeleccion').val() + '&empleadoId=' + $('#empleadoIdHasta').val(), 2)
        }
    });
}

function minutaIdSeleccionChange(nombreDivContenedor, urlBase) {

    $('#minutaIdSeleccion').on('change', function () {
        var typeSelected = $('#minutaIdSeleccion :selected').val();
        typeSelected = ((typeSelected == "") ? "0" : typeSelected);
        if (typeSelected == "" || typeSelected == "0") {
            $('#empleadoIdHasta').empty();
            $('#empleadoIdHasta').append('<option value=""> Seleccionar Empleado Hasta </option>');
            return false;
        }
        LlenarCombo(urlBase, typeSelected, nombreDivContenedor);
    });

}

function LlenarCombo(urlBase, minutaId, nombreDivContenedor) {

    var formData = {  //Array 
        minutaId: minutaId
    };

    $.ajax({
        url: urlBase + "/ListaParticipantes",
        type: "POST",
        data: formData,
        success: function (data, textStatus, jqXHR) {
            if (data != null) {
                $('#empleadoIdHasta').empty();
                $('#empleadoIdHasta').append('<option value=""> Seleccionar Empleado Hasta </option>');
                $.each(data.Records, function (index, data) {
                    $('#empleadoIdHasta').append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(nombreDivContenedor).jtable('error', "Error: " + errorThrown);
        }
    });

}

function CargarTablaListaReasignacion(nombreDivContenedor, div) {

    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var dataMinutaId = div.getAttribute('data-minutaid');
    var dataReunionId = div.getAttribute('data-reunionid');
    $(nombreDivContenedor).jtable({
        selecting: true, //Enable selecting
        multiselect: true, //Allow multiple selecting
        selectingCheckboxes: true, //Show checkboxes on first column
        selectOnRowClick: false, //Enable this to only select using checkboxes
        messages: tareasMensajes,
        //title: '&nbsp;',
        title: 'Reasignacion de Tareas ',
        actions: {
            listAction: urlBase + '/Listado?soloNoRealizadas=true&minutaId=' + dataMinutaId
        },
        fields: {
            empleadoId: {
                title: 'Asignado a',
                width: '97%',
                create: true,
                edit: true,
                list: false,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                }
            },
            solicitante: {
                title: 'Solicitante',
                create: true,
                list: false,
                edit: false,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true&todosEmpleados=true&todosEmpleados=true';
                }
            },
            tareaRecId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            tarea: {
                title: 'Tarea',
                listClass: 'anchoTarea',
                width: '73%',
                create: true,
                edit: true,
                type: 'textarea'
            },
            Notas: {
                title: 'Notas',
                list: false,
                create: false,
                edit: true,
                type: 'textarea'
            },
            fecha_asignada: {
                title: 'Asignada',
                listClass: 'anchoFechaVence',
                width: '13%',
                create: false,
                edit: false,
                type: 'date'
            },
            fecha_vence: {
                listClass: 'anchoFechaVence idFechaVence',
                title: 'Vence',
                width: '13%',
                create: false,
                edit: false,
                type: 'date'
            },
            ccColorTarea: {
                visibility: 'hidden',
                create: false,
                edit: false,
                display: function (data) {
                    if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                        return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                    } else {
                        return "";
                    }
                }
            },
            Dias: {
                title: 'Dias',
                list: false,
                create: false,
                edit: true,
                input: function (data) {
                    if (data.record) {
                        return '<input type="text" id="Edit-Dias" name="Dias" value="' + getDiferenciaDias(data.record.fecha_asignada, data.record.fecha_vence) + '" />';
                    }
                }
            },
            hora_ini: {
                title: 'Hora Inicio',
                list: false,
                create: false,
                edit: true,
                input: function (data) {
                    if (data.record) {
                        return '<input type="text" id="Edit-hora_ini" class="horaPick" name="hora_ini" value="' + data.record.hora_ini + '" />';
                    }
                }
            },
            hora_fin: {
                title: 'Hora Inicio',
                list: false,
                create: false,
                edit: true,
                input: function (data) {
                    if (data.record) {
                        return '<input type="text" id="Edit-hora_fin" class="horaPick" name="hora_fin" value="' + data.record.hora_fin + '" />';
                    }
                }
            },
            Realizada: {
                title: 'Realizada',
                visibility: 'hidden',
                create: false,
                edit: true,
                type: 'checkbox',
                values: { 'false': 'No', 'true': 'Si' },
                setOnTextClick: true,
                display: function (data) {
                    return '<div id="RealizadaID" data-realizada="' + data.record.Realizada + '">' + data.record.Realizada + '</div>';
                }
            },
            estatusId: {
                visibility: 'hidden',
                create: false,
                edit: false,
                display: function (data) {
                    return '<div id="estatusId" data-estatusId="' + getEstatusId(data.record) + '">' + getEstatusId(data.record) + '</div>';
                }
            }
        },
        rowInserted: function (event, data) {
            var $divColorFila = data.row.find("#ColorFila");
            if ($divColorFila.length > 0) {
                var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                data.row.css("color", valorColor);
            }
            if (data.record.Realizada) {
                data.row.css("color", "blue");
                data.row.addClass("clRealizada");

            } else {
                if (data.row.hasClass("clRealizada")) {
                    data.row.removeClass("clRealizada");
                }
                var $divEstatusId = data.row.find("#estatusId");
                if ($divEstatusId.length > 0) {
                    var valorEstatusId = $divEstatusId[0].attributes.getNamedItem("data-estatusId").value;
                    if (valorEstatusId == 2) {
                        data.row.css("background-color", '#ffe6e6');
                        data.row.css("border-color", '#ffe6e6');
                    }
                    if (valorEstatusId == 5) {
                        data.row.css("background-color", '#FFFFA3');
                        data.row.css("border-color", '#FFFFA3');
                    }
                }
            }
        }

    });

    $('#empleadoIdDesde').on('change', function () {

        if (!isEmpty($('#empleadoIdDesde').val())) {
            $(nombreDivContenedor).jtable('load', { empleadoId: $('#empleadoIdDesde').val() });
            $("#btnReasignar").show();
        }

    });
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getEstatusId(record) {
    var dateString1 = record.fecha_vence;
    var retVal = 0;
    if (record.Realizada) {
        retVal = 4;
    }
    else {
        var fechaAhora = new Date();
        if (isEmpty(dateString1) || isBlank(dateString1)) {
            retVal = 1;
        }
        else {
            if (record.tareaperiodicaId != null && record.tareaperiodicaId > 0) {
                retVal = 5;
            }
            var date1 = new Date();
            if (dateString1.indexOf('Date') >= 0) { //Format: /Date(1320259705710)/
                date1 = new Date(
                    parseInt(dateString1.substr(6), 10)
                );
            }
            //if (date1 > fechaAhora) {
            //    retVal = 3;
            //}
            if (date1 <= fechaAhora) {
                retVal = 2;
            }
        }
    }
    return retVal;
}
