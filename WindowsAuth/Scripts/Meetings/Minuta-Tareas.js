﻿function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getQueryString(item) {
    var retVal = '';
    var $_GET = getQueryParams(document.location.search);
    var collectSearchString = $_GET[item];
    if (!isBlank(collectSearchString)) {
        retVal = collectSearchString;
    }
    return retVal;
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

function escapeRegex(value) {
    return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
}

function BuscarEnTablaCliente(IdBoton) {

    $(IdBoton).keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            var nombreDivContenedor = '#divContenedorTareas';
            // Bloque para buscar en Servidor.
            var div = document.getElementById(nombreDivContenedor.replace('#', ''));
            // Milliseconds
            var delay = 0;
            // Id Minuta 
            var id1 = div.getAttribute('data-id');
            //your code to be executed after 1 seconds
            $(nombreDivContenedor).jtable('load', {
                minutaId: id1,
                searchString: $(IdBoton).val()
            });
            // Termina Bloque para buscar en servidor.
        }

    });

}

function AjustesDeTabla() {
    var nombreDivContenedor = '#divContenedorTareas';
    var IdBoton = '#search-table';
    $(nombreDivContenedor + " .jtable-toolbar").prepend('<input id="' + IdBoton.replace("#", "") + '" type="search" placeholder="Búsqueda..." style="margin: 0px; margin-right: 0px;" >');
    BuscarEnTablaCliente(IdBoton);
}

function AjustesDeTabla_CheckBox(nombreDivContenedor) {

    var nombreSelector = nombreDivContenedor + " .jtable-toolbar";
    var Id_Check1 = 'Id_Check_Realizadas';
    var objLabel1 = '<label for="' + Id_Check1 + '" style="margin-left: 3px; margin-bottom: 1px; margin-right: 3px;" >Realizadas</label>';
    var objCheck1 = '<input  id="' + Id_Check1 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel1);
    $(nombreSelector).prepend(objCheck1);

    var Id_Check2 = 'Id_Check_NoVencidas';
    var objLabel2 = '<label for="' + Id_Check2 + '" style="margin-left: 3px; margin-bottom: 1px;" >No Vencidas</label>';
    var objCheck2 = '<input  id="' + Id_Check2 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel2);
    $(nombreSelector).prepend(objCheck2);

    var Id_Check3 = 'Id_Check_Vencidas';
    var objLabel3 = '<label for="' + Id_Check3 + '" style="margin-left: 3px; margin-bottom: 1px;" >Vencidas</label>';
    var objCheck3 = '<input  id="' + Id_Check3 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel3);
    $(nombreSelector).prepend(objCheck3);

    var Id_Check4 = 'Id_Check_SinFecha';
    var objLabel4 = '<label for="' + Id_Check4 + '" style="margin-left: 3px; margin-bottom: 1px;" >Sin Fecha</label>';
    var objCheck4 = '<input  id="' + Id_Check4 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel4);
    $(nombreSelector).prepend(objCheck4);

}

function CargarTablaListaTareas(nombreDivContenedor, div) {
    var puedeCambiarSeleccion = false;
    var tipoHacer = 1;
    var dataChildTableRefechas;
    var dataChildTableTareas;
    var datosArray = [];
    var contadorRows = 0;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var currentUser = div.getAttribute('data-currentuser');
    var id1 = div.getAttribute('data-id');

    $(nombreDivContenedor).jtable({
        title: '&nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        toolbar: {
            items: [{
                icon: divRoot + 'Content/images/Misc/excel.png',
                text: '',
                click: function () {
                    window.location = divRoot + "Reportes/TareasExcel";
                }
            }, {
                icon: divRoot + 'Content/images/Misc/pdf.png',
                text: '',
                click: function () {
                    window.location = divRoot + "Reportes/TareasPDF";
                }
            }]
        },
        gotoPageArea: 'none',
        messages: tareasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/ListadoEmpleados' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&minutaId=' + id1,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            },
            createAction: urlBase + '/Create?minutaId=' + id1 + '&enviarCodigo=true'
        },
        fields: {
            Tareas: {
                title: '',
                listClass: 'anchoChil',
                width: '3%',
                sorting: false,
                edit: false,
                create: false,
                display: function (tareaData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Ver Tareas" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        var estaAbierto1 = $(nombreDivContenedor).jtable('isChildRowOpen', $img.closest('tr'));
                        if (estaAbierto1) {
                            $(nombreDivContenedor).jtable('closeChildTable', $img.closest('tr'));
                        } else {
                            $(nombreDivContenedor).jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        selecting: true, //Enable selecting
                                        multiselect: true, //Allow multiple selecting
                                        selectingCheckboxes: true, //Show checkboxes on first column
                                        selectOnRowClick: false, //Enable this to only select using checkboxes
                                        messages: tareasMensajes,
                                        //title: '&nbsp;',
                                        //title: 'Tareas Asignadas a ' + tareaData.record.nombre,
                                        actions: {
                                            listAction: urlBase + '/Listado?empleadoId=' + tareaData.record.empleadoId + '&minutaId=' + id1,
                                            createAction: urlBase + '/Create?minutaId=' + id1,
                                            updateAction: urlBase + '/Update',
                                            deleteAction: urlBase + '/Delete'
                                        },
                                        fields: {
                                            empleadoId: {
                                                title: 'Asignado a',
                                                width: '97%',
                                                create: true,
                                                edit: true,
                                                list: false,
                                                options: function (data) {
                                                    if (data.source == 'list') {
                                                        //Return url all options for optimization. 
                                                        return urlBase + '/GetEmpleadosOptions';
                                                    }
                                                    data.clearCache();
                                                    //This code runs when user opens edit/create form to create combobox.
                                                    //data.source == 'edit' || data.source == 'create'
                                                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                                                }
                                                //,defaultValue: tareaData.record.empleadoId
                                            },
                                            solicitante: {
                                                title: 'Solicitante',
                                                create: true,
                                                list: false,
                                                edit: false,
                                                options: function (data) {
                                                    if (data.source == 'list') {
                                                        //Return url all options for optimization. 
                                                        return urlBase + '/GetEmpleadosOptions';
                                                    }
                                                    data.clearCache();
                                                    //This code runs when user opens edit/create form to create combobox.
                                                    //data.source == 'edit' || data.source == 'create'
                                                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true&todosEmpleados=true&todosEmpleados=true';
                                                }
                                            },
                                            tareaRecId: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            Refechas: {
                                                title: '',
                                                listClass: 'anchoChil',
                                                width: '1%',
                                                sorting: false,
                                                edit: false,
                                                create: false,
                                                display: function (refechasData) {
                                                    //Create an image that will be used to open child table
                                                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Refechas" />');
                                                    //Open child table when user clicks the image
                                                    $img.click(function () {

                                                        var estaAbierto2 = $(nombreDivContenedor).jtable('isChildRowOpen', $img.closest('tr'));

                                                        if (estaAbierto2) {
                                                            $(nombreDivContenedor).jtable('closeChildTable', $img.closest('tr'));
                                                        } else {
                                                            $(nombreDivContenedor).jtable('openChildTable',
                                                                $img.closest('tr'),
                                                                {
                                                                    //selecting: true, //Enable selecting
                                                                    //multiselect: true, //Allow multiple selecting
                                                                    //selectingCheckboxes: true, //Show checkboxes on first column
                                                                    //selectOnRowClick: false, //Enable this to only select using checkboxes
                                                                    messages: refechasMensajes,
                                                                    tableId: 'refechaTable',
                                                                    toolbar: {
                                                                        items: [{
                                                                            icon: divRoot + 'Content/images/Misc/eliminar.png',
                                                                            text: '',
                                                                            tooltip: 'Eliminar Todas Las Refechas',
                                                                            click: function () {
                                                                                //perform your custom job...
                                                                                if (tipoHacer == 0) {
                                                                                    var $selectedRows = dataChildTableRefechas.jtable('selectedRows');
                                                                                    dataChildTableRefechas.jtable('deleteRows', $selectedRows);
                                                                                }
                                                                                if (tipoHacer == 1) {
                                                                                    for (index = 0; index < datosArray.length; index++) {
                                                                                        var valorKey = datosArray[index][0].attributes.getNamedItem("data-record-key").value;
                                                                                        dataChildTableRefechas.jtable('deleteRecord', { key: valorKey });
                                                                                    }
                                                                                }
                                                                            }
                                                                        }]
                                                                    },
                                                                    title: 'Refechas' + '<div id="VerComoQueda"></div>',
                                                                    actions: {
                                                                        listAction: urlBase + '/ListadoRefechas?tareaRecId=' + refechasData.record.tareaRecId,
                                                                        createAction: urlBase + '/CreateRefecha?minutaId=' + id1,
                                                                        updateAction: urlBase + '/UpdateRefecha',
                                                                        deleteAction: urlBase + '/DeleteRefecha'

                                                                    },
                                                                    fields: {
                                                                        tareaRecId: {
                                                                            type: 'hidden',
                                                                            defaultValue: refechasData.record.tareaRecId
                                                                        },
                                                                        refechaId: {
                                                                            key: true,
                                                                            create: false,
                                                                            edit: false,
                                                                            list: false
                                                                        },
                                                                        activa: {
                                                                            visibility: 'hidden',
                                                                            title: 'activa',
                                                                            create: false,
                                                                            edit: false,
                                                                            display: function (data) {
                                                                                if (data.record.activa)
                                                                                {
                                                                                    return '<div id="PuedeEditar">' + data.record.activa + '</div>';
                                                                                }
                                                                                else
                                                                                {
                                                                                    return '<div id="NoPuedeEditar">' + data.record.activa + '</div>';
                                                                                }
                                                                            }
                                                                        },
                                                                        Fecha: {
                                                                            title: 'Refecha',
                                                                            create: true,
                                                                            edit: true,
                                                                            list: true,
                                                                            type: 'date',
                                                                            listClass: 'RefechaClase'
                                                                        },
                                                                        ccColorTarea: {
                                                                            visibility: 'hidden',
                                                                            create: false,
                                                                            edit: false,
                                                                            display: function (data) {
                                                                                if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                                                                                    return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                                                                                } else {
                                                                                    return "";
                                                                                }
                                                                            }
                                                                        }
                                                                    },
                                                                    rowInserted: function (event, data) {
                                                                        datosArray[contadorRows] = data.row;
                                                                        //if (data.row.find("#NoPuedeEditar").length > 0) {
                                                                        //    data.row.find(".jtable-command-column").css("display", "none");
                                                                        //}
                                                                        contadorRows += 1;
                                                                        var $divColorFila = data.row.find("#ColorFila");
                                                                        if ($divColorFila.length > 0) {
                                                                            var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                                                            data.row.css("color", valorColor);
                                                                        }
                                                                    },
                                                                    recordDeleted: function (event, data) {
                                                                        var currentRow = dataChildTableTareas.jtable('getRowByKey', refechasData.record.tareaRecId);
                                                                        currentRow.css("color", data.serverResponse.Color);
                                                                        var $tdFechaVence = currentRow.find(".idFechaVence");
                                                                        if ($tdFechaVence.length > 0) {
                                                                            $tdFechaVence.text(data.serverResponse.FechaVence);
                                                                        }
                                                                    },
                                                                    rowsRemoved: function (event, data) {
                                                                        var delay = 1100; // milliseconds
                                                                        //setTimeout(function () {
                                                                        //    dataChildTableRefechas.jtable('reload');
                                                                        //}, delay);
                                                                    },
                                                                    recordAdded: function (event, data) {
                                                                        var currentRow = dataChildTableTareas.jtable('getRowByKey', refechasData.record.tareaRecId);
                                                                        currentRow.css("color", data.serverResponse.Color);
                                                                        var $tdFechaVence = currentRow.find(".idFechaVence");
                                                                        if ($tdFechaVence.length > 0) {
                                                                            $tdFechaVence.text(data.serverResponse.FechaVence);
                                                                        }
                                                                        //var delay = 1100; // milliseconds
                                                                        //setTimeout(function () {
                                                                        //    dataChildTableRefechas.jtable('load', { tareaRecId: refechasData.record.tareaRecId });
                                                                        //}, delay);
                                                                    }
                                                                }, function (data) { //opened handler
                                                                    contadorRows = 0;
                                                                    datosArray = [];
                                                                    data.childTable.jtable('load');
                                                                    dataChildTableRefechas = data.childTable;
                                                                });
                                                        }

                                                    });
                                                    //Return image to show on the person row
                                                    return $img;
                                                }
                                            },
                                            tarea: {
                                                title: 'Tarea',
                                                listClass: 'anchoTarea',
                                                width: '73%',
                                                create: true,
                                                edit: true,
                                                type: 'textarea'
                                            },
                                            Notas: {
                                                title: 'Notas',
                                                list: false,
                                                create: false,
                                                edit: true,
                                                type: 'textarea'
                                            },
                                            fecha_asignada: {
                                                title: 'Asignada',
                                                listClass: 'anchoFechaVence',
                                                width: '13%',
                                                create: false,
                                                edit: false,
                                                type: 'date'
                                            },
                                            fecha_vence: {
                                                listClass: 'anchoFechaVence idFechaVence',
                                                title: 'Vence',
                                                width: '13%',
                                                create: false,
                                                edit: false,
                                                type: 'date'
                                            },
                                            botonCrearRefecha: {
                                                listClass: 'anchoChil',
                                                title: '',
                                                create: false,
                                                edit: false,
                                                display: function (data) {
                                                    return '<img src="' + divRoot + 'Scripts/jtable/themes/lightcolor/add.png" onclick="' + "llamadaBotonAgregar('" + data.record.tareaRecId + "');" + '" title="Crear nueva refecha" />';
                                                }
                                            },
                                            botonEliminarRefechas: {
                                                listClass: 'anchoChil',
                                                title: '',
                                                create: false,
                                                edit: false,
                                                display: function (data) {
                                                    return '<img src="' + divRoot + 'Content/images/Misc/eliminar.png" onclick="' + "cargarModal('" + divRoot + "FollowTaskIt/EliminarRefechas/" + data.record.tareaRecId + "')" + '" title="Eliminar Todas Las Refechas" />';
                                                    //onclick="cargarModal('@Url.Action("NuevaMinuta", "FollowTaskIt", new { @id = Model.minuta.minutaId })')"
                                                }
                                            },
                                            ccColorTarea: {
                                                visibility: 'hidden',
                                                create: false,
                                                edit: false,
                                                display: function (data) {
                                                    if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                                                        return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                                                    } else {
                                                        return "";
                                                    }
                                                }
                                            },
                                            Dias: {
                                                title: 'Dias',
                                                list: false,
                                                create: false,
                                                edit: true,
                                                input: function (data) {
                                                    if (data.record) {
                                                        return '<input type="text" id="Edit-Dias" name="Dias" value="' + getDiferenciaDias(data.record.fecha_asignada, data.record.fecha_vence) + '" />';
                                                    }
                                                }
                                            },
                                            hora_ini: {
                                                title: 'Hora Inicio',
                                                list: false,
                                                create: false,
                                                edit: true,
                                                input: function (data) {
                                                    if (data.record) {
                                                        return '<input type="text" id="Edit-hora_ini" class="horaPick" name="hora_ini" value="' + data.record.hora_ini + '" />';
                                                    }
                                                }
                                            },
                                            hora_fin: {
                                                title: 'Hora Inicio',
                                                list: false,
                                                create: false,
                                                edit: true,
                                                input: function (data) {
                                                    if (data.record) {
                                                        return '<input type="text" id="Edit-hora_fin" class="horaPick" name="hora_fin" value="' + data.record.hora_fin + '" />';
                                                    }
                                                }
                                            },
                                            Realizada: {
                                                title: 'Realizada',
                                                visibility: 'hidden',
                                                create: false,
                                                edit: true,
                                                type: 'checkbox',
                                                values: { 'false': 'No', 'true': 'Si' },
                                                setOnTextClick: true,
                                                display: function (data) {
                                                    return '<div id="RealizadaID" data-realizada="' + data.record.Realizada + '">' + data.record.Realizada + '</div>';
                                                }
                                            },
                                            estatusId: {
                                                visibility: 'hidden',
                                                create: false,
                                                edit: false,
                                                display: function (data) {
                                                    return '<div id="estatusId" data-estatusId="' + getEstatusId(data.record) + '">' + getEstatusId(data.record) + '</div>';
                                                }
                                            }
                                        },
                                        //Register to selectionChanged event to hanlde events
                                        selectionChanged: function () {
                                            // Desactivar todas las realizadas.
                                            if (puedeCambiarSeleccion) {
                                                var tableRows = dataChildTableTareas.find('tr.jtable-data-row');
                                                tableRows.each(function () {
                                                    var estaRealizada = $(this).hasClass("clRealizada");
                                                    var estaMarcada = $(this).hasClass("jtable-row-selected");
                                                    var tareaRecId = $(this).attr("data-record-key");
                                                    if (estaRealizada && !estaMarcada) {
                                                        MarcarRealizada(urlBase, tareaRecId, false, $(this));
                                                        $(this).removeClass("clRealizada");
                                                        var $divColorFila = $(this).find("#ColorFila");
                                                        if ($divColorFila.length > 0) {
                                                            var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                                            $(this).css("color", valorColor);
                                                        }
                                                    }
                                                    if (!estaRealizada && estaMarcada) {
                                                        MarcarRealizada(urlBase, tareaRecId, true, $(this));
                                                        $(this).addClass("clRealizada");
                                                        $(this).css("color", "blue");
                                                    }
                                                });
                                            }
                                        },
                                        rowInserted: function (event, data) {
                                            
                                            var $divColorFila = data.row.find("#ColorFila");
                                            if ($divColorFila.length > 0) {
                                                var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                                data.row.css("color", valorColor);
                                            }
                                            if (data.record.Realizada) {
                                                data.row.css("color", "blue");
                                                data.row.addClass("clRealizada");
                                                dataChildTableTareas.jtable('selectRows', data.row);
                                            } else {
                                                if (data.row.hasClass("clRealizada")) {
                                                    data.row.removeClass("clRealizada");
                                                }
                                                var $divEstatusId = data.row.find("#estatusId");
                                                if ($divEstatusId.length > 0) {
                                                    var valorEstatusId = $divEstatusId[0].attributes.getNamedItem("data-estatusId").value;
                                                    if (valorEstatusId == 2) {
                                                        data.row.css("background-color", '#ffe6e6');
                                                        data.row.css("border-color", '#ffe6e6');
                                                    }
                                                    if (valorEstatusId == 5) {
                                                        data.row.css("background-color", '#FFFFA3');
                                                        data.row.css("border-color", '#FFFFA3');
                                                    }
                                                }
                                            }
                                        },
                                        recordsLoaded: function (event, data) {
                                            puedeCambiarSeleccion = true;
                                        },
                                        recordUpdated: function (event, data) {
                                            $(nombreDivContenedor).jtable('reload');
                                        },
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load', {
                                            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
                                            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
                                            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
                                            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked")
                                        });

                                        //data.childTable.jtable('load');
                                        puedeCambiarSeleccion = false;
                                        dataChildTableTareas = data.childTable;
                                    });
                        }

                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            empleadoId: {
                title: 'Asignado a',
                width: '97%',
                create: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                }

            },
            solicitante: {
                title: 'Solicitante',
                create: true,
                list: false,
                defaultValue: currentUser,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true&todosEmpleados=true';
                }
            },
            tarea: {
                width: '0%',
                title: 'Tarea',
                create: true,
                edit: false,
                list: false,
                type: 'textarea'
            },
            fechaVence: {
                width: '0%',
                title: 'Fecha Vence',
                create: true,
                edit: false,
                list: false,
                type: 'date'
            }
        },
        recordAdded: function (event, data) {
            $(nombreDivContenedor).jtable('reload');
        }
    });

    AjustesDeTabla_CheckBox(nombreDivContenedor);

    $('#Id_Check_Realizadas').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_Realizadas1) {
            $('#Id_Check_NoVencidas').prop('checked', false);
            $('#Id_Check_Vencidas').prop('checked', false);
            $('#Id_Check_SinFecha').prop('checked', false);
        }
    });

    $('#Id_Check_NoVencidas, #Id_Check_Vencidas, #Id_Check_SinFecha').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_NoVencidas1 || Id_Check_Vencidas1 || Id_Check_SinFecha1) {
            $('#Id_Check_Realizadas').prop('checked', false);
        }
    });

    $('.table-update-trigger').on('change', function () {

        $(nombreDivContenedor).jtable('load', {
            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked")
        });

    });

    $(nombreDivContenedor).jtable('load');
}

function llamadaBotonAgregar(tareaRecId) {
    var nombreDivContenedor = "#divContenedorTareas_Oculto";
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    CargarTablaRefechasOculta(nombreDivContenedor, div, tareaRecId);
    $(nombreDivContenedor).jtable('showCreateForm');
}

function getEstatusId(record) {
    var dateString1 = record.fecha_vence;
    var retVal = 0;
    if (record.Realizada) {
        retVal = 4;
    }
    else {
        var fechaAhora = new Date();
        if (isEmpty(dateString1) || isBlank(dateString1)) {
            retVal = 1;
        }
        else {
            if (record.tareaperiodicaId != null && record.tareaperiodicaId > 0) {
                retVal = 5;
            }
            var date1 = new Date();
            if (dateString1.indexOf('Date') >= 0) { //Format: /Date(1320259705710)/
                date1 = new Date(
                    parseInt(dateString1.substr(6), 10)
                );
            }
            //if (date1 > fechaAhora) {
            //    retVal = 3;
            //}
            if (date1 <= fechaAhora) {
                retVal = 2;
            }
        }
    }
    return retVal;
}

function getDiferenciaDias(dateString1, dateString2) {
    var date1 = new Date();
    var date2 = new Date();
    if (!isEmpty(dateString1) && !isBlank(dateString1)) {
        if (dateString1.indexOf('Date') >= 0) { //Format: /Date(1320259705710)/
            date1 = new Date(
                parseInt(dateString1.substr(6), 10)
            );
        }
    }
    if (!isEmpty(dateString2) && !isBlank(dateString2)) {
        if (dateString2.indexOf('Date') >= 0) { //Format: /Date(1320259705710)/
            date2 = new Date(
                parseInt(dateString2.substr(6), 10)
            );
        }
    }
    return days_between(date1, date2);;
}

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)

}

function getTituloRefechas(tarea) {
    var largoMaximo = 30;
    var largo = tarea.length;
    if (largoMaximo > largo) {
        tarea = tarea.substring(0, largo - 1);
    }
    else {
        var indice = tarea.indexOf(' ', largoMaximo);
        if (indice < 0) {
            tarea = tarea.substring(0, largo - 1);
        } else {
            tarea = tarea.substring(0, indice);
        }
    }
    return tarea + ' ...';
}

function MarcarRealizada(urlBase, tareaRecId, realizada, objJquery) {

    var formData = {  //Array 
        tareaRecId: tareaRecId,
        realizada: realizada
    };

    $.ajax({
        url: urlBase + "/Realizar",
        type: "POST",
        data: formData,
        success: function (data, textStatus, jqXHR) {
            var $tdFechaVence = objJquery.find(".idFechaVence");
            if ($tdFechaVence.length > 0) {
                $tdFechaVence.text(data.FechaVence);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });

}

$(document).ready(function () {

    var nombreDivContenedor = '#divContenedorTareas';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var soloLectura = div.getAttribute('data-sololectura');
    var agregatareas = div.getAttribute('data-agregatareas');
  
    //if (soloLectura == "False") {
    if (soloLectura == "False" && agregatareas == "True") {
        CargarTablaListaTareas(nombreDivContenedor, div);
    } else {
        CargarTablaListaTareasSoloLectura(nombreDivContenedor, div);
    }

});

function CargarTablaRefechasOculta(nombreDivContenedor, div, tareaRecId) {

    if ($(nombreDivContenedor).jtable() != null) {
        $(nombreDivContenedor).jtable('destroy');
    }
   
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var currentUser = div.getAttribute('data-currentuser');
    var id1 = div.getAttribute('data-id');;

    $(nombreDivContenedor).jtable({
        //selecting: true, //Enable selecting
        //multiselect: true, //Allow multiple selecting
        //selectingCheckboxes: true, //Show checkboxes on first column
        //selectOnRowClick: false, //Enable this to only select using checkboxes
        messages: refechasMensajes,
        tableId: 'refechaTable',
        title: 'Refechas' ,
        actions: {
            listAction: urlBase + '/ListadoRefechas?tareaRecId=' + tareaRecId,
            createAction: urlBase + '/CreateRefecha?minutaId=' + id1 + '&tareaRecId=' + tareaRecId,
            updateAction: urlBase + '/UpdateRefecha',
            deleteAction: urlBase + '/DeleteRefecha'

        },
        fields: {
            refechaId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Fecha: {
                title: 'Refecha',
                create: true,
                edit: true,
                list: true,
                type: 'date',
                listClass: 'RefechaClase'
            }
        },
        recordAdded: function (event, data) {
            $('#divContenedorTareas').jtable('reload');
        }
    
    });
}

function CargarTablaListaTareasSoloLectura(nombreDivContenedor, div) {
    var puedeCambiarSeleccion = false;
    var tipoHacer = 0;
    var dataChildTableRefechas;
    var dataChildTableTareas;
    var datosArray = [];
    var contadorRows = 0;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var currentUser = div.getAttribute('data-currentuser');
    var id1 = div.getAttribute('data-id');

    $(nombreDivContenedor).jtable({
        title: '&nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        toolbar: {
            items: [{
                icon: divRoot + 'Content/images/Misc/excel.png',
                text: '',
                click: function () {
                    window.location = divRoot + "Reportes/TareasExcel";
                }
            }, {
                icon: divRoot + 'Content/images/Misc/pdf.png',
                text: '',
                click: function () {
                    window.location = divRoot + "Reportes/TareasPDF";
                }
            }]
        },
        gotoPageArea: 'none',
        messages: tareasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/ListadoEmpleados' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&minutaId=' + id1,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            },
            createAction: urlBase + '/Create?minutaId=' + id1 + '&enviarCodigo=true'
        },
        fields: {
            Tareas: {
                title: '',
                listClass: 'anchoChil',
                width: '3%',
                sorting: false,
                edit: false,
                create: false,
                display: function (tareaData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Ver Tareas" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $(nombreDivContenedor).jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    selecting: true, //Enable selecting
                                    multiselect: true, //Allow multiple selecting
                                    selectingCheckboxes: true, //Show checkboxes on first column
                                    selectOnRowClick: false, //Enable this to only select using checkboxes
                                    messages: tareasMensajes,
                                    //title: '&nbsp;',
                                    //title: 'Tareas Asignadas a ' + tareaData.record.nombre,
                                    actions: {
                                        listAction: urlBase + '/Listado?empleadoId=' + tareaData.record.empleadoId + '&minutaId=' + id1
                                    },
                                    fields: {
                                        //empleadoId: {
                                        //    type: 'hidden',
                                        //    defaultValue: tareaData.record.empleadoId
                                        //},
                                        empleadoId: {
                                            title: 'Asignado a',
                                            width: '97%',
                                            create: true,
                                            edit: true,
                                            list: false,
                                            options: function (data) {
                                                if (data.source == 'list') {
                                                    //Return url all options for optimization. 
                                                    return urlBase + '/GetEmpleadosOptions';
                                                }
                                                data.clearCache();
                                                //This code runs when user opens edit/create form to create combobox.
                                                //data.source == 'edit' || data.source == 'create'
                                                return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                                            },
                                            defaultValue: tareaData.record.empleadoId
                                        },
                                        solicitante: {
                                            title: 'Solicitante',
                                            create: true,
                                            list: false,
                                            edit: false,
                                            options: function (data) {
                                                if (data.source == 'list') {
                                                    //Return url all options for optimization. 
                                                    return urlBase + '/GetEmpleadosOptions';
                                                }
                                                data.clearCache();
                                                //This code runs when user opens edit/create form to create combobox.
                                                //data.source == 'edit' || data.source == 'create'
                                                return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true&todosEmpleados=true';
                                            }
                                        },
                                        tareaRecId: {
                                            key: true,
                                            create: false,
                                            edit: false,
                                            list: false
                                        },
                                        Refechas: {
                                            title: '',
                                            listClass: 'anchoChil',
                                            width: '1%',
                                            sorting: false,
                                            edit: false,
                                            create: false,
                                            display: function (refechasData) {
                                                //Create an image that will be used to open child table
                                                var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Refechas" />');
                                                //Open child table when user clicks the image
                                                $img.click(function () {
                                                    $(nombreDivContenedor).jtable('openChildTable',
                                                            $img.closest('tr'),
                                                            {
                                                                //selecting: true, //Enable selecting
                                                                //multiselect: true, //Allow multiple selecting
                                                                //selectingCheckboxes: true, //Show checkboxes on first column
                                                                //selectOnRowClick: false, //Enable this to only select using checkboxes
                                                                messages: refechasMensajes,
                                                                tableId: 'refechaTable',
                                                                title: 'Refechas' + '<div id="VerComoQueda"></div>',
                                                                //title: 'Refechas para ' + getTituloRefechas(refechasData.record.tarea) + '<div id="VerComoQueda"></div>',
                                                                actions: {
                                                                    listAction: urlBase + '/ListadoRefechas?tareaRecId=' + refechasData.record.tareaRecId
                                                                },
                                                                fields: {
                                                                    tareaRecId: {
                                                                        type: 'hidden',
                                                                        defaultValue: refechasData.record.tareaRecId
                                                                    },
                                                                    refechaId: {
                                                                        key: true,
                                                                        create: false,
                                                                        edit: false,
                                                                        list: false
                                                                    },
                                                                    activa: {
                                                                        visibility: 'hidden',
                                                                        title: 'activa',
                                                                        display: function (data) {
                                                                            if (data.record.activa) {
                                                                                return '<div id="PuedeEditar">' + data.record.activa + '</div>';
                                                                            }
                                                                            else {
                                                                                return '<div id="NoPuedeEditar">' + data.record.activa + '</div>';
                                                                            }
                                                                        }
                                                                    },
                                                                    Fecha: {
                                                                        title: 'Refecha',
                                                                        create: true,
                                                                        edit: true,
                                                                        list: true,
                                                                        type: 'date',
                                                                        listClass: 'RefechaClase'
                                                                    },
                                                                    ccColorTarea: {
                                                                        visibility: 'hidden',
                                                                        create: false,
                                                                        edit: false,
                                                                        display: function (data) {
                                                                            if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                                                                                return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                                                                            } else {
                                                                                return "";
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                rowInserted: function (event, data) {
                                                                    data.row.find('.jtable-selecting-column').find("input").attr('readonly', 'readonly');
                                                                    data.row.find('.jtable-selecting-column').find("input").attr('disabled', 'disabled');
                                                                    datosArray[contadorRows] = data.row;
                                                                    if (data.row.find("#NoPuedeEditar").length > 0) {
                                                                        data.row.find(".jtable-command-column").css("display", "none");
                                                                    }
                                                                    contadorRows += 1;
                                                                    var $divColorFila = data.row.find("#ColorFila");
                                                                    if ($divColorFila.length > 0) {
                                                                        var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                                                        data.row.css("color", valorColor);
                                                                    }
                                                                }
                                                            }, function (data) { //opened handler
                                                                contadorRows = 0;
                                                                datosArray = [];
                                                                data.childTable.jtable('load');
                                                                dataChildTableRefechas = data.childTable;
                                                            });
                                                });
                                                //Return image to show on the person row
                                                return $img;
                                            }
                                        },
                                        tarea: {
                                            title: 'Tarea',
                                            listClass: 'anchoTarea',
                                            width: '73%',
                                            create: true,
                                            edit: true,
                                            type: 'textarea'
                                        },
                                        fecha_asignada: {
                                            title: 'Asignada',
                                            listClass: 'anchoFechaVence',
                                            width: '13%',
                                            create: false,
                                            edit: false,
                                            type: 'date'
                                        },
                                        fecha_vence: {
                                            listClass: 'anchoFechaVence',
                                            title: 'Vence',
                                            width: '13%',
                                            create: false,
                                            edit: false,
                                            type: 'date'
                                        },
                                        ccColorTarea: {
                                            visibility: 'hidden',
                                            create: false,
                                            edit: false,
                                            display: function (data) {
                                                if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                                                    return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                                                } else {
                                                    return "";
                                                }
                                            }
                                        }, // Realizada
                                        Realizada: {
                                            visibility: 'hidden',
                                            create: false,
                                            edit: false,
                                            display: function (data) {
                                                return '<div id="RealizadaID" data-realizada="' + data.record.Realizada + '">' + data.record.Realizada + '</div>';
                                            }
                                        }
                                    },
                                    rowInserted: function (event, data) {
                                        data.row.find('.jtable-selecting-column').find("input").attr('readonly', 'readonly');
                                        data.row.find('.jtable-selecting-column').find("input").attr('disabled', 'disabled');
                                        var $divColorFila = data.row.find("#ColorFila");
                                        if ($divColorFila.length > 0) {
                                            var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                            data.row.css("color", valorColor);
                                        }
                                        if (data.record.Realizada) {
                                            data.row.css("color", "blue");
                                            dataChildTableTareas.jtable('selectRows', data.row);
                                        }
                                    },
                                    recordsLoaded: function (event, data) {
                                        puedeCambiarSeleccion = true;
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load', {
                                        Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
                                        Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
                                        Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
                                        Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked")
                                    });
                                    puedeCambiarSeleccion = false;
                                    dataChildTableTareas = data.childTable;
                                });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            empleadoId: {
                title: 'Asignado a',
                width: '97%',
                create: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                }

            },
            solicitante: {
                title: 'Solicitante',
                create: true,
                list: false,
                defaultValue: currentUser,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + id1 + '&soloMinuta=true';
                }
            },
            tarea: {
                width: '0%',
                title: 'Tarea',
                create: true,
                edit: false,
                list: false,
                type: 'textarea'
            }
        }
    });

    AjustesDeTabla_CheckBox(nombreDivContenedor);

    $('#Id_Check_Realizadas').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_Realizadas1) {
            $('#Id_Check_NoVencidas').prop('checked', false);
            $('#Id_Check_Vencidas').prop('checked', false);
            $('#Id_Check_SinFecha').prop('checked', false);
        }
    });

    $('#Id_Check_NoVencidas, #Id_Check_Vencidas, #Id_Check_SinFecha').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_NoVencidas1 || Id_Check_Vencidas1 || Id_Check_SinFecha1) {
            $('#Id_Check_Realizadas').prop('checked', false);
        }
    });

    $('.table-update-trigger').on('change', function () {

        $(nombreDivContenedor).jtable('load', {
            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked")
        });

    });

    $(nombreDivContenedor).jtable('load');

}
