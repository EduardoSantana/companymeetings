﻿$(document).ready(function () {

    var nombreDivContenedor = '#divContenedorPeriodicas';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    CargarTablaListaPeriodicas(nombreDivContenedor, div);

});

function CargarTablaListaPeriodicas(nombreDivContenedor, div) {

    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');
    var dataMinutaId = div.getAttribute('data-minutaid');
    var dataReunionId = div.getAttribute('data-reunionid');
    $(nombreDivContenedor).jtable({
        title: 'Tareas Periodicas',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        messages: tareasPeriodicasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/Listado' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting +
                                '&reunionId=' + dataReunionId,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            },
            createAction: urlBase + '/Create?reunionId=' + dataReunionId,
            updateAction: urlBase + '/Update',
            deleteAction: urlBase + '/Delete'
        },
        fields: {
            tareaperiodicaId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            empleadoId: {
                title: 'Empleado',
                width: '30%',
                create: true,
                edit: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetEmpleadosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetEmpleadosOptions?minutaId=' + dataMinutaId + '&soloMinuta=true';
                }

            },
            tarea: {
                title: 'Tarea',
                create: true,
                width: '30%',
                list: true,
                type: 'textarea'
            },
            Activa: {
                title: 'Activa',
                width: '5%',
                create: false,
                edit: true,
                list: true,
                type: 'checkbox',
                values: { 'false': 'No', 'true': 'Si' },
                setOnTextClick: true
            },
            PeriodoId: {
                title: 'Periodo',
                width: '10%',
                create: true,
                edit: true,
                list: true,
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetPeriodosOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetPeriodosOptions';
                }

            },
            DiaId: {
                title: 'Dia',
                create: true,
                edit: true,
                list: false,
                dependsOn: 'PeriodoId', //Cities depends on countries.
                options: function (data) {
                    if (data.source == 'list') {
                        //Return url all options for optimization. 
                        return urlBase + '/GetDiasOptions';
                    }
                    data.clearCache();
                    //This code runs when user opens edit/create form to create combobox.
                    //data.source == 'edit' || data.source == 'create'
                    return urlBase + '/GetDiasOptions?PeriodoId=' + data.dependedValues.PeriodoId;
                }

            },
            proxima_ejecucion: {
                title: 'Proxima',
                create: true,
                edit: true,
                width: '10%',
                list: true,
                type: 'date'
            }
        }
    });


    $(nombreDivContenedor).jtable('load');
}
