﻿$(document).ready(function () {
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    $("#btnEliminarRefecha").click(function () {
        var tareaRecId = $("#frmEliminarRefecha #tareaRecId").val();
        var Confirmar = $("#frmEliminarRefecha #Confirmar").val();
        llamadaBotonEliminar(divRoot, tareaRecId, Confirmar);
    });
});

function llamadaBotonEliminar(divRoot, tareaRecId, Confirmar) {

    var formData = {  //Array 
        refechaId: 0,
        tareaRecId: tareaRecId,
        Confirmar: Confirmar
    };
    if (Confirmar == "true") {
        $.ajax({
            url: divRoot + "TareasJson/DeleteRefecha",
            type: "POST",
            data: formData,
            success: function (data, textStatus, jqXHR) {
                $('#divContenedorTareas').jtable('reload');
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }
}
