﻿var autosave_timer = null;
function save() {

    var nombreDivContenedor = '#divContenedorParticipantes';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var soloLectura = div.getAttribute('data-soloLectura');
    var urlBase = div.getAttribute('data-url');
    var id1 = div.getAttribute('data-id');
    var formData = {  //Array 
        minutaId: id1,
        agenda: $("#minuta_agenda").val(),
        pendientes: $("#minuta_puntos_pendientes").val()
    };

    if (soloLectura == "False") {
        $.ajax({
            type: "POST",
            url: urlBase + "/AutoSave",
            data: formData,
            success: function (data) {
                console.log("Auto Saved Agenda.");
            }
        })
    }
}

function autosave() {

    if (autosave_timer) {
        clearTimeout(autosave_timer);
    }

    autosave_timer = setTimeout(save, 5000);
}

function CerrarMinuta() {

    var nombreDivContenedor = '#divContenedorParticipantes';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));
    var soloLectura = div.getAttribute('data-soloLectura');
    var urlBase = div.getAttribute('data-url');
    var id1 = div.getAttribute('data-id');
    var formData = {  //Array 
        minutaId: id1
    };

    if (soloLectura == "False") {
        $.ajax({
            type: "POST",
            url: urlBase + "/CerrarMinuta",
            data: formData,
            success: function (data) {
                console.log("Minuta Cerrada.");
                var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
                Redirect(divRoot + 'FollowTaskIt');
            }
        })
    }
}

$(document).ready(function () {

    var IdBoton = '#btnCerrarMinuta';
  
    $(IdBoton).click(function (event) {
        event.preventDefault();
        CerrarMinuta();
    });

});


//function keep_alive() {
//    http_request = new XMLHttpRequest();
//    http_request.open('GET', "/restricted_file_url");
//    http_request.send(null);
//};

//setInterval(keep_alive, 840000);