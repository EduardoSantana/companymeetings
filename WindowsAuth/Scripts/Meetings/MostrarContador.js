﻿var minutosContador;
$(document).ready(function () {
    changeMinutos();
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    $("#btnMostrarContador").click(function () {
      
        var esVisible = $('.divContador1').is(':visible');
        $(".divContador1").fadeToggle(3000);
        if (!esVisible) {
            function getMinutosFromNow() {
                var _numero = Number(minutosContador);
                return new Date(new Date().valueOf() + _numero * 60 * 1000);
            }
            var $clock = $('#divContador2');
            $clock.countdown(getMinutosFromNow(), function (event) {
                $(this).html(event.strftime('%H:%M:%S'));
                var tieneClase = $(this).parent().hasClass("TiempoFinalizado");
                if (tieneClase) { $(this).parent().removeClass("TiempoFinalizado"); }
            }).on('finish.countdown', function (event) {
                $(this).html('Tiempo Finalizado!')
                     .parent().addClass('TiempoFinalizado');
            });;
        }
       
        // Otro Contador con numeros de pizarra.
        //$('#divContador2').countdown({
        //    image: divRoot + 'Content/images/Misc/digits.png',
        //    startTime: minutosContador + ':00',
        //    timerEnd: function () {
        //        //alert('end!');
        //    },
        //    format: 'mm:ss'
        //});

    });
});
function changeMinutos() {
    minutosContador = $("#Minutos").val();
}