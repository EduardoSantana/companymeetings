﻿

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function AjustesDeTabla_CheckBox(nombreDivContenedor) {

    var nombreSelector = nombreDivContenedor + " .jtable-toolbar";

    var Id_Fecha1 = 'Id_FechaHasta';
    var objLabelFecha1 = '<label for="' + Id_Fecha1 + '" style="margin-left: 3px; margin-bottom: 1px;" >Hasta</label>';
    var objFecha1 = '<input  id="' + Id_Fecha1 + '" type="text" class="date-picker table-update-trigger" placeholder="Fecha Desde" style="margin-left: 10px;     width: 105px;">';
    $(nombreSelector).prepend(objFecha1);
    $(nombreSelector).prepend(objLabelFecha1);

    var Id_Fecha2 = 'Id_FechaDesde';
    var objLabelFecha2 = '<label for="' + Id_Fecha2 + '" style="margin-left: 3px; margin-bottom: 1px;" >Desde</label>';
    var objFecha2 = '<input  id="' + Id_Fecha2 + '" type="text" class="date-picker table-update-trigger" placeholder="Fecha Hasta" style="margin-left: 10px;     width: 105px;">';
    $(nombreSelector).prepend(objFecha2);
    $(nombreSelector).prepend(objLabelFecha2);

    var Id_Check1 = 'Id_Check_Realizadas';
    var objLabel1 = '<label for="' + Id_Check1 + '" style="margin-left: 3px; margin-bottom: 1px; margin-right: 3px;" >Realizadas</label>';
    var objCheck1 = '<input  id="' + Id_Check1 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel1);
    $(nombreSelector).prepend(objCheck1);

    var Id_Check2 = 'Id_Check_NoVencidas';
    var objLabel2 = '<label for="' + Id_Check2 + '" style="margin-left: 3px; margin-bottom: 1px;" >No Vencidas</label>';
    var objCheck2 = '<input  id="' + Id_Check2 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel2);
    $(nombreSelector).prepend(objCheck2);

    var Id_Check3 = 'Id_Check_Vencidas';
    var objLabel3 = '<label for="' + Id_Check3 + '" style="margin-left: 3px; margin-bottom: 1px;" >Vencidas</label>';
    var objCheck3 = '<input  id="' + Id_Check3 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel3);
    $(nombreSelector).prepend(objCheck3);

    var Id_Check4 = 'Id_Check_SinFecha';
    var objLabel4 = '<label for="' + Id_Check4 + '" style="margin-left: 3px; margin-bottom: 1px;" >Sin Fecha</label>';
    var objCheck4 = '<input  id="' + Id_Check4 + '" type="checkbox" class="table-update-trigger" value="true" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabel4);
    $(nombreSelector).prepend(objCheck4);

    $(".date-picker").datepicker();


//    <label>Radio buttons</label>            
//          <input type = "radio"
//    name = "radSize"
//    id = "sizeSmall"
//    value = "small"
//    checked = "checked" />
//<label for = "sizeSmall">small</label>
          
//<input type = "radio"
//       name = "radSize"
//       id = "sizeMed"
//       value = "medium" />
//<label for = "sizeMed">medium</label>
 
//<input type = "radio"
//       name = "radSize"
//       id = "sizeLarge"
//       value = "large" />
//<label for = "sizeLarge">large</label>

}

function AjustesDeTabla_CheckBoxTrigger(nombreDivContenedor) {

    $('#Id_Check_Realizadas').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_Realizadas1) {
            $('#Id_Check_NoVencidas').prop('checked', false);
            $('#Id_Check_Vencidas').prop('checked', false);
            $('#Id_Check_SinFecha').prop('checked', false);
        }
    });

    $('#Id_Check_NoVencidas, #Id_Check_Vencidas, #Id_Check_SinFecha').on('change', function () {
        var Id_Check_Realizadas1 = $('#Id_Check_Realizadas').is(":checked");
        var Id_Check_NoVencidas1 = $('#Id_Check_NoVencidas').is(":checked");
        var Id_Check_Vencidas1 = $('#Id_Check_Vencidas').is(":checked");
        var Id_Check_SinFecha1 = $('#Id_Check_SinFecha').is(":checked");
        if (Id_Check_NoVencidas1 || Id_Check_Vencidas1 || Id_Check_SinFecha1) {
            $('#Id_Check_Realizadas').prop('checked', false);
        }
    });

    $('.table-update-trigger, #currentEmpleadoId').on('change', function () {

        $(nombreDivContenedor).jtable('load', {
            empleadoId: $('#currentEmpleadoId').val(),
            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked"),
            fechaDesde: $('#Id_FechaDesde').val(),
            fechaHasta: $('#Id_FechaHasta').val()
        });

        $('#calendar').fullCalendar('refetchEvents');

    });

}

function CargarCalendario(div) {

    var urlBase = div.getAttribute('data-url');

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: new Date(),
        defaultView: 'agendaWeek',
        editable: false,
        allDaySlot: false,
        slotDuration: '01:00:00',
        //minTime: "06:00:00",
        //maxTime: "22:00:00",
        height: 400,
        //events: urlBase + '/EventosCalendario?empleadoId=' + $('#currentEmpleadoId').val(),
        events: {
            url: urlBase + '/EventosCalendario',
            type: 'POST',
            data: function () { // a function that returns an object
                return {
                    empleadoId: $('#currentEmpleadoId').val()
                };
            }
        },
        eventRender: function (event, element) {
            element.qtip({
                content: {
                    text: event.description,
                    title: event.title
                },
                //style: 'qtip-blue'
                //style: 'qtip-tipped'
                style: 'qtip-bootstrap'
            });
        }
        //dayClick: function () { alert('a day has been clicked!'); },
    });

}

$(document).ready(function () {

    var nombreDivContenedor = '#divContenedorCompromisos';
    var div = document.getElementById(nombreDivContenedor.replace('#', ''));

    CargarCalendario(div);

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var currentTab = $(e.target).text(); // get current tab
        var LastTab = $(e.relatedTarget).text(); // get last tab
        //$(".current-tab span").html(currentTab);
        //$(".last-tab span").html(LastTab);
        if (currentTab == 'Calendario') {
            $('#calendar').fullCalendar('render');
        }
    });

    CargarTablaCompromisos(nombreDivContenedor, div);
    AjustesDeTabla_CheckBox(nombreDivContenedor);
    AjustesDeTabla_CheckBoxTrigger(nombreDivContenedor);

    $(nombreDivContenedor).jtable('load', {
        empleadoId: $('#currentEmpleadoId').val()
    });

});

function CargarTablaCompromisos(nombreDivContenedor, div) {
    var dataChildTableTareas;
    var dataChildTableReuniones;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');

    $(nombreDivContenedor).jtable({
        title: '&nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        toolbar: {
            items: [{
                icon: divRoot + 'Content/images/Misc/excel.png',
                text: '',
                click: function () {
                    var queryString = 'empleadoId=' + $('#currentEmpleadoId').val() + '&';
                    queryString += 'Id_Check_Realizadas=' + $('#Id_Check_Realizadas').is(":checked") + '&';
                    queryString += 'Id_Check_NoVencidas=' + $('#Id_Check_NoVencidas').is(":checked") + '&';
                    queryString += 'Id_Check_Vencidas=' + $('#Id_Check_Vencidas').is(":checked") + '&';
                    queryString += 'Id_Check_SinFecha=' + $('#Id_Check_SinFecha').is(":checked") + '&';
                    queryString += 'fechaDesde=' + $('#Id_FechaDesde').val() + '&';
                    queryString += 'fechaHasta=' + $('#Id_FechaHasta').val();

                    window.location = divRoot + "Reportes/TareasExcel?" + queryString;
                }
            }, {
                icon: divRoot + 'Content/images/Misc/pdf.png',
                text: '',
                click: function () {
                    var queryString = 'empleadoId=' + $('#currentEmpleadoId').val() + '&';
                    queryString += 'Id_Check_Realizadas=' + $('#Id_Check_Realizadas').is(":checked") + '&';
                    queryString += 'Id_Check_NoVencidas=' + $('#Id_Check_NoVencidas').is(":checked") + '&';
                    queryString += 'Id_Check_Vencidas=' + $('#Id_Check_Vencidas').is(":checked") + '&';
                    queryString += 'Id_Check_SinFecha=' + $('#Id_Check_SinFecha').is(":checked") + '&';
                    queryString += 'fechaDesde=' + $('#Id_FechaDesde').val() + '&';
                    queryString += 'fechaHasta=' + $('#Id_FechaHasta').val();
                    window.location = divRoot + "Reportes/TareasPDF?" + queryString;
                }
            }]
        },
        gotoPageArea: 'none',
        messages: compromisosMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/Departamentos' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            }
        },
        fields: {
            Reuniones: {
                title: '',
                listClass: 'anchoChil',
                width: '3%',
                sorting: false,
                edit: false,
                create: false,
                display: function (reunionData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Ver Reuniones" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        var estaAbierto1 = $(nombreDivContenedor).jtable('isChildRowOpen', $img.closest('tr'));
                        if (estaAbierto1) {
                            $(nombreDivContenedor).jtable('closeChildTable', $img.closest('tr'));
                        } else {
                            $(nombreDivContenedor).jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        actions: {
                                            listAction: urlBase + '/Reuniones?departamentoId=' + reunionData.record.departamentoId + '&empleadoId=' + reunionData.record.empleadoId
                                        },
                                        fields: {
                                            Tareas: {
                                                title: '',
                                                listClass: 'anchoChil',
                                                width: '3%',
                                                sorting: false,
                                                edit: false,
                                                create: false,
                                                display: function (tareaData) {
                                                    //Create an image that will be used to open child table
                                                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Ver Tareas" />');
                                                    //Open child table when user clicks the image
                                                    $img.click(function () {
                                                        var estaAbierto1 = $(nombreDivContenedor).jtable('isChildRowOpen', $img.closest('tr'));
                                                        if (estaAbierto1) {
                                                            $(nombreDivContenedor).jtable('closeChildTable', $img.closest('tr'));
                                                        } else {
                                                            $(nombreDivContenedor).jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        selecting: true, //Enable selecting
                                                                        multiselect: true, //Allow multiple selecting
                                                                        selectingCheckboxes: true, //Show checkboxes on first column
                                                                        selectOnRowClick: false, //Enable this to only select using checkboxes
                                                                        messages: tareasMensajes,
                                                                        //title: '&nbsp;',
                                                                        //title: 'Tareas Asignadas a ' + tareaData.record.nombre,
                                                                        actions: {
                                                                            listAction: urlBase + '/Tareas?empleadoId=' + tareaData.record.empleadoId + '&reunionId=' + tareaData.record.reunionId
                                                                        },
                                                                        fields: {
                                                                            //empleadoId: {
                                                                            //    type: 'hidden',
                                                                            //    defaultValue: tareaData.record.empleadoId
                                                                            //},
                                                                            empleadoId: {
                                                                                title: 'Asignado a',
                                                                                width: '97%',
                                                                                create: true,
                                                                                edit: true,
                                                                                list: false,
                                                                                options: function (data) {
                                                                                    if (data.source == 'list') {
                                                                                        //Return url all options for optimization. 
                                                                                        return urlBase + '/GetEmpleadosOptions';
                                                                                    }
                                                                                    data.clearCache();
                                                                                },
                                                                                defaultValue: tareaData.record.empleadoId
                                                                            },
                                                                            solicitante: {
                                                                                title: 'Solicitante',
                                                                                create: true,
                                                                                list: false,
                                                                                edit: false,
                                                                                options: function (data) {
                                                                                    if (data.source == 'list') {
                                                                                        //Return url all options for optimization. 
                                                                                        return urlBase + '/GetEmpleadosOptions';
                                                                                    }
                                                                                    data.clearCache();
                                                                                }
                                                                            },
                                                                            tareaRecId: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            estatusId: {
                                                                                visibility: 'hidden',
                                                                                create: false,
                                                                                edit: false,
                                                                                display: function (data) {
                                                                                    return '<div id="estatusId" data-estatusId="' + getEstatusId(data.record) + '">' + getEstatusId(data.record) + '</div>';
                                                                                }
                                                                            },
                                                                            tarea: {
                                                                                title: 'Tarea',
                                                                                listClass: 'anchoTarea',
                                                                                width: '73%',
                                                                                create: true,
                                                                                edit: true,
                                                                                type: 'textarea'
                                                                            },
                                                                            fecha_asignada: {
                                                                                title: 'Asignada',
                                                                                listClass: 'anchoFechaVence',
                                                                                width: '13%',
                                                                                create: false,
                                                                                edit: false,
                                                                                type: 'date'
                                                                            },
                                                                            fecha_vence: {
                                                                                listClass: 'anchoFechaVence',
                                                                                title: 'Vence',
                                                                                width: '13%',
                                                                                create: false,
                                                                                edit: false,
                                                                                type: 'date'
                                                                            },
                                                                            ccColorTarea: {
                                                                                visibility: 'hidden',
                                                                                create: false,
                                                                                edit: false,
                                                                                display: function (data) {
                                                                                    if (!isBlank(data.record.ccColorTarea) && !isEmpty(data.record.ccColorTarea)) {
                                                                                        return '<div id="ColorFila" data-colorfila="' + data.record.ccColorTarea + '">' + data.record.ccColorTarea + '</div>';
                                                                                    } else {
                                                                                        return "";
                                                                                    }
                                                                                }
                                                                            },
                                                                            estatusId: {
                                                                                visibility: 'hidden',
                                                                                create: false,
                                                                                edit: false,
                                                                                display: function (data) {
                                                                                    return '<div id="estatusId" data-estatusId="' + getEstatusId(data.record) + '">' + getEstatusId(data.record) + '</div>';
                                                                                }
                                                                            },
                                                                            Realizada: {
                                                                                visibility: 'hidden',
                                                                                create: false,
                                                                                edit: false,
                                                                                display: function (data) {
                                                                                    return '<div id="RealizadaID" data-realizada="' + data.record.Realizada + '">' + data.record.Realizada + '</div>';
                                                                                }
                                                                            }
                                                                        },
                                                                        rowInserted: function (event, data) {
                                                                            data.row.find('.jtable-selecting-column').find("input").attr('readonly', 'readonly');
                                                                            data.row.find('.jtable-selecting-column').find("input").attr('disabled', 'disabled');
                                                                            var $divColorFila = data.row.find("#ColorFila");
                                                                            if ($divColorFila.length > 0) {
                                                                                var valorColor = $divColorFila[0].attributes.getNamedItem("data-colorfila").value;
                                                                                data.row.css("color", valorColor);
                                                                            }
                                                                            if (data.record.Realizada) {
                                                                                data.row.css("color", "blue");
                                                                                data.row.addClass("clRealizada");
                                                                                dataChildTableTareas.jtable('selectRows', data.row);
                                                                            } else {
                                                                                if (data.row.hasClass("clRealizada")) {
                                                                                    data.row.removeClass("clRealizada");
                                                                                }
                                                                                var $divEstatusId = data.row.find("#estatusId");
                                                                                if ($divEstatusId.length > 0) {
                                                                                    var valorEstatusId = $divEstatusId[0].attributes.getNamedItem("data-estatusId").value;
                                                                                    if (valorEstatusId == 2) { // Esta Vencida
                                                                                        data.row.css("background-color", '#ffe6e6');
                                                                                        data.row.css("border-color", '#ffe6e6');
                                                                                    }
                                                                                    if (valorEstatusId == 5) { // Es Periodica
                                                                                        data.row.css("background-color", '#FFFFA3');
                                                                                        data.row.css("border-color", '#FFFFA3');
                                                                                    }
                                                                                }
                                                                            }
                                                                        },
                                                                        recordsLoaded: function (event, data) {
                                                                            puedeCambiarSeleccion = true;
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load', {
                                                                            empleadoId: $('#currentEmpleadoId').val(),
                                                                            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
                                                                            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
                                                                            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
                                                                            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked"),
                                                                            fechaDesde: $('#Id_FechaDesde').val(),
                                                                            fechaHasta: $('#Id_FechaHasta').val()
                                                                        });

                                                                        puedeCambiarSeleccion = false;
                                                                        dataChildTableTareas = data.childTable;

                                                                    });
                                                        }

                                                    });
                                                    //Return image to show on the person row
                                                    return $img;
                                                }
                                            },
                                            nombre_reunion: {
                                                title: 'Reunion',
                                                width: '97%'
                                            }
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load', {
                                            empleadoId: $('#currentEmpleadoId').val(),
                                            Id_Check_Realizadas: $('#Id_Check_Realizadas').is(":checked"),
                                            Id_Check_NoVencidas: $('#Id_Check_NoVencidas').is(":checked"),
                                            Id_Check_Vencidas: $('#Id_Check_Vencidas').is(":checked"),
                                            Id_Check_SinFecha: $('#Id_Check_SinFecha').is(":checked"),
                                            fechaDesde: $('#Id_FechaDesde').val(),
                                            fechaHasta: $('#Id_FechaHasta').val()
                                        });
                                        dataChildTableReuniones = data.childTable;
                                    });
                        }


                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            nombre_depto: {
                title: 'Departamento',
                width: '97%'
            }
        }
    });

}

function getEstatusId(record) {
    var dateString1 = record.fecha_vence;
    var retVal = 0;
    if (record.Realizada) {
        retVal = 4;
    }
    else {
        var fechaAhora = new Date();
        if (isEmpty(dateString1) || isBlank(dateString1)) {
            retVal = 1;
        }
        else {
            if (record.tareaperiodicaId != null && record.tareaperiodicaId > 0) { // Es Periodica
                retVal = 5;
            }
            var date1 = new Date();
            if (dateString1.indexOf('Date') >= 0) { //Format: /Date(1320259705710)/
                date1 = new Date(
                    parseInt(dateString1.substr(6), 10)
                );
            }
            //if (date1 > fechaAhora) {
            //    retVal = 3;
            //}
            if (date1 <= fechaAhora) { // Esta Vencida
                retVal = 2;
            }
        }
    }
    return retVal;
}
