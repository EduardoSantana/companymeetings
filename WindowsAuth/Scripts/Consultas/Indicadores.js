﻿function CargarTablaIndicadores(nombreDivContenedor, div) {
    var puedeCambiarSeleccion = false;
    var tipoHacer = 1;
    var dataChildTableRefechas;
    var dataChildTableTareas;
    var dataTareas;
    var datosArray = [];
    var contadorRows = 0;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');

    $(nombreDivContenedor).jtable({
        title: 'Reuniones y Otros &nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        messages: tareasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/ListadoReuniones' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            }
        },
        fields: {
            FechaAno: {
                width: '10%',
                title: 'Ano'
            },
            FechaMesNombre: {
                width: '10%',
                title: 'Mes'
            },
            Realizadas: {
                width: '10%',
                title: 'Realizadas',
                listClass: 'anchoCentrado'
            },
            Total: {
                width: '10%',
                title: 'Total',
                listClass: 'anchoCentrado'
            },
            Porcentaje: {
                title: 'Cumplimiento',
                listClass: 'anchoCentrado'
            },
            Refechadas: {
                title: 'Refechadas',
                listClass: 'anchoCentrado'
            }
        },
        rowInserted: function (event, data) {
            if (!isEmpty(data.record.ccColor)) {
                data.row.css("color", data.record.ccColor);
                data.row.css("font-weight", 'bold');
                //    font-weight: bold;
                //data.row.css("background-color", data.record.ccColor);
                //data.row.css("border-color", data.record.ccColor);
            }
        }
    });

    $(nombreDivContenedor).jtable('load', { empleadoId: $('#currentEmpleadoId').val() });

    $('#currentEmpleadoId').on('change', function () {

        $(nombreDivContenedor).jtable('load', { empleadoId: $('#currentEmpleadoId').val() });

    });

}

function CargarTablaServicios(nombreDivContenedor, div) {

    var puedeCambiarSeleccion = false;
    var tipoHacer = 1;
    var dataChildTableRefechas;
    var dataChildTableTareas;
    var dataTareas;
    var datosArray = [];
    var contadorRows = 0;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');

    $(nombreDivContenedor).jtable({
        title: 'Servicios &nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        messages: tareasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/ListadoServicios' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            }
        },
        fields: {
            FechaAno: {
                width: '10%',
                title: 'Ano'
            },
            FechaMesNombre: {
                width: '10%',
                title: 'Mes'
            },
            Cumplidas: {
                width: '10%',
                title: 'Cumplidas',
                listClass: 'anchoCentrado'
            },
            Realizadas: {
                width: '10%',
                title: 'Realizadas',
                listClass: 'anchoCentrado'
            },
            Total: {
                width: '10%',
                title: 'Total',
                listClass: 'anchoCentrado'
            },
            Porcentaje: {
                title: 'Cumplimiento',
                listClass: 'anchoCentrado'
            }
        },
        rowInserted: function (event, data) {
            if (!isEmpty(data.record.ccColor)) {
                data.row.css("color", data.record.ccColor);
                //data.row.css("background-color", data.record.ccColor);
                //data.row.css("border-color", data.record.ccColor);
                data.row.css("font-weight", 'bold');
            }
        }
    });

    $(nombreDivContenedor).jtable('load', { empleadoId: $('#currentEmpleadoId').val() });

    $('#currentEmpleadoId').on('change', function () {

        $(nombreDivContenedor).jtable('load', { empleadoId: $('#currentEmpleadoId').val() });

    });

}

function CargarTablaProyectos(nombreDivContenedor, div) {

    var puedeCambiarSeleccion = false;
    var tipoHacer = 1;
    var dataChildTableRefechas;
    var dataChildTableTareas;
    var dataTareas;
    var datosArray = [];
    var contadorRows = 0;
    var divRoot = document.getElementsByTagName("body")[0].getAttribute('data-root');
    var urlBase = div.getAttribute('data-url');

    $(nombreDivContenedor).jtable({
        title: 'Proyectos &nbsp;',
        openChildAsAccordion: true,
        paging: false,
        sorting: false,
        pageSize: 10,
        gotoPageArea: 'none',
        messages: tareasMensajes,
        pageSizeChangeArea: false,
        columnResizable: false, //Actually, no need to set true since it's default
        columnSelectable: false, //Actually, no need to set true since it's default
        saveUserPreferences: false, //Actually, no need to set true since it's default
        actions: {
            listAction: function (postData, jtParams) {
                return $.Deferred(function ($dfd) {
                    $.ajax({
                        url: urlBase + '/ListadoProyectos1Empleados' +
                                '?jtStartIndex=' + jtParams.jtStartIndex +
                                '&jtPageSize=' + jtParams.jtPageSize +
                                '&jtSorting=' + jtParams.jtSorting,
                        type: 'POST',
                        dataType: 'json',
                        data: postData,
                        success: function (data) {
                            $dfd.resolve(data);
                        },
                        error: function () {
                            $dfd.reject();
                        }
                    });
                });
            }
        },
        fields: {
            Reuniones: {
                title: '',
                width: '1%',
                sorting: false,
                edit: false,
                create: false,
                display: function (reunionData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="' + divRoot + 'Content/images/Misc/list_metro.png" title="Ver Proyectos" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        var estaAbierto1 = $(nombreDivContenedor).jtable('isChildRowOpen', $img.closest('tr'));
                        if (estaAbierto1) {
                            $(nombreDivContenedor).jtable('closeChildTable', $img.closest('tr'));
                        } else {
                            $(nombreDivContenedor).jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        actions: {
                                            listAction: urlBase + '/ListadoProyectos2Reuniones?empleadoId=' + reunionData.record.empleadoId
                                        },
                                        fields: {
                                            nombre_reunion: {
                                                title: 'Reunion',
                                                width: '19%'
                                            },
                                            Logro: {
                                                title: '% Logro',
                                                listClass: 'anchoCentrado'
                                            },
                                            Porcentaje: {
                                                title: '% Realizado',
                                                listClass: 'anchoCentrado'
                                            },
                                            PorcentajeEstimadas: {
                                                title: '% Estimado',
                                                listClass: 'anchoCentrado'
                                            },
                                            Total: {
                                                title: 'Total',
                                                listClass: 'anchoCentrado'
                                            },
                                            Realizadas: {
                                                title: 'Realizadas',
                                                listClass: 'anchoCentrado'
                                            },
                                            Estimadas: {
                                                title: 'Estimadas',
                                                listClass: 'anchoCentrado'
                                            }
                                        },
                                        rowInserted: function (event, data) {
                                            if (!isEmpty(data.record.ccColor)) {
                                                data.row.css("color", data.record.ccColor);
                                                data.row.css("font-weight", 'bold');
                                                //data.row.css("background-color", data.record.ccColor);
                                                //data.row.css("border-color", data.record.ccColor);
                                            }
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load', {
                                            empleadoSeleccionado: $('#currentEmpleadoId').val(),
                                            estatusId: 1
                                        });

                                    });
                        }

                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            NombreEmpleado: {
                width: '95%',
                title: 'Responsable'
            }
        }
    });

    AjustesDeTabla_CheckBox(nombreDivContenedor);

    $('#currentEmpleadoId, .radioTriguer').on('change', function () {

        var opcionSeleccion = 0;
        var Id_Inactivo = $('#Id_Inactivo').is(":checked");
        var Id_No_Aprobado = $('#Id_No_Aprobado').is(":checked");

        if (Id_Inactivo) {
            opcionSeleccion = 1;
        }
        if (Id_No_Aprobado) {
            opcionSeleccion = 2;
        }

        $(nombreDivContenedor).jtable('load', { empleadoSeleccionado: $('#currentEmpleadoId').val(), estatusId: opcionSeleccion });

    });

    $(nombreDivContenedor).jtable('load', { empleadoSeleccionado: $('#currentEmpleadoId').val(), estatusId: '1' });

}

function AjustesDeTabla_CheckBox(nombreDivContenedor) {

    var nombreSelector = nombreDivContenedor + " .jtable-toolbar";

    var Id_Radio3 = 'Id_No_Aprobado';
    var ValorRadio3 = 'No_Aprobado';
    var objLabelRadio3 = '<label for="' + Id_Radio3 + '" style="margin-left: 3px; margin-bottom: 1px;" >No Aprobado</label>';
    var objRadio3 = '<input  id="' + Id_Radio3 + '" type="radio" class="radioTriguer" name="estatusId" value="' + ValorRadio3 + '" style="margin-left: 10px; ">';
    $(nombreSelector).prepend(objLabelRadio3);
    $(nombreSelector).prepend(objRadio3);

    var Id_Radio2 = 'Id_Inactivo';
    var ValorRadio2 = 'Inactivo';
    var objLabelRadio2 = '<label for="' + Id_Radio2 + '" style="margin-left: 3px; margin-bottom: 1px;" >' + ValorRadio2 + '</label>';
    var objRadio2 = '<input  id="' + Id_Radio2 + '" type="radio" class="radioTriguer" name="estatusId" value="' + ValorRadio2 + '" style="margin-left: 10px; ">';
    $(nombreSelector).prepend(objLabelRadio2);
    $(nombreSelector).prepend(objRadio2);

    var Id_Radio1 = 'Id_Activo';
    var ValorRadio1 = 'Activo';
    var objLabelRadio1 = '<label for="' + Id_Radio1 + '" style="margin-left: 3px; margin-bottom: 1px;" >' + ValorRadio1 + '</label>';
    var objRadio1 = '<input  id="' + Id_Radio1 + '" type="radio" class="radioTriguer" name="estatusId" value="' + ValorRadio1 + '" checked="checked" style="margin-left: 10px;">';
    $(nombreSelector).prepend(objLabelRadio1);
    $(nombreSelector).prepend(objRadio1);

}

$(document).ready(function () {

    var nombreDivContenedor1 = '#divContenedorIndicadores';
    var div1 = document.getElementById(nombreDivContenedor1.replace('#', ''));
    CargarTablaIndicadores(nombreDivContenedor1, div1);

    var nombreDivContenedor2 = '#divContenedorServicios';
    var div2 = document.getElementById(nombreDivContenedor2.replace('#', ''));
    CargarTablaServicios(nombreDivContenedor2, div2);

    var nombreDivContenedor3 = '#divContenedorProyectos';
    var div3 = document.getElementById(nombreDivContenedor3.replace('#', ''));
    CargarTablaProyectos(nombreDivContenedor3, div3);

});

function isEmpty(str) {
    return (!str || 0 === str.length);
}