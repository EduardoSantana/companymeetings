﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using FollowTaskIt.AccesoBaseDatosV2;
//Required for custom paging
using System.Linq.Expressions;

namespace FollowTaskIt.Presentacion.Models
{
    public static class SortExtension
    {
        public static IOrderedEnumerable<TSource> OrderByWithDirection<TSource, TKey> 
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, bool descending)
        {
            return descending ? source.OrderByDescending(keySelector) : source.OrderBy(keySelector);
        }

        public static IOrderedQueryable<TSource> OrderByWithDirection<TSource, TKey>
            (this IQueryable<TSource> source,
             Expression<Func<TSource, TKey>> keySelector,
             bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }
    }

    public class ModelServices : IDisposable
    {
        private ModelFollowTaskIt entities = new ModelFollowTaskIt();
        public bool SaveCustomer(int tareaid, string tarea, string solicitante)
        {
            try
            {
                TAREAS tareas = new TAREAS();
                tareas.tareaId = tareaid;
                tareas.tarea = tarea;
                tareas.solicitante = solicitante;

                entities.TAREAS.Add(tareas);
                entities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateCustomer(int tareaid, string tarea, string solicitante)
        {
            try
            {
                var tareas = (from tbl in entities.TAREAS
                              where tbl.tareaId == tareaid
                              select tbl).FirstOrDefault();
                tareas.tareaId = tareaid;
                tareas.tarea = tarea;
                tareas.solicitante = solicitante;

                entities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteCustomer(int tareaid)
        {
            try
            {
                var tareas = (from tbl in entities.TAREAS
                              where tbl.tareaId == tareaid
                              select tbl).FirstOrDefault();

                entities.TAREAS.Remove(tareas);
                entities.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //For Custom Paging
        public IEnumerable<TAREAS> GetCustomerPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "tareaId")
                return entities.TAREAS.OrderByWithDirection(x => x.tareaId, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else if (sort == "tarea")
                return entities.TAREAS.OrderByWithDirection(x => x.tarea, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else if (sort == "solicitante")
                return entities.TAREAS.OrderByWithDirection(x => x.solicitante, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else
                return entities.TAREAS.OrderByWithDirection(x => x.tareaId, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public int CountCustomer()
        {
            return entities.TAREAS.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

    }

    public class PagedMinutaGridModel
    {
        public int TotalRows { get; set; }
        public IEnumerable<TAREAS> tareas1 { get; set; }
        public int PageSize { get; set; }
    }
    //public class MinutaGrid
    //{
    //}
}