﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FollowTaskIt.Presentacion.Startup))]
namespace FollowTaskIt.Presentacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
