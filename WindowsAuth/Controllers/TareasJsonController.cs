﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class TareasJsonController : BaseJsonController
    {
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoEmpleados(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", int minutaId = 0, string searchString = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false)
        {
            try
            {
                var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, null, null, minutaId);
                var listadoEmpleados = listadoTareas.Select(p => p.EMPLEADOS).ToList().Distinct();
                var listado = listadoEmpleados.Select(a => new
                {
                    tareaId = 0,
                    minutaId = 0,
                    empleadoId = a.empleadoId,
                    nombre = a.nombre,
                    tarea = "",
                    fecha_asignada = DateTime.Now,
                    tareaRecId = 0
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Create(string tarea, string empleadoId, int minutaId, DateTime? fechaVence, string solicitante = "", bool enviarCodigo = false)
        {
            try
            {
                var esParticipante = db.MINUTA.Find(minutaId).PARTICIPANTES.Where(p => p.empleadoId == empleadoId).Any();
                if (!ModelState.IsValid || String.IsNullOrEmpty(tarea) || String.IsNullOrEmpty(empleadoId) || minutaId == 0 || !esParticipante)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                if (fechaVence != null && fechaVence < DateTime.Now)
                {
                    return Json(new { Result = "ERROR", Message = "No puede agregarse porque la fecha es menor al dia de hoy." });
                }
                string usuario = "";
                string usuario_nombre = "";
                if (String.IsNullOrEmpty(solicitante))
                {
                    usuario = User.Identity.Name.ToString();
                    usuario_nombre = (from data in db.EMPLEADOS where data.usuario == usuario select data.nombre).FirstOrDefault();
                }
                else if (enviarCodigo)
                {
                    usuario_nombre = (from data in db.EMPLEADOS where data.empleadoId == solicitante select data.nombre).FirstOrDefault();
                }
                if (!String.IsNullOrEmpty(usuario_nombre))
                {
                    solicitante = usuario_nombre;
                }
                var nuevaTarea1 = new TAREAS();
                nuevaTarea1.empleadoId = empleadoId;
                nuevaTarea1.tarea = tarea;
                nuevaTarea1.minutaId = minutaId;
                nuevaTarea1.solicitante = solicitante;
                nuevaTarea1.fecha_asignada = DateTime.Now;
                nuevaTarea1.Realizada = false;
                if (fechaVence != null)
                {
                    var nuevaRefecha = new REFECHAS();
                    nuevaRefecha.Fecha = (DateTime)fechaVence;
                    nuevaRefecha.activa = true;
                    nuevaTarea1.REFECHAS.Add(nuevaRefecha);
                    nuevaTarea1.fecha_vence = fechaVence;
                }
                db.TAREAS.Add(nuevaTarea1);
                var nuevaTarea = new TAREAS();
                nuevaTarea.empleadoId = empleadoId;
                nuevaTarea.tarea = tarea;
                nuevaTarea.minutaId = minutaId;
                nuevaTarea.solicitante = solicitante;
                nuevaTarea.fecha_asignada = DateTime.Now;
                nuevaTarea.Realizada = false;
                nuevaTarea.fecha_vence = fechaVence;

                db.SaveChanges();
                return Json(new { Result = "OK", Record = nuevaTarea });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Delete(int tareaRecId)
        {
            try
            {
                var eliminar = db.TAREAS.Find(tareaRecId);
                db.TAREAS.Remove(eliminar);
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListaParticipantes(int minutaId)
        {
            try
            {
                var listado = db.MINUTA.Find(minutaId).PARTICIPANTES.Select(pp => new { Value = pp.EMPLEADOS.empleadoId, Text = pp.EMPLEADOS.nombre });
                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.Text) });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ReasignarTareas(string listadoTareas, int minutaId, string empleadoIdHasta, bool conRefechas)
        {
            try
            {
                var _listaConvertir = new List<int>();
                foreach (string item in listadoTareas.Split(';'))
                {
                    int valorSalida = 0;
                    if (int.TryParse(item, out valorSalida))
                    {
                        _listaConvertir.Add(valorSalida);
                    }
                }
                var _tempListadoTareas = db.TAREAS.Where(p => _listaConvertir.Contains(p.tareaRecId));
                foreach (TAREAS ta in _tempListadoTareas)
                {
                    ta.empleadoId = empleadoIdHasta;
                    ta.minutaId = minutaId;
                    if (!conRefechas && ta.REFECHAS.Any())
                    {
                        ta.fecha_vence = null;
                        foreach (REFECHAS re in ta.REFECHAS.ToList())
                        {
                            db.REFECHAS.Remove(re);
                        }
                    }
                }
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Update(int tareaRecId, string tarea, bool? Realizada, int? Dias, string Notas, string empleadoId, string hora_ini, string hora_fin)
        {
            try
            {
                bool estaRealizada = false;

                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                var obj = db.TAREAS.Find(tareaRecId);
                obj.tarea = tarea;
                if (Realizada != null)
                {
                    estaRealizada = (bool)Realizada;
                    obj.Realizada = estaRealizada;
                }
                obj.empleadoId = empleadoId;
                obj.Notas = Notas;
                obj.hora_fin = hora_fin;
                obj.hora_ini = hora_ini;
                if (Dias != null && Dias > 0)
                {
                    DateTime nuevaFechaVence = ((DateTime)obj.fecha_asignada).AddDays((int)Dias);
                    if (nuevaFechaVence > DateTime.Now && nuevaFechaVence > obj.fecha_vence)
                    {
                        obj.fecha_vence = nuevaFechaVence;
                        var objRefecha = obj.REFECHAS.Where(p => p.activa == true).FirstOrDefault();
                        objRefecha.Fecha = nuevaFechaVence;
                    }
                }

                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Listado(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", int minutaId = 0, string searchString = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, bool soloNoRealizadas = false)
        {
            try
            {
                var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, null, null, minutaId, soloNoRealizadas);
                var listado = listadoTareas.Select(a => new
                {
                    minutaId = a.minutaId,
                    tareaId = a.tareaId,
                    tarea = a.tarea,
                    empleadoId = a.empleadoId,
                    fecha_asignada = a.fecha_asignada,
                    fecha_vence = a.fecha_vence,
                    tareaRecId = a.tareaRecId,
                    nombre = a.EMPLEADOS.nombre,
                    ccColorTarea = a.ccColorTarea,
                    Realizada = a.Realizada,
                    Nota = a.Notas,
                    hora_ini = a.hora_ini,
                    hora_fin = a.hora_fin,
                    tareaperiodicaId = a.tareaperiodicaId
                });
                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.tareaId).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoRefechas(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", int tareaRecId = 0)
        {
            try
            {
                var listado = db.TAREAS.Find(tareaRecId).REFECHAS.Select(p => new
                {
                    tareaRecId = p.tareaRecId,
                    Fecha = p.Fecha,
                    refechaId = p.refechaId,
                    activa = p.activa,
                    ccColorTarea = p.TAREAS.ccColorTarea
                });

                return Json(new { Result = "OK", Records = listado.OrderByDescending(orden => orden.refechaId).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult CreateRefecha(DateTime? fecha, int tareaRecId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var nuevaRefecha = new REFECHAS();
                nuevaRefecha.Fecha = (DateTime)fecha;
                nuevaRefecha.tareaRecId = tareaRecId;
                nuevaRefecha.activa = true;

                if (db.TAREAS.Find(tareaRecId).REFECHAS.Where(p => p.Fecha >= nuevaRefecha.Fecha).Any())
                {
                    return Json(new { Result = "ERROR", Message = "La refecha no puede ser menor a la ultima fecha." });
                }


                if (fecha < DateTime.Now.Date)
                {
                    return Json(new { Result = "ERROR", Message = "La refecha no puede ser menor a la fecha actual." });
                }

                foreach (var item in db.TAREAS.Find(tareaRecId).REFECHAS)
                {
                    if (item.activa)
                    {
                        item.activa = false;
                    }
                }
                db.TAREAS.Find(tareaRecId).fecha_vence = nuevaRefecha.Fecha;
                db.REFECHAS.Add(nuevaRefecha);
                var tempNuevaRefecha = new REFECHAS();
                tempNuevaRefecha.tareaRecId = tareaRecId;
                tempNuevaRefecha.Fecha = (DateTime)fecha;

                nuevaRefecha.activa = true;

                db.SaveChanges();
                tempNuevaRefecha.refechaId = nuevaRefecha.refechaId;
                db = new ModelFollowTaskIt();
                var objTarea = db.TAREAS.Find(tareaRecId);
                return Json(new { Result = "OK", Record = tempNuevaRefecha, Color = objTarea.ccColorTarea, FechaVence = ((DateTime)objTarea.fecha_vence).ToString("dd/MM/yyyy") });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult UpdateRefecha(DateTime? fecha, int refechaId, int tareaRecId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                if (db.TAREAS.Find(tareaRecId).REFECHAS.Where(p => p.Fecha >= fecha && p.refechaId != refechaId).Any())
                {
                    return Json(new { Result = "ERROR", Message = "La refecha no puede ser menor a la ultima fecha." });
                }

                if (fecha < DateTime.Now.Date)
                {
                    return Json(new { Result = "ERROR", Message = "La refecha no puede ser menor a la fecha actual." });
                }
              
                var nuevaRefecha = db.REFECHAS.Find(refechaId);
                nuevaRefecha.Fecha = (DateTime)fecha;
                db.TAREAS.Find(tareaRecId).fecha_vence = nuevaRefecha.Fecha;

                if (!nuevaRefecha.activa)
                {
                    return Json(new { Result = "ERROR", Message = "La refecha no puede ser modificada porque no es la refecha activa." });
                }

                var tempNuevaRefecha = new REFECHAS();
                tempNuevaRefecha.tareaRecId = tareaRecId;
                tempNuevaRefecha.refechaId = refechaId;
                tempNuevaRefecha.Fecha = (DateTime)fecha;
                tempNuevaRefecha.activa = true;
                db.SaveChanges();
                db = new ModelFollowTaskIt();
                var objTarea = db.TAREAS.Find(tareaRecId);
                return Json(new { Result = "OK", Record = tempNuevaRefecha, Color = objTarea.ccColorTarea, FechaVence = ((DateTime)objTarea.fecha_vence).ToString("dd/MM/yyyy") });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Realizar(int tareaRecId, bool realizada)
        {
            try
            {
                DateTime fechaVence = DateTime.Now;
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                var objTarea = db.TAREAS.Find(tareaRecId);
                if (objTarea.Realizada)
                {
                    if (!realizada)
                    {
                        objTarea.Realizada = false;
                        db.SaveChanges();
                    }
                }
                else
                {
                    if (realizada)
                    {
                        objTarea.Realizada = true;
                        if (!objTarea.REFECHAS.Any())
                        {
                            var nuevaRefecha = new REFECHAS();
                            nuevaRefecha.Fecha = DateTime.Now;
                            nuevaRefecha.activa = true;
                            nuevaRefecha.tareaRecId = tareaRecId;
                            objTarea.fecha_vence = nuevaRefecha.Fecha;
                            objTarea.REFECHAS.Add(nuevaRefecha);
                        }
                        db.SaveChanges();
                    }
                }
                if (objTarea.fecha_vence != null)
                {
                    fechaVence = (DateTime)objTarea.fecha_vence;
                }
                return Json(new { Result = "OK", FechaVence = fechaVence.ToString("dd/MM/yyyy") });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult DeleteRefecha(int refechaId, int tareaRecId = 0)
        {
            try
            {
                if (refechaId == 0 && tareaRecId > 0)
                {
                    db = new ModelFollowTaskIt();
                    var objTarea0 = db.TAREAS.Find(tareaRecId);
                    foreach (var borrarItem in objTarea0.REFECHAS.ToList())
                    {
                        db.REFECHAS.Remove(borrarItem);
                    }
                    objTarea0.fecha_vence = null;
                    db.SaveChanges();
                    return Json(new { Result = "OK", Color = objTarea0.ccColorTarea, FechaVence = "" });
                }
                var refechaEliminar = db.REFECHAS.Find(refechaId);
                var objTarea2 = db.TAREAS.Find(refechaEliminar.tareaRecId);
                var fechaEliminando = refechaEliminar.Fecha;
                if (objTarea2.REFECHAS.Where(p => p.refechaId != refechaId).Any())
                {
                    if (objTarea2.REFECHAS.Where(p2 => p2.Fecha > fechaEliminando).Any()) {
                        return Json(new { Result = "ERROR", Message = "No puede eliminarse porque no es la ultima refecha." });
                    }

                    var refechaQueSeQueda = objTarea2.REFECHAS.Where(p => p.refechaId != refechaId).OrderByDescending(p => p.refechaId).FirstOrDefault();
                    refechaQueSeQueda.activa = true;
                    objTarea2.fecha_vence = refechaQueSeQueda.Fecha;
                }
                else
                {
                    objTarea2.fecha_vence = null;
                }
                db.REFECHAS.Remove(refechaEliminar);
                db.SaveChanges();
                db = new ModelFollowTaskIt();
                var objTarea1 = db.TAREAS.Find(objTarea2.tareaRecId);
                string fechaVence = "";
                if (objTarea2.fecha_vence != null)
                {
                    fechaVence = ((DateTime)objTarea1.fecha_vence).ToString("dd/MM/yyyy");
                }
                return Json(new { Result = "OK", Color = objTarea1.ccColorTarea, FechaVence = fechaVence });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public IQueryable<TAREAS> getResultadosTareas(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int minutaId = 0, bool soloNoRealizadas = false)
        {
            if (Id_Check_Realizadas)
            {
                Id_Check_NoVencidas = false;
                Id_Check_SinFecha = false;
                Id_Check_Vencidas = false;
            }
            var listadoTareas = db.TAREAS.AsQueryable();
            if (soloNoRealizadas)
            {
                listadoTareas = listadoTareas.Where(p => p.Realizada == false);
            }
            if (!String.IsNullOrEmpty(empleadoId))
            {
                listadoTareas = listadoTareas.Where(p => p.empleadoId == empleadoId && p.MINUTA.Activo == true);
            }

            if (departamentoId > 0)
            {
                listadoTareas = listadoTareas.Where(p => p.MINUTA.REUNION.departamentoId == departamentoId && p.MINUTA.Activo == true);
            }
            if (minutaId > 0)
            {
                listadoTareas = listadoTareas.Where(p => p.minutaId == minutaId);
            }
            if (reunionId > 0)
            {
                listadoTareas = listadoTareas.Where(p => p.MINUTA.reunionId == reunionId);
            }
            if (fechaDesde != null)
            {
                listadoTareas = listadoTareas.Where(p => p.fecha_asignada > fechaDesde);
            }
            if (fechaHasta != null)
            {
                listadoTareas = listadoTareas.Where(p => p.fecha_asignada < fechaHasta);
            }
            if (Id_Check_Realizadas)
            {
                listadoTareas = listadoTareas.Where(p => p.Realizada == true);
            }
            else
            {
                if (Id_Check_NoVencidas || Id_Check_Vencidas || Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.Realizada == false);
                }
                if (Id_Check_NoVencidas && !Id_Check_Vencidas && !Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence > DateTime.Now);
                }
                if (!Id_Check_NoVencidas && Id_Check_Vencidas && !Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence < DateTime.Now);
                }
                if (!Id_Check_NoVencidas && !Id_Check_Vencidas && Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence == null);
                }
                if (Id_Check_NoVencidas && Id_Check_Vencidas && !Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence > DateTime.Now || p.fecha_vence < DateTime.Now);
                }
                if (!Id_Check_NoVencidas && Id_Check_Vencidas && Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence < DateTime.Now || p.fecha_vence == null);
                }
                if (Id_Check_NoVencidas && !Id_Check_Vencidas && Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence > DateTime.Now || p.fecha_vence == null);
                }
                if (Id_Check_NoVencidas && Id_Check_Vencidas && Id_Check_SinFecha)
                {
                    listadoTareas = listadoTareas.Where(p => p.fecha_vence > DateTime.Now || p.fecha_vence < DateTime.Now || p.fecha_vence == null);
                }
            }

            return listadoTareas;

        }

    }
}