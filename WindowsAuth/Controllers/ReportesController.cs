﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;
using FollowTaskIt.AccesoBaseDatosV2;
using System.Collections;


namespace FollowTaskIt.Presentacion.Controllers
{

    public class ReportesController : Controller
    {
        public void TareasExcel(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int minutaId = 0)
        {
            System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = consultaReporte(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, null, null, minutaId);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Marklist.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        public void TareasPDF(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int minutaId = 0)
        {
          
            System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
            gv.DataSource = consultaReporte(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, null, null, minutaId);
            gv.DataBind();
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            gv.RenderControl(htw);

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(GetPDF_HTMLParser(sw.ToString()));
            Response.End();

        }

        private byte[] GetPDF_HTMLParser(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class
            Document doc = new Document(PageSize.LETTER, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        private byte[] GetPDF_XMLParser(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class
            Document doc = new Document(PageSize.LETTER, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 4: we open document and start the worker on the document
            doc.Open();

            XMLWorkerHelper.GetInstance().ParseXHtml(oPdfWriter, doc, txtReader);

            // 6: close the document and the worker
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }

        private IList consultaReporte(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int minutaId = 0)
        {
            var controllerTareas = new TareasJsonController();
            var listadoTareas = controllerTareas.getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, null, null, minutaId);
            var listado = listadoTareas.Select(a => new
            {
                Empleado = a.EMPLEADOS.nombre,
                Tarea = a.tarea,
                Fecha_Asignada = a.fecha_asignada,
                Fecha_Vence = a.fecha_vence,
                Realizada = ( a.Realizada ? "SI" : "NO" ),
                Nota = a.Notas
            }).ToList();

            return listado;
        }
    }
}