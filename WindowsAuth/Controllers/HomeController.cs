﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using FollowTaskIt.Presentacion.Models;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using FollowTaskIt.Models;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult ListDepartamento()
        {
            return View(db.DEPARTAMENTO.Where(a => a.Activo == true));
        }
        public ActionResult ListDepartamentoReuniones()
        {
            return View(db.DEPARTAMENTO.Where(a => a.Activo == true));
        }
        public ActionResult ListReuniones()
        {
            return View(db.REUNION.Where(a => a.estatusId == ReunionEstatus.Activo));
        }
    }
}