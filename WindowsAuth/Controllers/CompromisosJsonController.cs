﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
namespace FollowTaskIt.Presentacion.Controllers
{
    public class CompromisosJsonController : BaseJsonController
    {
        // GET: CompromisosJson
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Departamentos(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, DateTime? fechaDesde = null, DateTime? fechaHasta = null)
        {
            try
            {
                var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, 0, fechaDesde, fechaHasta);
                var listadoDepartamentos = listadoTareas.Select(p => p.MINUTA.REUNION.DEPARTAMENTO).ToList().Distinct();
                var listado = listadoDepartamentos.Select(a => new
                {
                    departamentoId = a.departamentoId,
                    nombre_depto = a.nombre_depto,
                    empleadoId = empleadoId
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre_depto).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Reuniones(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int departamentoId = 0)
        {
            try
            {
                var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, departamentoId, fechaDesde, fechaHasta);
                var listadoReuniones = listadoTareas.Select(p => p.MINUTA.REUNION).ToList().Distinct();
                var listado = listadoReuniones.Select(a => new
                {
                    reunionId = a.reunionId,
                    nombre_reunion = a.nombre_reunion,
                    empleadoId = empleadoId,
                    departamentoId = departamentoId
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre_reunion).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Tareas(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int reunionId = 0)
        {
            try
            {
                var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, reunionId, 0, fechaDesde, fechaHasta);
                var listado = listadoTareas.Select(a => new
                {
                    minutaId = a.minutaId,
                    tareaId = a.tareaId,
                    tarea = a.tarea,
                    empleadoId = a.empleadoId,
                    fecha_asignada = a.fecha_asignada,
                    fecha_vence = a.fecha_vence,
                    tareaRecId = a.tareaRecId,
                    nombre = a.EMPLEADOS.nombre,
                    ccColorTarea = a.ccColorTarea,
                    Realizada = a.Realizada,
                    Nota = a.Notas,
                    tareaperiodicaId = a.tareaperiodicaId
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.tareaId).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult EventosCalendario(string start, string end, string empleadoId)
        {
            var fechaDesde = DateTime.Parse(start);
            var fechaHasta = DateTime.Parse(end);
            var eventosCalendario = db.TAREAS.Where(p => p.fecha_asignada > fechaDesde && p.fecha_asignada < fechaHasta && p.empleadoId == empleadoId).ToList();
            var listado = new List<dynamic>();

            foreach (var item in eventosCalendario)
            {
                DateTime fecha = ((item.fecha_vence == null) ? (DateTime)item.fecha_asignada : (DateTime)item.fecha_vence);
                if (String.IsNullOrEmpty(item.hora_ini) || String.IsNullOrEmpty(item.hora_fin))
                {
                    listado.Add(new
                    {
                        title = item.tarea + (!String.IsNullOrEmpty(item.Notas) ? "<br> Notas: " + item.Notas : ""),
                        description = item.EMPLEADOS.nombre,
                        start = fecha.ToString("yyyy-MM-dd") + "T08:00:00",
                        end = fecha.ToString("yyyy-MM-dd") + "T17:00:00"
                    });
                }
                else
                {
                   listado.Add(new
                    {
                        title = item.tarea + (!String.IsNullOrEmpty(item.Notas) ? "<br> Notas: " + item.Notas : ""),
                        description = item.EMPLEADOS.nombre,
                        start = fecha.ToString("yyyy-MM-dd") + convertirHora24(item.hora_ini, true),
                        end = fecha.ToString("yyyy-MM-dd") + convertirHora24(item.hora_fin, false)
                    });

                }
            }

            return Json(listado.ToList());

        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult EventosNotificacion(int agregarMinutos)
        {
            var usuarioActual = User.Identity.GetUserName();
            var codigoEmpleado = db.EMPLEADOS.Where(p=>p.usuario == usuarioActual).Select(b=>b.empleadoId).FirstOrDefault();
            var fechaFinal = DateTime.Now.AddMinutes(agregarMinutos);
            var eventosCalendario = db.TAREAS.Where(p => p.ccFechaNotificacion >= DateTime.Now && p.ccFechaNotificacion <= fechaFinal && p.empleadoId == codigoEmpleado && p.Realizada == false).ToList();
            var listado = new List<dynamic>();

            foreach (var item in eventosCalendario)
            {
               listado.Add(new
                    {
                        title = item.EMPLEADOS.nombre,
                        description = item.tarea + (!String.IsNullOrEmpty(item.Notas) ? "<br> Notas: " + item.Notas : ""),
                        hora_ini = item.hora_ini,
                        hora_fin = item.hora_fin
                    });
            }

            return Json(listado.ToList());

        }




        private static string convertirHora24(string horaConvertir, bool esInicio = false)
        {
            string retVal = "";
            try
            {
                retVal = "T" + DateTime.Parse(horaConvertir).ToString("HH:mm:ss");
            }
            catch (Exception)
            {
                if (esInicio)
                {
                    retVal = "T08:00:00";
                }
                else
                {
                    retVal = "T17:00:00";
                }
            }

            return retVal;
        }

        //private static string convertirHora24(string horaInicio, bool esInicio = false)
        //{
        //    string retVal = "";
        //    try
        //    {
        //        //var convertidaLocalMente = convertirHora24(item.hora_ini, true);
        //        //var convertidaPorOtro = DateTime.Parse(item.hora_ini);
        //        //var otroValor = convertidaPorOtro.ToString("HH:mm:ss");
        //        char pad = '0';
        //        var resultado1 = horaInicio.Split(' ');
        //        var hora = resultado1.First();
        //        var am_pm = resultado1.Last();
        //        if (am_pm == "PM")
        //        {
        //            var resultado2 = hora.Split(':');
        //            int soloHora = int.Parse(resultado2.First());
        //            var soloMinutos = resultado2.Last();
        //            if (soloHora != 12)
        //            {
        //                soloHora += 12;
        //            }
        //            retVal = "T" + soloHora.ToString().PadLeft(2, pad) + ":" + soloMinutos.PadLeft(2, pad) + ":00";
        //        }
        //        else
        //        {
        //            var resultado2 = hora.Split(':');
        //            int soloHora = int.Parse(resultado2.First());
        //            var soloMinutos = resultado2.Last();
        //            if (soloHora == 12)
        //            {
        //                soloHora -= 12;
        //            }
        //            retVal = "T" + soloHora.ToString().PadLeft(2, pad) + ":" + soloMinutos.PadLeft(2, pad) + ":00";
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        if (esInicio)
        //        {
        //            retVal = "T08:00:00";
        //        }
        //        else
        //        {
        //            retVal = "T17:00:00";
        //        }
        //    }
        //    return retVal;
        //}

        private IQueryable<TAREAS> getResultadosTareas(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null)
        {

            var controlardorTareas = new TareasJsonController();
            int minutaId = 0;
            if (reunionId > 0)
            {
                minutaId = db.MINUTA.Where(p => p.reunionId == reunionId && p.Activo == true).FirstOrDefault().minutaId;
            }
            var listadoTareas = controlardorTareas.getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, reunionId, departamentoId, fechaDesde, fechaHasta, minutaId);

            return listadoTareas;

        }

    }
}