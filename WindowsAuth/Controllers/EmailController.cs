﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    public class EmailController
    {
            public string GmailUsername { get; set; }
            public string GmailPassword { get; set; }
            public string GmailTo { get; set; }
            public string GmailAdd { get; set; }
            public string GmailHost { get; set; }
            public int GmailPort { get; set; }
            public static bool GmailSSL { get; set; }
            public string ToEmail { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public static bool IsHtml { get; set; }

            static EmailController()
            {
                GmailSSL = true;
                IsHtml = true;
            }

            public void Send()
            {
				//SmtpClient smtp = new SmtpClient();
				//smtp.Host = GmailHost;
				//smtp.Port = GmailPort;
				//smtp.EnableSsl = GmailSSL;
				//smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
				//smtp.UseDefaultCredentials = false;
				//smtp.Credentials = new NetworkCredential(GmailUsername, GmailPassword);

				//using (var message = new MailMessage(GmailAdd,GmailTo))
				//{
				//	message.Subject = Subject;
				//	message.Body = Body;
				//	message.IsBodyHtml = IsHtml;
				//	System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
				//	System.Security.Cryptography.X509Certificates.X509Certificate certificate,
				//	System.Security.Cryptography.X509Certificates.X509Chain chain,
				//	System.Net.Security.SslPolicyErrors sslPolicyErrors)
				//	{
				//		return true;
				//	};
				//	smtp.Send(message);
				//}
            }

    }
}