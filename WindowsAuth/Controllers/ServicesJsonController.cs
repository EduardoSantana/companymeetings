﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class ServicesJsonController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();
        [HttpPost]
        public JsonResult GetTiempoAsignado(int TiempoId = 0)
        {

            var objTiempoStandard = (from data in db.TIEMPO_STANDARD
                                     where data.tiempoid == TiempoId
                                     select data).FirstOrDefault();

             var TiempoStandard = new {  //Array 
                Nombre = objTiempoStandard.EMPLEADOS.nombre,
                FechaVence = DateTime.Now.AddDays(objTiempoStandard.dias).ToString("dd/MM/yyyy")
            };

             return Json(new { TiempoStandard, JsonRequestBehavior.AllowGet });
        }
    }
}