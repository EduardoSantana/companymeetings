﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class BaseJsonController : Controller
    {
        public ModelFollowTaskIt db = new ModelFollowTaskIt();
        public JsonResult GetEmpleadosOptions(int minutaId = 0, bool soloMinuta = false, bool todosEmpleados = false)
        {
            try
            {
                var empleados = db.EMPLEADOS.Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);

                if (minutaId > 0)
                {
                    var listaEmpleadoId = db.MINUTA.Find(minutaId).PARTICIPANTES.Select(p => p.empleadoId).ToList();
                    if (todosEmpleados)
                    {
                        empleados = db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == FollowTaskIt.Enumeradores.TipoEmpleado.Empleado).Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);
                    }
                    else 
                    {
                        if (soloMinuta)
                        {
                            empleados = db.EMPLEADOS.Where(p => p.Activo == true && listaEmpleadoId.Contains(p.empleadoId)).Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);
                        }
                        else
                        {
                            empleados = db.EMPLEADOS.Where(p => p.Activo == true && !listaEmpleadoId.Contains(p.empleadoId)).Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);
                        }
                    }
                  
                }

                return Json(new { Result = "OK", Options = empleados });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}