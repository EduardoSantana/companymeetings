﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Models;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class DiasController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: Dias
        public ActionResult Index(int id = 2)
        {
            ViewBag.nombreAccion = "Index";
            var modelo = new ListadoDiasViewModel();
            modelo.listado = db.Dias.Where(p => p.PeriodoId == id).OrderBy(p => p.Nombre).Include(d => d.Periodos);
            ViewBag.periodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion", id);
            modelo.periodoId = id;
            return View(modelo);
        }

        // GET: Dias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dias dias = db.Dias.Find(id);
            if (dias == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(dias);

            return View(dias);
        }

        // GET: Dias/Create
        public ActionResult Create()
        {
            ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion");
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Dias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DiaId,Nombre,Descripcion,Dia,Orden,Activo,PeriodoId")] Dias dias)
        {
            if (ModelState.IsValid)
            {
                
                db.Dias.Add(dias);
               
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion");
                    ModelState.AddModelError("", "No se ha podido guardar Dias. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(dias);
                }
                catch (Exception ex)
                {
                    ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion");
                    ModelState.AddModelError("", "No se ha podido guardar Dias. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(dias);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(dias);

                return RedirectToAction("Index");
            }

            ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion", dias.PeriodoId);
            if (Request.IsAjaxRequest())
                return PartialView(dias);

            return View(dias);
        }

        // GET: Dias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dias dias = db.Dias.Find(id);
            if (dias == null)
            {
                return HttpNotFound();
            }
            ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion", dias.PeriodoId);
            if (Request.IsAjaxRequest())
                return PartialView(dias);

            return View(dias);
        }

        // POST: Dias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DiaId,Nombre,Descripcion,Dia,Orden,Activo,PeriodoId")] Dias dias)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dias).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.PeriodoId = new SelectList(db.Periodos, "PeriodoId", "Descripcion", dias.PeriodoId);
            if (Request.IsAjaxRequest())
                    return PartialView(dias);

            return View(dias);
        }

        // GET: Dias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dias dias = db.Dias.Find(id);
            if (dias == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(dias);

            return View(dias);
        }

        // POST: Dias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dias dias = db.Dias.Find(id);
            db.Dias.Remove(dias);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar Dias. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(dias);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
