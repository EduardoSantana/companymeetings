﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class TiposReunionesController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: TiposReuniones
        public ActionResult Index()
        {
            return View(db.REUNION_TIPO.ToList());
        }

        // GET: TiposReuniones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION_TIPO rEUNION_TIPO = db.REUNION_TIPO.Find(id);
            if (rEUNION_TIPO == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(rEUNION_TIPO);

            return View(rEUNION_TIPO);
        }

        // GET: TiposReuniones/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView();

            return View();
        }

        // POST: TiposReuniones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tipoId,Nombre")] REUNION_TIPO rEUNION_TIPO)
        {
            if (ModelState.IsValid)
            {

                db.REUNION_TIPO.Add(rEUNION_TIPO);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {

                    ModelState.AddModelError("", "No se ha podido guardar REUNION_TIPO. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(rEUNION_TIPO);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar REUNION_TIPO. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(rEUNION_TIPO);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(rEUNION_TIPO);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(rEUNION_TIPO);

            return View(rEUNION_TIPO);
        }

        // GET: TiposReuniones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION_TIPO rEUNION_TIPO = db.REUNION_TIPO.Find(id);
            if (rEUNION_TIPO == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION_TIPO);

            return View(rEUNION_TIPO);
        }

        // POST: TiposReuniones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tipoId,Nombre")] REUNION_TIPO rEUNION_TIPO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rEUNION_TIPO).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION_TIPO);

            return View(rEUNION_TIPO);
        }

        // GET: TiposReuniones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION_TIPO rEUNION_TIPO = db.REUNION_TIPO.Find(id);
            if (rEUNION_TIPO == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION_TIPO);

            return View(rEUNION_TIPO);
        }

        // POST: TiposReuniones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            REUNION_TIPO rEUNION_TIPO = db.REUNION_TIPO.Find(id);
            db.REUNION_TIPO.Remove(rEUNION_TIPO);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar REUNION_TIPO. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(rEUNION_TIPO);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
