﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Models;
using FollowTaskIt.Enumeradores;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class ReunionesController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: Reuniones
        public ActionResult Index(int id = 0)
        {
            ViewBag.nombreAccion = "Index";
            var modelo = new ListadoReunionesViewModel();
            modelo.listado = db.REUNION.Where(p=>p.estatusId == id).Include(r => r.DEPARTAMENTO).Include(r => r.REUNION_ESTATUS).Include(r => r.REUNION_TIPO).Include(r => r.EMPLEADOS).Include(r => r.EMPLEADOS1).Include(r => r.EMPLEADOS2).OrderBy(p=>p.nombre_reunion);
            ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre", id);
            modelo.estatusId = id;
            return View(modelo);
        }
        // GET: Reuniones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION rEUNION = db.REUNION.Find(id);
            if (rEUNION == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(rEUNION);

            return View(rEUNION);
        }

        // GET: Reuniones/Create
        public ActionResult Create()
        {
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
            ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre");
            ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre");
            ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
            ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
            ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
            if (Request.IsAjaxRequest())
                return PartialView();

            return View();
        }

        private void CreacionMinuta(REUNION reunion)
        {
            MINUTA item = new MINUTA();
            item.nombre = DateTime.Now.ToString("dd/MM/yyyy");
            item.reunionId = reunion.reunionId;
            item.fecha = DateTime.Now;
            item.Activo = true;
            item.abierta = false;
            reunion.MINUTA.Add(item);
        }
      
        // POST: Reuniones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "departamentoId,reunionId,nombre_reunion,usuario1,usuario2,usuario3,depto_nombre,fecha_inicio,fecha_final,fecha_cierre,tiempo,password,usopassword,estatusId,tipoId")] REUNION rEUNION)
        {
            if (ModelState.IsValid)
            {
                CreacionMinuta(rEUNION);
                rEUNION.estatusId = 0;
                db.REUNION.Add(rEUNION);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {

                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
                    ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre");
                    ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre");
                    ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ModelState.AddModelError("", "No se ha podido guardar REUNION. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(rEUNION);
                }
                catch (Exception ex)
                {
                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
                    ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre");
                    ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre");
                    ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ModelState.AddModelError("", "No se ha podido guardar REUNION. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(rEUNION);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(rEUNION);

                return RedirectToAction("Index");
            }

            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", rEUNION.departamentoId);
            ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre", rEUNION.estatusId);
            ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre", rEUNION.tipoId);
            ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario1);
            ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario2);
            ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario3);
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION);

            return View(rEUNION);
        }

        // GET: Reuniones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION rEUNION = db.REUNION.Find(id);
            if (rEUNION == null)
            {
                return HttpNotFound();
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", rEUNION.departamentoId);
            ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre", rEUNION.estatusId);
            ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre", rEUNION.tipoId);
            ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario1);
            ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario2);
            ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario3);
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION);

            return View(rEUNION);
        }

        // POST: Reuniones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "departamentoId,reunionId,nombre_reunion,usuario1,usuario2,usuario3,depto_nombre,fecha_inicio,fecha_final,fecha_cierre,tiempo,password,usopassword,estatusId,tipoId")] REUNION rEUNION)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rEUNION).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", rEUNION.departamentoId);
            ViewBag.estatusId = new SelectList(db.REUNION_ESTATUS, "estatusId", "Nombre", rEUNION.estatusId);
            ViewBag.tipoId = new SelectList(db.REUNION_TIPO, "tipoId", "Nombre", rEUNION.tipoId);
            ViewBag.usuario1 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario1);
            ViewBag.usuario2 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario2);
            ViewBag.usuario3 = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", rEUNION.usuario3);
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION);

            return View(rEUNION);
        }

        // GET: Reuniones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            REUNION rEUNION = db.REUNION.Find(id);
            if (rEUNION == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(rEUNION);

            return View(rEUNION);
        }

        // POST: Reuniones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            REUNION rEUNION = db.REUNION.Find(id);
            db.REUNION.Remove(rEUNION);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar REUNION. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(rEUNION);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
