﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    public class ParticipantesJsonController : BaseJsonController
    {
        // GET: ParticipantesJson
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Listado(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", int minutaId = 0, string searchString = "")
        {
            try
            {
                var minuta = db.MINUTA.Find(minutaId);
                var listado = minuta.PARTICIPANTES.Select(a => new
                {
                    minutaId = a.minutaId,
                    nombre_empleado = a.EMPLEADOS.nombre,
                    empleadoId = a.empleadoId,
                    asistencia = a.asistencia
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre_empleado).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Create(PARTICIPANTES participante)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                if (participante.asistencia == null)
                {
                    participante.asistencia = 0;
                }
                participante.nombre_empleado = db.EMPLEADOS.Find(participante.empleadoId).nombre;
                db.PARTICIPANTES.Add(participante);
                db.SaveChanges();
                var nuevoEmpleado = new PARTICIPANTES();
                nuevoEmpleado.empleadoId = participante.empleadoId;
                nuevoEmpleado.asistencia = participante.asistencia;
                nuevoEmpleado.nombre_empleado = participante.nombre_empleado;
                nuevoEmpleado.minutaId = participante.minutaId;
                return Json(new { Result = "OK", Record = nuevoEmpleado });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Update(int minutaId, string empleadoId, int asistencia)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                var obj = db.PARTICIPANTES.Where(p => p.minutaId == minutaId && p.empleadoId == empleadoId).FirstOrDefault();
                obj.asistencia = asistencia;
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Delete(int minutaId, string empleadoId)
        {
            try
            {
                var objMinuta = db.MINUTA.Find(minutaId);
                var tieneTareas = objMinuta.TAREAS.Where(p => p.empleadoId == empleadoId).Any();
                if (tieneTareas)
                {
                    return Json(new { Result = "ERROR", Message = "Este participante no puede ser eliminado, por tener tareas pendientes." });
                }
                var participante = objMinuta.PARTICIPANTES.Where(p => p.empleadoId == empleadoId).FirstOrDefault();
                db.PARTICIPANTES.Remove(participante);
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult AutoSave(int minutaId, string agenda, string pendientes)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                var minuta = db.MINUTA.Find(minutaId);
                minuta.agenda = agenda;
                minuta.puntos_pendientes = pendientes;
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CerrarMinuta(int minutaId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                var minuta = db.MINUTA.Find(minutaId);
                minuta.abierta = false;
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}