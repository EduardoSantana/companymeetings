﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using FollowTaskIt.Models;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class ServicesController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();
        private EmailSendController emailSend = new EmailSendController();
        // GET: Services
        public ActionResult Index(int id = 0)
        {
            if (id == 0) { id = 5; }
            ViewBag.Title = "Servicios Solicitados";
            var modelo = new ListadoServiciosViewModel();

            string usuario = User.Identity.Name.ToString();

            string solicitanteId = (from data in db.EMPLEADOS
                                    where data.usuario == usuario
                                    select data.empleadoId).FirstOrDefault();

            modelo.listado = db.SERVICIOS.Where(t => t.solicitanteid == solicitanteId);
            modelo.usuarioLogeado = solicitanteId;
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", id);
            ViewBag.nombreAccion = "Index";
            modelo.listado = modelo.listado.Where(p => p.estadoId == id).ToList();
            modelo.estadoId = id;

            return View(modelo);

        }
        public ActionResult Solicitados(int id = ServiciosEstados.Pendiente)
        {
            ViewBag.Title = "Servicios Solicitados";
            var modelo = new ListadoServiciosViewModel();

            string usuario = User.Identity.Name.ToString();

            string solicitanteId = (from data in db.EMPLEADOS
                                    where data.usuario == usuario
                                    select data.empleadoId).FirstOrDefault();

            modelo.listado = db.SERVICIOS.Where(t => t.solicitanteid == solicitanteId);
            modelo.usuarioLogeado = solicitanteId;
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", id);
            ViewBag.nombreAccion = "Solicitados";
            modelo.listado = modelo.listado.Where(p => p.estadoId == id).ToList();
            modelo.estadoId = id;
            return View("Index", modelo);
        }
        public ActionResult Completados(int id = ServiciosEstados.Completada)
        {
            ViewBag.Title = "Servicios Completados";
            var modelo = new ListadoServiciosViewModel();

            string usuario = User.Identity.Name.ToString();

            string solicitanteId = (from data in db.EMPLEADOS
                                    where data.usuario == usuario
                                    select data.empleadoId).FirstOrDefault();

            modelo.listado = db.SERVICIOS.Where(t => t.solicitanteid == solicitanteId);
            modelo.usuarioLogeado = solicitanteId;
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", id);
            ViewBag.nombreAccion = "Solicitados";
            modelo.listado = modelo.listado.Where(p => p.estadoId == id).ToList();
            modelo.estadoId = id;
            return View("Index", modelo);

        }
        public ActionResult MisServicios(int id = 0)
        {
            if (id == 0) { id = 5; }
            ViewBag.Title = "Mis Servicios Asignados";
            ViewBag.nombreAccion = "MisServicios";
            var modelo = new ListadoServiciosViewModel();

            string usuario = User.Identity.Name.ToString();

            string solicitanteId = (from data in db.EMPLEADOS
                                    where data.usuario == usuario
                                    select data.empleadoId).FirstOrDefault();

            modelo.listado = db.SERVICIOS.Where(t => t.asignadoid == solicitanteId);

            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", id);
            modelo.usuarioLogeado = solicitanteId;

            modelo.listado = modelo.listado.Where(p => p.estadoId == id).ToList();
            modelo.estadoId = id;

            return View("Index", modelo);

        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS servicio = db.SERVICIOS.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            string usuario = User.Identity.Name.ToString();
            string usuario_empleadoId = (from data in db.EMPLEADOS
                                         where data.usuario == usuario
                                         select data.empleadoId).FirstOrDefault();
            ViewBag.estaAsignado = (servicio.asignadoid == usuario_empleadoId);
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO, "departamentoId", "nombre_depto", servicio.departamentoId);
            ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.OrderBy(p => p.nombre), "empleadoId", "nombre", servicio.solicitanteid);
            ViewBag.asignadoid = new SelectList(db.EMPLEADOS.OrderBy(p => p.nombre), "empleadoId", "nombre", servicio.asignadoid);
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", servicio.estadoId);
            ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD, "tiempoid", "descripcion", servicio.tiempoid);
            if (Request.IsAjaxRequest())
                return PartialView(servicio);

            return View(servicio);
        }
        public ActionResult Create(int id = 0)
        {
            string currentUser = User.Identity.Name.ToString();
            var objSolicitante = (from data in db.EMPLEADOS
                                  where data.usuario == currentUser
                                  select data).FirstOrDefault();

            SERVICIOS modelo = new SERVICIOS();
            modelo.pcid = System.Environment.MachineName;
            modelo.solicitanteid = objSolicitante.empleadoId;
            if (id == 0)
            {
                modelo.departamentoId = objSolicitante.departamentoId;
            }
            else
            {
                modelo.departamentoId = id;
            }

            modelo.estadoId = ServiciosEstados.Pendiente;
            modelo.fecha = DateTime.Now;
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.departamentoId == modelo.departamentoId), "departamentoId", "nombre_depto", modelo.departamentoId);
            ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.empleadoId == modelo.solicitanteid), "empleadoId", "nombre", modelo.solicitanteid);
            ViewBag.asignadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true), "empleadoId", "nombre", modelo.asignadoid);
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", modelo.estadoId);
            ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD.Where(p => p.departamentoId == modelo.departamentoId && p.Activo == true), "tiempoid", "descripcion");

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }
        public ActionResult Solicitar()
        {
            return View(db.DEPARTAMENTO.Where(a => a.Activo == true && a.Servicios == true));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "servicioId,departamentoId,descripcion,pcid,solicitanteid,fecha,tiempoid,estadoId,solucion,asignadoid,fecha_vence,fecha_cierre,fecha_solucion,notificaciones,satisfaccion")] SERVICIOS modelo)
        {
            if (ModelState.IsValid)
            {

                string currentUser = User.Identity.Name.ToString();
                var objSolicitante = (from data in db.EMPLEADOS
                                      where data.usuario == currentUser
                                      select data).FirstOrDefault();
                var objTiempoStandard = (from p in db.TIEMPO_STANDARD
                                         where p.tiempoid == modelo.tiempoid
                                         select p).FirstOrDefault();
                modelo.asignadoid = objTiempoStandard.empleadoid;
                modelo.fecha_vence = DateTime.Now.AddDays(objTiempoStandard.dias);
                modelo.pcid = System.Environment.MachineName;
                modelo.solicitanteid = objSolicitante.empleadoId;
                modelo.departamentoId = objTiempoStandard.departamentoId;
                modelo.estadoId = ServiciosEstados.Pendiente;
                modelo.fecha = DateTime.Now;
                db.SERVICIOS.Add(modelo);

                try
                {
                    db.SaveChanges();
                    try
                    {
                        emailSend.EnviarCorreoServicioCreado(modelo);
                    }
                    catch (Exception ex)
                    {
                        TempData["MensajeError"] = "error|No se ha podido enviar el correo. Por los siguientes errores: ";
                        TempData["MensajeError"] += ex.Message;
                        if (ex.InnerException != null)
                        {
                            string mensajeErrorInner = ex.InnerException.ToString();
                            if (mensajeErrorInner.Length > 500)
                            {
                                mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                            }
                            if (mensajeErrorInner.Length > 0)
                            {
                                TempData["MensajeError"] += "<br> Inner Exception: " + mensajeErrorInner;
                            }
                        }

                        return RedirectToAction("Index");
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.departamentoId == modelo.departamentoId), "departamentoId", "nombre_depto", modelo.departamentoId);
                    ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.empleadoId == modelo.solicitanteid), "empleadoId", "nombre", modelo.solicitanteid);
                    ViewBag.asignadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true), "empleadoId", "nombre", modelo.asignadoid);
                    ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", modelo.estadoId);
                    ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD.Where(p => p.departamentoId == modelo.departamentoId && p.Activo == true), "tiempoid", "descripcion");

                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(modelo);
                }
                catch (Exception ex)
                {
                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.departamentoId == modelo.departamentoId), "departamentoId", "nombre_depto", modelo.departamentoId);
                    ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.empleadoId == modelo.solicitanteid), "empleadoId", "nombre", modelo.solicitanteid);
                    ViewBag.asignadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true), "empleadoId", "nombre", modelo.asignadoid);
                    ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", modelo.estadoId);
                    ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD.Where(p => p.departamentoId == modelo.departamentoId && p.Activo == true), "tiempoid", "descripcion");

                    ModelState.AddModelError("", "No se ha podido guardar SERVICIOS. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(modelo);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(modelo);

                return RedirectToAction("Index");
            }

            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.departamentoId == modelo.departamentoId), "departamentoId", "nombre_depto", modelo.departamentoId);
            ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.empleadoId == modelo.solicitanteid), "empleadoId", "nombre", modelo.solicitanteid);
            ViewBag.asignadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true), "empleadoId", "nombre", modelo.asignadoid);
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", modelo.estadoId);
            ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD.Where(p => p.departamentoId == modelo.departamentoId && p.Activo == true), "tiempoid", "descripcion");
            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS servicio = db.SERVICIOS.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            string usuario = User.Identity.Name.ToString();
            string usuario_empleadoId = (from data in db.EMPLEADOS
                                         where data.usuario == usuario
                                         select data.empleadoId).FirstOrDefault();
            ViewBag.estaAsignado = (servicio.asignadoid == usuario_empleadoId);
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO, "departamentoId", "nombre_depto", servicio.departamentoId);
            ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true).OrderBy(p => p.nombre), "empleadoId", "nombre", servicio.solicitanteid);
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", servicio.estadoId);
            ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD, "tiempoid", "descripcion", servicio.tiempoid);
            if (Request.IsAjaxRequest())
                return PartialView(servicio);

            return View(servicio);
        }

        public ActionResult Cerrar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS servicio = db.SERVICIOS.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            if (servicio.estadoId != ServiciosEstados.Completada)
            {
                return HttpNotFound();
            }
            servicio.estadoId = ServiciosEstados.Cerrada;
            servicio.fecha_cierre = DateTime.Now;
            db.SaveChanges();
            try
            {
                emailSend.EnviarCorreoServicioCerrado(servicio);
                TempData["MensajeError"] = "ok|Se ha cerrado correctamente el servicio. ";
            }
            catch (Exception ex)
            {
                TempData["MensajeError"] = "error|No se ha podido enviar el correo. Por los siguientes errores: ";
                TempData["MensajeError"] += ex.Message;
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    if (mensajeErrorInner.Length > 0)
                    {
                        TempData["MensajeError"] += "<br> Inner Exception: " + mensajeErrorInner;
                    }
                }

            }

            if (Request.IsAjaxRequest())
                return PartialView(servicio);

            return View(servicio);

        }

        // POST: SERVICIOS1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "servicioId,departamentoId,descripcion,pcid,solicitanteid,fecha,tiempoid,estadoId,solucion,asignadoid,fecha_vence,fecha_cierre,fecha_solucion,notificaciones,satisfaccion,Solucionado,Cerrado")] SERVICIOS servicio)
        {
            if (ModelState.IsValid)
            {
                var currentObj = db.SERVICIOS.Find(servicio.servicioId);
                string usuario = User.Identity.Name.ToString();
                string usuario_empleadoId = (from data in db.EMPLEADOS where data.usuario == usuario select data.empleadoId).FirstOrDefault();

                bool estaAsignado = (currentObj.asignadoid == usuario_empleadoId);
                if (estaAsignado && servicio.Solucionado)
                {
                    if (currentObj.estadoId != ServiciosEstados.Pendiente)
                    {
                        TempData["MensajeError"] = "error|El servicio no puede ser marcado como completado porque no esta pendiente.";
                        return RedirectToAction("Index");
                    }
                    currentObj.estadoId = ServiciosEstados.Completada;
                    currentObj.fecha_solucion = DateTime.Now;
                    currentObj.solucion = servicio.solucion;
                    db.SaveChanges();
                }

                try
                {
                    emailSend.EnviarCorreoServicioCompletado(currentObj);
                }
                catch (Exception ex)
                {
                    TempData["MensajeError"] = "error|No se ha podido enviar el correo. Por los siguientes errores: ";
                    TempData["MensajeError"] += ex.Message;
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        if (mensajeErrorInner.Length > 0)
                        {
                            TempData["MensajeError"] += "<br> Inner Exception: " + mensajeErrorInner;
                        }
                    }

                    return RedirectToAction("Index");
                }

                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO, "departamentoId", "nombre_depto", servicio.departamentoId);
            ViewBag.solicitanteid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true).OrderBy(p => p.nombre), "empleadoId", "nombre", servicio.solicitanteid);
            ViewBag.estadoId = new SelectList(db.SERVICIOS_ESTADOS, "estadoId", "Nombre", servicio.estadoId);
            ViewBag.tiempoid = new SelectList(db.TIEMPO_STANDARD, "tiempoid", "descripcion", servicio.tiempoid);
            if (Request.IsAjaxRequest())
                return PartialView(servicio);

            return View(servicio);
        }

        // GET: SERVICIOS1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            if (sERVICIOS == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(sERVICIOS);

            return View(sERVICIOS);
        }

        // POST: SERVICIOS1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SERVICIOS sERVICIOS = db.SERVICIOS.Find(id);
            db.SERVICIOS.Remove(sERVICIOS);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar SERVICIOS. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(sERVICIOS);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
