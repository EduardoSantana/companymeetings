﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Models;
using System.Configuration;
using FollowTaskIt.Enumeradores;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class EmpleadosController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: Empleados
        public ActionResult Index(int id = TipoEmpleado.Empleado, bool activo = true)
        {
            if (id == TipoEmpleado.Empleado)
            {
                ViewBag.Title = "Listado de Empleados";
            }
            else
            {
                ViewBag.Title = "Listado de Invitados";
            }
            var modelo = new ListadoEmpleadosViewModel();
            modelo.Listado = db.EMPLEADOS.Where(p => p.Activo == activo && p.tipoId == id).OrderBy(p => p.nombre).Include(e => e.DEPARTAMENTO4);
            modelo.Activo = activo;
            modelo.TipoId = id;
            ViewBag.TipoId = new SelectList(db.EMPLEADOS_TIPO, "EmpleadoTipoId", "Nombre", id);
            var opcionesActivo = new List<dynamic>();
            opcionesActivo.Add(new { Nombre = "Activos", Valor = "true" });
            opcionesActivo.Add(new { Nombre = "Inactivos", Valor = "false" });
            ViewBag.Activo = new SelectList(opcionesActivo, "Valor", "Nombre", activo);

            return View(modelo);
        }

        // GET: Empleados/Details/5
        public ActionResult Details(string id, string username)
        {
            if (id == null && username == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			bool notFound = false;

			EMPLEADOS eMPLEADOS = null;

			if (id != null)
			{
				eMPLEADOS = db.EMPLEADOS.Find(id);
				notFound = (eMPLEADOS == null);
			}
			else if (username != null)
			{
				eMPLEADOS = db.EMPLEADOS.Where(p => p.usuario == username).FirstOrDefault();
				notFound = (eMPLEADOS == null);
			}

			if (notFound)
			{
				return HttpNotFound();
			}

            if (Request.IsAjaxRequest())
                return PartialView(eMPLEADOS);

            return View(eMPLEADOS);
        }

        // GET: Empleados/Create
        public ActionResult Create(int? id)
        {
            var modelo = new CreacionEmpleadosViewModel();
            modelo.empleado = new EMPLEADOS();
            modelo.empleado.Activo = true;

            if (id != null)
            {
                var objMinuta = db.MINUTA.Find(id);
                if (objMinuta == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Title = "Nuevo Invitado";
                modelo.empleado.empleadoId = getCodigoInvitado();
                modelo.empleado.departamentoId = objMinuta.REUNION.departamentoId;
                modelo.empleado.usuario = getCodigoInvitado();
                modelo.minutaId = (int)id;
                modelo.esParticipante = true;
                modelo.Asistencia = 0;
                modelo.empleado.tipoId = TipoEmpleado.Invitado;
            }
            else
            {
                ViewBag.Title = "Creacion de Empleados";
                ViewBag.empleado_departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto");
                modelo.esParticipante = false;
                modelo.empleado.tipoId = TipoEmpleado.Empleado;
            }

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }

        private string getCodigoInvitado()
        {
            string codigoInvitado = ConfigurationManager.AppSettings["CodigoInvitado"];
            var maximoNumero = db.EMPLEADOS.Where(p => p.tipoId == TipoEmpleado.Invitado).Select(p => p.empleadoId.Substring(1, 8)).Max();
            int proximoNumero = 1 + Convert.ToInt32(maximoNumero);
            return codigoInvitado + proximoNumero.ToString().PadLeft(7, '0');
        }

        // POST: Empleados/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreacionEmpleadosViewModel modelo)
        {
            var empleado = modelo.empleado;

            if (ModelState.IsValid)
            {
                try
                {
                    if (modelo.esParticipante)
                    {
                        empleado.departamentoId = db.MINUTA.Find(modelo.minutaId).REUNION.departamentoId;
                        empleado.usuario = empleado.empleadoId;
                        empleado.Activo = true;
                        empleado.tipoId = TipoEmpleado.Invitado;
                        var nuevoParticipante = new PARTICIPANTES();
                        nuevoParticipante.minutaId = modelo.minutaId;
                        nuevoParticipante.empleadoId = empleado.empleadoId;
                        nuevoParticipante.nombre_empleado = empleado.nombre;
                        nuevoParticipante.asistencia = modelo.Asistencia;
                        empleado.PARTICIPANTES.Add(nuevoParticipante);
                        db.EMPLEADOS.Add(empleado);
                    }
                    else
                    {
                        empleado.tipoId = TipoEmpleado.Empleado;
                        if (!String.IsNullOrEmpty(empleado.PasswordHash))
                        {
                            empleado.PasswordHash = FollowTaskIt.Presentacion.Controllers.SHA1.Encode(empleado.PasswordHash);
                        }
                        db.EMPLEADOS.Add(empleado);
                    }
                    modelo.empleado = empleado;
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {

                    ViewBag.empleado_departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto");
                    ModelState.AddModelError("", "No se ha podido guardar EMPLEADOS. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }
                    return View(modelo);
                }
                catch (Exception ex)
                {
                    ViewBag.empleado_departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto");
                    ModelState.AddModelError("", "No se ha podido guardar EMPLEADOS. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    return View(modelo);
                }

                if (modelo.esParticipante)
                {
                    var objMinuta = db.MINUTA.Find(modelo.minutaId);
                    return RedirectToAction("Reunir", "FollowTaskIt", new { id = modelo.empleado.departamentoId, tipoId = objMinuta.REUNION.tipoId, reunionId = objMinuta.reunionId });
                }

                return RedirectToAction("Index");
            }

            ViewBag.empleado_departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto", empleado.departamentoId);
            return View(modelo);
        }

        // GET: Empleados/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLEADOS eMPLEADOS = db.EMPLEADOS.Find(id);

            if (eMPLEADOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto", eMPLEADOS.departamentoId);
            if (Request.IsAjaxRequest())
                return PartialView(eMPLEADOS);

            return View(eMPLEADOS);
        }

        // POST: Empleados/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "empleadoId,nombre,email,usuario,accesomant,PCid,codigo_grupo,desktop,notas,modifica_noticias,muestra_tres_tareas,chat,idioma,Activo,departamentoId,PasswordHash,tipoId")] EMPLEADOS empleado)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (empleado.Activo == false)
                    {
                        var tieneTareasPendientes = db.TAREAS.Where(p => p.MINUTA.Activo == true && p.empleadoId == empleado.empleadoId && p.Realizada == false).Any();

                        if (tieneTareasPendientes)
                        {
                            throw new Exception("Tiene tareas pendientes. No puede ser desactivado. ");
                        }
                    }
                    if (!String.IsNullOrEmpty(empleado.PasswordHash))
                    {
                        var nuevaDB = new ModelFollowTaskIt();
                        string originalPasswordHash = nuevaDB.EMPLEADOS.Find(empleado.empleadoId).PasswordHash;
                        if (String.IsNullOrEmpty(originalPasswordHash) || originalPasswordHash != empleado.PasswordHash)
                        {
                            empleado.PasswordHash = FollowTaskIt.Presentacion.Controllers.SHA1.Encode(empleado.PasswordHash);
                        }
                    }
                    db.Entry(empleado).State = EntityState.Modified;
                    db.SaveChanges();
                    if (Request.IsAjaxRequest())
                        return PartialView();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar EMPLEADOS. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    if (empleado.Activo == false)
                    {
                        var tieneTareasPendientes = db.TAREAS.Where(p => p.MINUTA.Activo == true && p.empleadoId == empleado.empleadoId && p.Realizada == false).FirstOrDefault();

                        if (tieneTareasPendientes != null)
                        {
                            // throw new Exception("Tiene Tareas Pendientes. No puede ser Desactivado.; );
                            ModelState.AddModelError("", "Reunion: " + tieneTareasPendientes.MINUTA.REUNION.nombre_reunion);
                            ModelState.AddModelError("", "Minuta: " + tieneTareasPendientes.MINUTA.nombre);
                            ModelState.AddModelError("", "Tarea Pendiente: " + tieneTareasPendientes.tarea);
                        }
                    }
                }
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p => p.Activo == true).OrderBy(p => p.nombre_depto), "departamentoId", "nombre_depto", empleado.departamentoId);
            if (Request.IsAjaxRequest())
                return PartialView(empleado);

            return View(empleado);
        }

        // GET: Empleados/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLEADOS eMPLEADOS = db.EMPLEADOS.Find(id);
            if (eMPLEADOS == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(eMPLEADOS);

            return View(eMPLEADOS);
        }

        // POST: Empleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            EMPLEADOS eMPLEADOS = db.EMPLEADOS.Find(id);
            db.EMPLEADOS.Remove(eMPLEADOS);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar EMPLEADOS. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(eMPLEADOS);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
