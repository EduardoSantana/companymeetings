﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    public class IndicadoresJsonController : BaseJsonController
    {

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoReuniones(string empleadoId, int anoNumero = 2016)
        {
            try
            {
                var listado = vista_IndicadoresReuniones.Load(anoNumero, empleadoId);
                return Json(new { Result = "OK", Records = listado.Skip(0).Take(99), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoServicios(string empleadoId, int anoNumero = 2016)
        {
            try
            {
                var listado = vista_IndicadoresServicios.Load(anoNumero, empleadoId);
                return Json(new { Result = "OK", Records = listado.Skip(0).Take(99), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoProyectos1Empleados(string empleadoSeleccionado, int estatusId)
        {
            try
            {
                var listado = vista_IndicadoresProyectos.Load(empleadoSeleccionado,estatusId);
                var listado2 = listado.Select(p => new
                {
                    empleadoId = p.empleadoId,
                    NombreEmpleado = "Responsable: " + p.NombreEmpleado
                }).ToList().Distinct();
                return Json(new { Result = "OK", Records = listado2.Skip(0).Take(99), TotalRecordCount = listado2.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult ListadoProyectos2Reuniones(string empleadoSeleccionado, int estatusId, string empleadoId)
        {
            try
            {
                var listado = vista_IndicadoresProyectos.Load(empleadoSeleccionado, estatusId, empleadoId);
                return Json(new { Result = "OK", Records = listado.Skip(0).Take(99), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        //[HttpPost]
        //[OutputCache(Duration = 10, VaryByParam = "*")]
        //public JsonResult Reuniones(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, DateTime? fechaDesde = null, DateTime? fechaHasta = null, int departamentoId = 0)
        //{
        //    try
        //    {
        //        var listadoTareas = getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, 0, departamentoId, fechaDesde, fechaHasta);
        //        var listadoReuniones = listadoTareas.Select(p => p.MINUTA.REUNION).ToList().Distinct();
        //        var listado = listadoReuniones.Select(a => new
        //        {
        //            reunionId = a.reunionId,
        //            nombre_reunion = a.nombre_reunion,
        //            empleadoId = empleadoId,
        //            departamentoId = departamentoId
        //        });

        //        return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre_reunion).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        //private IQueryable<TAREAS> getResultadosTareas(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", string empleadoId = "", bool Id_Check_Realizadas = false, bool Id_Check_NoVencidas = false, bool Id_Check_Vencidas = false, bool Id_Check_SinFecha = false, int reunionId = 0, int departamentoId = 0, DateTime? fechaDesde = null, DateTime? fechaHasta = null)
        //{

        //    var controlardorTareas = new TareasJsonController();
        //    int minutaId = 0;
        //    if (reunionId > 0)
        //    {
        //        minutaId = db.MINUTA.Where(p => p.reunionId == reunionId && p.Activo == true).FirstOrDefault().minutaId;
        //    }
        //    var listadoTareas = controlardorTareas.getResultadosTareas(jtStartIndex, jtPageSize, jtSorting, empleadoId, Id_Check_Realizadas, Id_Check_NoVencidas, Id_Check_Vencidas, Id_Check_SinFecha, reunionId, departamentoId, fechaDesde, fechaHasta, minutaId);

        //    return listadoTareas;

        //}

    }
}