﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Models;
using FollowTaskIt.Enumeradores;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class TipoServiciosController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: TipoServicios
        public ActionResult Index(int id = 0)
        {
            var modelo = new ListadoTiempoStandardViewModel();
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo == true), "departamentoId", "nombre_depto", id);
            modelo.departamentoId = id;
            ViewBag.nombreAccion = "Index";
            if (id > 0)
            {
                modelo.listado = db.TIEMPO_STANDARD.Where(p => p.departamentoId == id).Include(t => t.DEPARTAMENTO).Include(t => t.EMPLEADOS);
            }
            else
            {
                modelo.listado = db.TIEMPO_STANDARD.Include(t => t.DEPARTAMENTO).Include(t => t.EMPLEADOS);
            }

            return View(modelo);
        }

        // GET: TipoServicios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIEMPO_STANDARD tIEMPO_STANDARD = db.TIEMPO_STANDARD.Find(id);
            if (tIEMPO_STANDARD == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(tIEMPO_STANDARD);

            return View(tIEMPO_STANDARD);
        }

        // GET: TipoServicios/Create
        public ActionResult Create()
        {
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
            if (Request.IsAjaxRequest())
                return PartialView();

            return View();
        }

        // POST: TipoServicios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tiempoid,departamentoId,descripcion,dias,empleadoid,depto_nombre,empleadoNombre,minutos,Activo")] TIEMPO_STANDARD tIEMPO_STANDARD)
        {
            if (ModelState.IsValid)
            {

                db.TIEMPO_STANDARD.Add(tIEMPO_STANDARD);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {

                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
                    ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ModelState.AddModelError("", "No se ha podido guardar TIEMPO_STANDARD. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(tIEMPO_STANDARD);
                }
                catch (Exception ex)
                {
                    ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto");
                    ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre");
                    ModelState.AddModelError("", "No se ha podido guardar TIEMPO_STANDARD. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(tIEMPO_STANDARD);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(tIEMPO_STANDARD);

                return RedirectToAction("Index");
            }

            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", tIEMPO_STANDARD.departamentoId);
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", tIEMPO_STANDARD.empleadoid);
            if (Request.IsAjaxRequest())
                return PartialView(tIEMPO_STANDARD);

            return View(tIEMPO_STANDARD);
        }

        // GET: TipoServicios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIEMPO_STANDARD tIEMPO_STANDARD = db.TIEMPO_STANDARD.Find(id);
            if (tIEMPO_STANDARD == null)
            {
                return HttpNotFound();
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", tIEMPO_STANDARD.departamentoId);
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", tIEMPO_STANDARD.empleadoid);
            if (Request.IsAjaxRequest())
                return PartialView(tIEMPO_STANDARD);

            return View(tIEMPO_STANDARD);
        }

        // POST: TipoServicios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tiempoid,departamentoId,descripcion,dias,empleadoid,depto_nombre,empleadoNombre,minutos,Activo")] TIEMPO_STANDARD tIEMPO_STANDARD)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tIEMPO_STANDARD).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.departamentoId = new SelectList(db.DEPARTAMENTO.Where(p=>p.Activo==true).OrderBy(p=>p.nombre_depto), "departamentoId", "nombre_depto", tIEMPO_STANDARD.departamentoId);
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p=>p.Activo==true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p=>p.nombre), "empleadoId", "nombre", tIEMPO_STANDARD.empleadoid);
            if (Request.IsAjaxRequest())
                return PartialView(tIEMPO_STANDARD);

            return View(tIEMPO_STANDARD);
        }

        // GET: TipoServicios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIEMPO_STANDARD tIEMPO_STANDARD = db.TIEMPO_STANDARD.Find(id);
            if (tIEMPO_STANDARD == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(tIEMPO_STANDARD);

            return View(tIEMPO_STANDARD);
        }

        // POST: TipoServicios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TIEMPO_STANDARD tIEMPO_STANDARD = db.TIEMPO_STANDARD.Find(id);
            db.TIEMPO_STANDARD.Remove(tIEMPO_STANDARD);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar TIEMPO_STANDARD. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(tIEMPO_STANDARD);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
