﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class ParametrosController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: Parametros
        public ActionResult Index()
        {
            return View(db.PARAMETROS.ToList());
        }

        // GET: Parametros/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARAMETROS pARAMETROS = db.PARAMETROS.Find(id);
            if (pARAMETROS == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                    return PartialView(pARAMETROS);

            return View(pARAMETROS);
        }

        // GET: Parametros/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                    return PartialView();

            return View();
        }

        // POST: Parametros/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Parametros_Id,formato_fecha,nombre_empresa,servidor_email,email_operador,direccion,formato_fecha_display,puerto_chat,secuencia_auto,usuario_email,clave_email,Version,puerto_email")] PARAMETROS pARAMETROS)
        {
            if (ModelState.IsValid)
            {
                
                db.PARAMETROS.Add(pARAMETROS);
               
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    
                    ModelState.AddModelError("", "No se ha podido guardar PARAMETROS. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(pARAMETROS);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido guardar PARAMETROS. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(pARAMETROS);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(pARAMETROS);

                return RedirectToAction("Index");
            }

            if (Request.IsAjaxRequest())
                return PartialView(pARAMETROS);

            return View(pARAMETROS);
        }

        // GET: Parametros/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARAMETROS pARAMETROS = db.PARAMETROS.Find(id);
            if (pARAMETROS == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(pARAMETROS);

            return View(pARAMETROS);
        }

        // POST: Parametros/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Parametros_Id,formato_fecha,nombre_empresa,servidor_email,email_operador,direccion,formato_fecha_display,puerto_chat,secuencia_auto,usuario_email,clave_email,Version,puerto_email")] PARAMETROS pARAMETROS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pARAMETROS).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                    return PartialView(pARAMETROS);

            return View(pARAMETROS);
        }

        // GET: Parametros/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PARAMETROS pARAMETROS = db.PARAMETROS.Find(id);
            if (pARAMETROS == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(pARAMETROS);

            return View(pARAMETROS);
        }

        // POST: Parametros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PARAMETROS pARAMETROS = db.PARAMETROS.Find(id);
            db.PARAMETROS.Remove(pARAMETROS);
            
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido Eliminar PARAMETROS. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    
                    return View(pARAMETROS);
                }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
