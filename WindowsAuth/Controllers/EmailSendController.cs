﻿using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    public class EmailSendController : Controller
    {
     
        private string getRutaArchivo(int tipo)
        {
            string retVal = "";
            switch (tipo)
            {
                case RutaArchivoCorreo.Solictud:
                    retVal = ConfigurationManager.AppSettings["RutaArchivoCorreoSolictud"];
                    //retVal = "\\App_Data\\Templates\\Solicitud.txt";
                    break;
                case RutaArchivoCorreo.Completada:
                    retVal = ConfigurationManager.AppSettings["RutaArchivoCorreoCompletada"];
                    //retVal = "\\App_Data\\Templates\\SolicitudCompletada.txt";
                    break;
                case RutaArchivoCorreo.Cerrada:
                    retVal = ConfigurationManager.AppSettings["RutaArchivoCorreoCerrada"];
                    //retVal = "\\App_Data\\Templates\\SolicitudCerrada.txt";
                    break;
            }

            return retVal;
        }
        // GET: EmailSend
        private ModelFollowTaskIt db = new ModelFollowTaskIt();
        #region Enviar Correos
        public void EnviarCorreoServicioCompletado(SERVICIOS servicios)
        {
            if (servicios.estadoId == ServiciosEstados.Completada)
            {
                string body;
                var variablePath = System.Web.HttpContext.Current.Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Completada));
                var sr = new System.IO.StreamReader(variablePath);
                //var sr = new System.IO.StreamReader(Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Completada)));
                body = sr.ReadToEnd();

                var EmailAdd = from data in db.EMPLEADOS
                               where data.empleadoId == servicios.asignadoid
                               select data.email;

                var objSolicitante = (from data in db.EMPLEADOS
                                      where data.empleadoId == servicios.solicitanteid
                                      select data).FirstOrDefault();

                var EmailTo = objSolicitante.email;


                var EmailPort = from data in db.PARAMETROS
                                select data.puerto_email;

                var EmailHost = from data in db.PARAMETROS
                                select data.servidor_email;

                var EmailParametros = from data in db.PARAMETROS select data.email_operador;

                var Emailuser = from data in db.PARAMETROS
                                select data.usuario_email;

                var EmailPassword = from data in db.PARAMETROS
                                    select data.clave_email;

                int tiempoid = Convert.ToInt32(servicios.tiempoid);

                var tiposervicio = from data in db.TIEMPO_STANDARD
                                   where data.tiempoid == tiempoid
                                   select data.descripcion;

                string messageBody = string.Format(body, objSolicitante.nombre, tiposervicio.FirstOrDefault(), servicios.descripcion, servicios.fecha_vence, servicios.solucion, servicios.servicioId);
                var sendEmail = new EmailController();
                sendEmail.GmailAdd = EmailParametros.FirstOrDefault();
                sendEmail.GmailTo = EmailTo + "," + EmailAdd.FirstOrDefault();
                sendEmail.Subject = "Solicitud de Servicios Completado ";
                sendEmail.Body = messageBody;
                sendEmail.GmailPort = Convert.ToInt32(EmailPort.FirstOrDefault());
                sendEmail.GmailHost = EmailHost.FirstOrDefault();
                sendEmail.GmailUsername = Emailuser.FirstOrDefault();
                sendEmail.GmailPassword = EmailPassword.FirstOrDefault();
                sendEmail.Send();
            }
        }
        public void EnviarCorreoServicioCerrado(SERVICIOS servicios)
        {
            if (servicios.estadoId == ServiciosEstados.Cerrada)
            {
                string body;
                var variablePath = System.Web.HttpContext.Current.Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Cerrada));
                var sr = new System.IO.StreamReader(variablePath);
                //var sr = new System.IO.StreamReader(Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Cerrada)));
                body = sr.ReadToEnd();

                var EmailAdd = from data in db.EMPLEADOS
                               where data.empleadoId == servicios.asignadoid
                               select data.email;

                var objSolicitante = (from data in db.EMPLEADOS
                                      where data.empleadoId == servicios.solicitanteid
                                      select data).FirstOrDefault();

                var EmailTo = objSolicitante.email;


                var EmailPort = from data in db.PARAMETROS
                                select data.puerto_email;

                var EmailHost = from data in db.PARAMETROS
                                select data.servidor_email;

                var EmailParametros = from data in db.PARAMETROS select data.email_operador;

                var Emailuser = from data in db.PARAMETROS
                                select data.usuario_email;

                var EmailPassword = from data in db.PARAMETROS
                                    select data.clave_email;

                int tiempoid = Convert.ToInt32(servicios.tiempoid);

                var tiposervicio = from data in db.TIEMPO_STANDARD
                                   where data.tiempoid == tiempoid
                                   select data.descripcion;

                string messageBody = string.Format(body, objSolicitante.nombre, tiposervicio.FirstOrDefault(), servicios.descripcion, servicios.fecha_vence, servicios.solucion, servicios.servicioId);
                var sendEmail = new EmailController();
                sendEmail.GmailAdd = EmailParametros.FirstOrDefault();
                sendEmail.GmailTo = EmailTo + "," + EmailAdd.FirstOrDefault();
                sendEmail.Subject = "Solicitud de Servicios Cerrado ";
                sendEmail.Body = messageBody;
                sendEmail.GmailPort = Convert.ToInt32(EmailPort.FirstOrDefault());
                sendEmail.GmailHost = EmailHost.FirstOrDefault();
                sendEmail.GmailUsername = Emailuser.FirstOrDefault();
                sendEmail.GmailPassword = EmailPassword.FirstOrDefault();
                sendEmail.Send();
            }
        }
        public void EnviarCorreoServicioCreado(SERVICIOS servicios)
        {

            var variablePath = System.Web.HttpContext.Current.Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Solictud));
            var sr = new System.IO.StreamReader(variablePath);
            //var sr = new System.IO.StreamReader(Server.MapPath(getRutaArchivo(RutaArchivoCorreo.Solictud)));
            string body = sr.ReadToEnd();

            var EmailAdd = from data in db.EMPLEADOS
                           where data.empleadoId == servicios.asignadoid
                           select data.email;

            var objSolicitante = (from data in db.EMPLEADOS
                                  where data.empleadoId == servicios.solicitanteid
                                  select data).FirstOrDefault();

            var EmailTo = objSolicitante.email;


            var EmailPort = from data in db.PARAMETROS
                            select data.puerto_email;

            var EmailHost = from data in db.PARAMETROS
                            select data.servidor_email;

            var Emailuser = from data in db.PARAMETROS
                            select data.usuario_email;

            var EmailParametros = from data in db.PARAMETROS select data.email_operador;

            var EmailPassword = from data in db.PARAMETROS
                                select data.clave_email;

            int tiempoid = Convert.ToInt32(servicios.tiempoid);

            var tiposervicio = from data in db.TIEMPO_STANDARD
                               where data.tiempoid == tiempoid
                               select data.descripcion;

            string messageBody = string.Format(body, objSolicitante.nombre, tiposervicio.FirstOrDefault(), servicios.descripcion, servicios.fecha_vence, servicios.servicioId);
            var sendEmail = new EmailController();
            sendEmail.GmailAdd = EmailParametros.FirstOrDefault();
            sendEmail.GmailTo = EmailTo + "," + EmailAdd.FirstOrDefault();
            sendEmail.Subject = "Solicitud de Servicios ";
            sendEmail.Body = messageBody;
            sendEmail.GmailPort = Convert.ToInt32(EmailPort.FirstOrDefault());
            sendEmail.GmailHost = EmailHost.FirstOrDefault();
            sendEmail.GmailUsername = Emailuser.FirstOrDefault();
            sendEmail.GmailPassword = EmailPassword.FirstOrDefault();
            sendEmail.Send();
        }
        #endregion
    }
}