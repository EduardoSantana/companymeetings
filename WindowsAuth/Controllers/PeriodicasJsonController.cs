﻿using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    public class PeriodicasJsonController : BaseJsonController
    {
        [HttpPost]
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public JsonResult Listado(int jtStartIndex = 0, int jtPageSize = 99, string jtSorting = "", int reunionId = 0, string searchString = "")
        {
            try
            {
                var reunion = db.REUNION.Find(reunionId);
                var listado = reunion.TAREAS_PERIODICAS.Select(a => new
                {
                    reunionId = a.reunionId,
                    tareaperiodicaId = a.tareaperiodicaId,
                    empleadoId = a.empleadoId,
                    nombre = a.EMPLEADOS.nombre,
                    tarea = a.tarea,
                    proxima_ejecucion = a.proxima_ejecucion,
                    Activa = a.Activa,
                    PeriodoId = a.PeriodoId,
                    DiaId = a.DiaId
                });

                return Json(new { Result = "OK", Records = listado.OrderBy(orden => orden.nombre).Skip(jtStartIndex).Take(jtPageSize), TotalRecordCount = listado.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Create(TAREAS_PERIODICAS periodica)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                if (periodica.proxima_ejecucion == null) {
                    periodica.proxima_ejecucion = DateTime.Now;
                }
                if (periodica.proxima_ejecucion < DateTime.Now)
                {
                    return Json(new { Result = "ERROR", Message = "La fecha de la proxima ejecucion no puede ser menor a la fecha actual." });
                }
                periodica.Activa = true;
                periodica.fechaInsertar = (periodica.proxima_ejecucion).AddDays(CantidadDias.InsertarPeriodicas);
                periodica.AuditoriaCrear(User.Identity.Name.ToString());
                db.TAREAS_PERIODICAS.Add(periodica);
                db.PARAMETROS.FirstOrDefault().TareaPeriodicaEjecutada = null;
                db.SaveChanges();
                var tempPeriodica = new TAREAS_PERIODICAS();
                tempPeriodica.empleadoId = periodica.empleadoId;
                tempPeriodica.proxima_ejecucion = periodica.proxima_ejecucion;
                tempPeriodica.reunionId = periodica.reunionId;
                tempPeriodica.tarea = periodica.tarea;
                tempPeriodica.PeriodoId = periodica.PeriodoId;
                tempPeriodica.DiaId = periodica.DiaId;
                tempPeriodica.Activa = true;
            

                return Json(new { Result = "OK", Record = tempPeriodica });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Update(TAREAS_PERIODICAS periodica)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                if (periodica.proxima_ejecucion == null)
                {
                    periodica.proxima_ejecucion = DateTime.Now;
                }
                if (periodica.proxima_ejecucion < DateTime.Now)
                {
                    return Json(new { Result = "ERROR", Message = "La fecha de la proxima ejecucion no puede ser menor a la fecha actual." });
                }
                var objPeriodica = db.TAREAS_PERIODICAS.Find(periodica.tareaperiodicaId);
                objPeriodica.empleadoId = periodica.empleadoId;
                objPeriodica.tarea = periodica.tarea;
                objPeriodica.Activa = periodica.Activa;
                objPeriodica.proxima_ejecucion = periodica.proxima_ejecucion;
                objPeriodica.PeriodoId = periodica.PeriodoId;
                objPeriodica.DiaId = periodica.DiaId;
                objPeriodica.fechaInsertar = (objPeriodica.proxima_ejecucion).AddDays(CantidadDias.InsertarPeriodicas);
                objPeriodica.AuditoriaEditar(User.Identity.Name.ToString());
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult Delete(int tareaperiodicaId)
        {
            try
            {
                var eliminarPeriodica = db.TAREAS_PERIODICAS.Find(tareaperiodicaId);
                db.TAREAS_PERIODICAS.Remove(eliminarPeriodica);
                db.SaveChanges();
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetPeriodosOptions()
        {
            try
            {
                var periodos = db.Periodos.Where(p => p.Activo == true).OrderBy(p => p.Orden).Select(c => new { DisplayText = c.Descripcion, Value = c.PeriodoId }).OrderBy(p => p.DisplayText);

                return Json(new { Result = "OK", Options = periodos });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetDiasOptions(int PeriodoId = 0)
        {
            try
            {
                if (PeriodoId > 0)
                {
                    var otro1 = db.Dias.Where(p => p.Activo == true && p.PeriodoId == PeriodoId).OrderBy(p => p.Orden).Select(c => new { DisplayText = c.Nombre, Value = c.DiaId });
                    return Json(new { Result = "OK", Options = otro1 });
                }
                else
                {
                    var otro2 = db.Dias.Where(p => p.Activo == true).OrderBy(p => p.Orden).Select(c => new { DisplayText = c.Nombre, Value = c.DiaId });
                    return Json(new { Result = "OK", Options = otro2 });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}