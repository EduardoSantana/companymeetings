﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using FollowTaskIt.Models;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class DepartamentosController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();

        // GET: Departamentos
        public ActionResult Index(bool id = true)
        {
            var listadoActivo = new List<dynamic>();
            listadoActivo.Add(new { Valor = "False", Nombre = "Inactivos" });
            listadoActivo.Add(new { Valor = "True", Nombre = "Activos" });
            ViewBag.Activo = new SelectList(listadoActivo.ToList(), "Valor", "Nombre", id);
            var modelo = new ListadoDepartamentosViewModel();
            modelo.Listado = db.DEPARTAMENTO.Where(p => p.Activo == id).Include(d => d.EMPLEADOS);
            modelo.Activo = id;
            return View(modelo);
        }

        // GET: Departamentos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTO dEPARTAMENTO = db.DEPARTAMENTO.Find(id);
            if (dEPARTAMENTO == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
                return PartialView(dEPARTAMENTO);

            return View(dEPARTAMENTO);
        }

        // GET: Departamentos/Create
        public ActionResult Create()
        {
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre");
            ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre");
            ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre");
            ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre");

            if (Request.IsAjaxRequest())
                return PartialView();

            return View();
        }

        // POST: Departamentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "departamentoId,nombre_depto,noticias,empleadoid,Activo,Servicios,AdministradorId1, AdministradorId2, AdministradorId3")] DEPARTAMENTO dEPARTAMENTO)
        {
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(dEPARTAMENTO.iconoBoostrap))
                {
                    dEPARTAMENTO.iconoBoostrap = @"<i class=""fa fa-group fa-3x""></i> ";
                }
                db.DEPARTAMENTO.Add(dEPARTAMENTO);

                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId1);
                    ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId2);
                    ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId3);
                    ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.empleadoid);

                    ModelState.AddModelError("", "No se ha podido guardar DEPARTAMENTO. Por los siguientes errores: ");
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }

                    return View(dEPARTAMENTO);
                }
                catch (Exception ex)
                {
                    ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId1);
                    ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId2);
                    ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId3);
                    ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.empleadoid);

                    ModelState.AddModelError("", "No se ha podido guardar DEPARTAMENTO. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }

                    return View(dEPARTAMENTO);
                }

                if (Request.IsAjaxRequest())
                    return PartialView(dEPARTAMENTO);

                return RedirectToAction("Index");
            }
            ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId1);
            ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId2);
            ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId3);
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.empleadoid);

            return View(dEPARTAMENTO);
        }

        // GET: Departamentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTO dEPARTAMENTO = db.DEPARTAMENTO.Find(id);
            if (dEPARTAMENTO == null)
            {
                return HttpNotFound();
            }
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.empleadoid);
            ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId1);
            ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId2);
            ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId3);

            if (Request.IsAjaxRequest())
                return PartialView(dEPARTAMENTO);

            return View(dEPARTAMENTO);
        }

        // POST: Departamentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "departamentoId,nombre_depto,noticias,empleadoid,Activo,Servicios,AdministradorId1, AdministradorId2, AdministradorId3")] DEPARTAMENTO dEPARTAMENTO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dEPARTAMENTO).State = EntityState.Modified;
                db.SaveChanges();
                if (Request.IsAjaxRequest())
                    return PartialView();
                return RedirectToAction("Index");
            }
            ViewBag.empleadoid = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.empleadoid);
            ViewBag.AdministradorId1 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId1);
            ViewBag.AdministradorId2 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId2);
            ViewBag.AdministradorId3 = new SelectList(db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == TipoEmpleado.Empleado).OrderBy(p => p.nombre), "empleadoId", "nombre", dEPARTAMENTO.AdministradorId3);
            if (Request.IsAjaxRequest())
                return PartialView(dEPARTAMENTO);

            return View(dEPARTAMENTO);
        }

        // GET: Departamentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEPARTAMENTO dEPARTAMENTO = db.DEPARTAMENTO.Find(id);
            if (dEPARTAMENTO == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest())
                return PartialView(dEPARTAMENTO);

            return View(dEPARTAMENTO);
        }

        // POST: Departamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DEPARTAMENTO dEPARTAMENTO = db.DEPARTAMENTO.Find(id);
            db.DEPARTAMENTO.Remove(dEPARTAMENTO);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "No se ha podido Eliminar DEPARTAMENTO. Por los siguientes errores: ");
                ModelState.AddModelError("", ex.Message);
                if (ex.InnerException != null)
                {
                    string mensajeErrorInner = ex.InnerException.ToString();
                    if (mensajeErrorInner.Length > 500)
                    {
                        mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                    }
                    ModelState.AddModelError("", mensajeErrorInner);
                }

                return View(dEPARTAMENTO);
            }


            if (Request.IsAjaxRequest())
                return PartialView();
            return RedirectToAction("Index");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
