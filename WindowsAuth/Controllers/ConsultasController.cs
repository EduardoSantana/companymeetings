﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class ConsultasController : Controller
    {
        public ModelFollowTaskIt db = new ModelFollowTaskIt();
        // GET: Consultas
        public ActionResult Compromisos()
        {
            string usuario = User.Identity.Name.ToString();
            var codigoEmpleadoId = db.EMPLEADOS.Where(p => p.usuario == usuario).Select(p => p.empleadoId).FirstOrDefault();
            var empleados = db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == FollowTaskIt.Enumeradores.TipoEmpleado.Empleado).Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);
            ViewBag.currentEmpleadoId = new SelectList(empleados, "Value", "DisplayText", codigoEmpleadoId);
            ViewBag.currentUser = codigoEmpleadoId;
            return View();
        }

        public ActionResult Indicadores()
        {
            string usuario = User.Identity.Name.ToString();
            var codigoEmpleadoId = db.EMPLEADOS.Where(p => p.usuario == usuario).Select(p => p.empleadoId).FirstOrDefault();
            var empleados = db.EMPLEADOS.Where(p => p.Activo == true && p.tipoId == FollowTaskIt.Enumeradores.TipoEmpleado.Empleado).Select(c => new { DisplayText = c.nombre, Value = c.empleadoId }).OrderBy(p => p.DisplayText);
            ViewBag.currentEmpleadoId = new SelectList(empleados, "Value", "DisplayText", codigoEmpleadoId);
            ViewBag.currentUser = codigoEmpleadoId; 
            return View();
        }

    }
}