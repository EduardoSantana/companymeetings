﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using FollowTaskIt.Models;
using MvcSiteMapProvider;
using Microsoft.AspNet.Identity;
using FollowTaskIt.Enumeradores;

namespace FollowTaskIt.Presentacion.Controllers
{
    [Authorize]
    public class MeetingsController : Controller
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();
        private EMPLEADOS _currentUser { get; set; }
        public ActionResult Index()
        {
            ViewBag.Title = "Listado de los departamentos con reuniones.";
            //var listaDepartamentos = db.DEPARTAMENTO.Where(a => a.Activo == true && a.REUNION.Any()).Include(d => d.REUNION);
            var listaDepartamentos = db.DEPARTAMENTO.Where(a => a.Activo == true && a.REUNION.Where(p => p.estatusId == ReunionEstatus.Activo).Any());
            return View(listaDepartamentos.OrderBy(p => p.nombre_depto));
        }
        public ActionResult Departamento(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var obj = db.DEPARTAMENTO.Find((int)id);
            if (obj == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Title = "Listado de tipos de reuniones de " + obj.nombre_depto + ".";

            var retVal = (from tipo in db.REUNION_TIPO
                          from reunion in tipo.REUNION
                          where reunion.departamentoId == id && reunion.tipoId != FollowTaskIt.Enumeradores.ReunionTipo.Servicio && tipo.REUNION.Where(p => p.estatusId == ReunionEstatus.Activo).Any()
                          group tipo by new
                          {
                              tipoId = tipo.tipoId,
                              Nombre = tipo.Nombre,
                              iconoBoostrap = tipo.iconoBoostrap
                          } into newGroup
                          select new FollowTaskIt.Models.ListadoTiposDespartamentoViewModel
                          {
                              tipoId = newGroup.Key.tipoId,
                              Nombre = newGroup.Key.Nombre,
                              iconoBoostrap = newGroup.Key.iconoBoostrap,
                              departamentoId = (int)id,
                              contador = newGroup.Count() //(from p in u.REUNION where p.departamentoId == id select p).Count,
                          }).ToList();

            SetBreadCrumb("Departamento", obj.nombre_depto);
            return View(retVal);
        }
        public ActionResult Reunir(int? reunionId, bool preguntar = true)
        {
            ViewBag.preguntar = preguntar;
            var modelo = new EditarMinutaViewModel();
            modelo.currentUserId = currentUser().empleadoId;
            modelo.vistaTipo = VistaTipo.Reunir;
            if (reunionId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            REUNION reunion = db.REUNION.Find(reunionId);
            if (reunion == null)
            {
                return HttpNotFound();
            }
            modelo.reunion = reunion;
            modelo.minuta = reunion.MINUTA.OrderByDescending(p => p.minutaId).FirstOrDefault();
            ViewBag.empleadoId = new SelectList(modelo.minuta.PARTICIPANTES.Where(p => p.EMPLEADOS.Activo == true).OrderBy(p => p.nombre_empleado).Select(p => new { empleadoId = p.empleadoId, nombre = p.EMPLEADOS.nombre }), "empleadoId", "nombre");
            ViewBag.solicitante = new SelectList(db.EMPLEADOS.Where(p => p.tipoId == TipoEmpleado.Empleado && p.Activo == true).OrderBy(p => p.nombre).Select(p => new { empleadoId = p.empleadoId, nombre = p.nombre }), "nombre", "nombre", currentUser().nombre);
            modelo.soloLectura = MinutaEsSoloLectura(modelo.minuta);
            if (!modelo.soloLectura.soloLectura && modelo.minuta.fecha < DateTime.Now.Date)
            {
                modelo.mostrarPopUp = true;
            }
            if (modelo.reunion.tiempo != null)
            {
                modelo.minutos = (int)modelo.reunion.tiempo;
            }
            else
            {
                modelo.minutos = 0;
            }

            SetBreadCrumb("Reunir", reunion.DEPARTAMENTO.nombre_depto, reunion.REUNION_TIPO.Nombre, modelo.reunion.nombre_reunion);

            return View("Minuta", modelo);
        }
        public ActionResult NuevaMinuta(int? id, bool popUp = false, bool OtroPreguntar = true)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var objMinuta = db.MINUTA.Find(id);
            if (objMinuta == null)
            {
                return HttpNotFound();
            }

            var modelo = new CrearNuevaMinutaViewModel();
            modelo.MinutaId = (int)id;
            modelo.NuevaFecha = DateTime.Now;
            modelo.NombreReunion = objMinuta.REUNION.nombre_reunion;
            modelo.esPopUp = popUp;
            modelo.Confirmar = false;
            modelo.preguntar = OtroPreguntar;

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NuevaMinuta(CrearNuevaMinutaViewModel modelo)
        {
            if (modelo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var objMinuta = db.MINUTA.Find(modelo.MinutaId);
            if (objMinuta == null)
            {
                return HttpNotFound();
            }

            if (modelo.NuevaFecha < DateTime.Now.Date)
            {
                ModelState.AddModelError("NuevaFecha", " Error: la nueva fecha no puede ser menor al dia de hoy.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (modelo.esPopUp)
                    {
                        if (modelo.Confirmar)
                        {
                            CreacionMinuta(objMinuta, modelo.NuevaFecha);
                        }
                        return RedirectToAction("Reunir", "Meetings", new { id = objMinuta.REUNION.departamentoId, tipoId = objMinuta.REUNION.tipoId, reunionId = objMinuta.reunionId, preguntar = modelo.preguntar });
                    }
                    else
                    {
                        CreacionMinuta(objMinuta, modelo.NuevaFecha);
                        return RedirectToAction("Reunir", "Meetings", new { id = objMinuta.REUNION.departamentoId, tipoId = objMinuta.REUNION.tipoId, reunionId = objMinuta.reunionId, preguntar = modelo.preguntar });
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    var listaValidaciones = ((System.Data.Entity.Validation.DbEntityValidationException)ex).EntityValidationErrors;
                    foreach (var item in listaValidaciones)
                    {
                        foreach (var itemInside in item.ValidationErrors)
                        {
                            ModelState.AddModelError("", " Error " + itemInside.PropertyName + " " + itemInside.ErrorMessage + ".");
                        }
                    }
                    return View(modelo);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "No se ha podido crear la nueva tarea. Por los siguientes errores: ");
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        string mensajeErrorInner = ex.InnerException.ToString();
                        if (mensajeErrorInner.Length > 500)
                        {
                            mensajeErrorInner = mensajeErrorInner.Substring(0, 500);
                        }
                        ModelState.AddModelError("", mensajeErrorInner);
                    }
                    return View(modelo);
                }
            }

            return View(modelo);
        }
        public ActionResult Reuniones(int? id, int? tipoId)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var obj = db.DEPARTAMENTO.Find((int)id);
            if (obj == null)
            {
                return HttpNotFound();
            }
            var obj2 = db.REUNION_TIPO.Find(tipoId);
            if (obj2 == null)
            {
                return HttpNotFound();
            }

            var modelo = new ListadoReunionesDepartamento();

            IEnumerable<objReuniones> listadoReuniones = db.REUNION
                .Where(p => p.departamentoId == id && p.tipoId == tipoId && p.estatusId == FollowTaskIt.Enumeradores.ReunionEstatus.Activo)
                .Select(item => new objReuniones
                {
                    iconoBoostrap = item.REUNION_TIPO.iconoBoostrap,
                    departamentoId = item.departamentoId,
                    contador = item.MINUTA.Count,
                    usuario1 = item.EMPLEADOS.nombre,
                    usuario2 = item.EMPLEADOS1.nombre,
                    usuario3 = item.EMPLEADOS2.nombre,
                    nombre_reunion = item.nombre_reunion,
                    reunionId = item.reunionId,
                    tipoId = item.tipoId,
                    NuevaMinutaId = (item.MINUTA.Where(p => p.Activo == true).FirstOrDefault() != null ? item.MINUTA.Where(p => p.Activo == true).FirstOrDefault().minutaId : 0)
                })
                .OrderBy(p => p.nombre_reunion).ToList();

            foreach (var item1 in listadoReuniones)
            {

                IEnumerable<objMinuta> listadoMinutas = db.MINUTA
                     .Where(p => p.reunionId == item1.reunionId)
                     .Select(item => new objMinuta
                     {
                         departamentoId = (int)id,
                         tipoId = (int)tipoId,
                         minutaId = item.minutaId,
                         nombre = item.nombre,
                         reunionId = item.reunionId
                     }).OrderByDescending(b => b.minutaId);

                item1.Minutas = listadoMinutas;

            }

            SetBreadCrumb("Reuniones", obj.nombre_depto, obj2.Nombre);
            string temporalNombre = "Listado de Reuniones de " + obj.nombre_depto;
            temporalNombre += " del tipo " + obj2.Nombre;
            ViewBag.Title = temporalNombre;

            modelo.Reuniones = listadoReuniones;

            return View(modelo);
        }
        public ActionResult Minuta(int? minutaId, bool preguntar = true)
        {
            ViewBag.preguntar = preguntar;
            var modelo = new EditarMinutaViewModel();
            modelo.currentUserId = currentUser().empleadoId;
            modelo.vistaTipo = VistaTipo.Minuta;

            if (minutaId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var minuta = db.MINUTA.Find(minutaId);
            if (minuta == null)
            {
                return HttpNotFound();
            }
            modelo.minuta = minuta;
            modelo.reunion = minuta.REUNION;

            ViewBag.empleadoId = new SelectList(modelo.minuta.PARTICIPANTES.Where(p => p.EMPLEADOS.Activo == true).OrderBy(p => p.nombre_empleado).Select(p => new { empleadoId = p.empleadoId, nombre = p.EMPLEADOS.nombre }), "empleadoId", "nombre");
            ViewBag.solicitante = new SelectList(db.EMPLEADOS.Where(p => p.tipoId == TipoEmpleado.Empleado && p.Activo == true).OrderBy(p => p.nombre).Select(p => new { empleadoId = p.empleadoId, nombre = p.nombre }), "nombre", "nombre", currentUser().nombre);

            modelo.soloLectura = MinutaEsSoloLectura(modelo.minuta);
            if (!modelo.soloLectura.soloLectura && modelo.minuta.fecha < DateTime.Now.Date)
            {
                modelo.mostrarPopUp = true;
            }
            if (modelo.reunion.tiempo != null)
            {
                modelo.minutos = (int)modelo.reunion.tiempo;
            }
            else
            {
                modelo.minutos = 0;
            }

            SetBreadCrumb("Minuta", modelo.reunion.DEPARTAMENTO.nombre_depto, modelo.reunion.REUNION_TIPO.Nombre, modelo.reunion.nombre_reunion);
            return View(modelo);

        }
        public ActionResult Contador(int id)
        {
            var modelo = new EditarContadorViewModel();
            modelo.Titulo = "Especificar el tiempo";
            modelo.Minutos = id;
            return PartialView(modelo);
        }
        public ActionResult EliminarRefechas(int id)
        {
            var modelo = new EliminarRefechasViewModel();
            modelo.Reunion = db.TAREAS.Find(id).MINUTA.REUNION.nombre_reunion;
            modelo.Minuta = db.TAREAS.Find(id).MINUTA.nombre;
            modelo.Tarea = db.TAREAS.Find(id).tarea;
            modelo.tareaRecId = id;

            if (Request.IsAjaxRequest())
                return PartialView(modelo);

            return View(modelo);
        }
        public ActionResult Reasignacion(int minutaId, int reunionId)
        {
            ViewBag.reunionId = reunionId;
            ViewBag.minutaId = minutaId;
            var objMinuta = db.MINUTA.Find(minutaId);
            var objReunion = objMinuta.REUNION;
            var listadoMinutas = db.MINUTA.Where(p => p.REUNION.tipoId != ReunionTipo.Plantilla && p.Activo == true
                && p.REUNION.estatusId == ReunionEstatus.Activo).Select(pp =>
                    new
                    {
                        minutaId = pp.minutaId,
                        Nombre = pp.REUNION.DEPARTAMENTO.nombre_depto + "->" + pp.REUNION.nombre_reunion
                    });
            ViewBag.empleadoIdDesde = new SelectList(objMinuta.PARTICIPANTES.Where(p => p.EMPLEADOS.Activo == true).OrderBy(p => p.nombre_empleado).Select(p => new { empleadoId = p.empleadoId, nombre = p.EMPLEADOS.nombre }), "empleadoId", "nombre");
            ViewBag.empleadoIdHasta = new SelectList(objMinuta.PARTICIPANTES.Where(p => p.EMPLEADOS.Activo == true).OrderBy(p => p.nombre_empleado).Select(p => new { empleadoId = p.empleadoId, nombre = p.EMPLEADOS.nombre }), "empleadoId", "nombre");
            ViewBag.minutaIdSeleccion = new SelectList(listadoMinutas.OrderBy(p => p.Nombre), "minutaId", "Nombre", minutaId);

            if (Request.IsAjaxRequest())
                return PartialView();

            return View();
        }
        public ActionResult ReasignacionConfirmar(string datosArray, int minutaId, string empleadoId)
        {
            //ReasignarTareas(urlBase, datosArray, $('#minutaIdSeleccion').val(), $('#empleadoIdHasta').val());
            var modelo = new ReasignacionConfirmarViewModel();
            modelo.datosArray = datosArray;
            modelo.minutaId = minutaId;
            modelo.empleadoId = empleadoId;
            modelo.ConRefechas = true;
            modelo.nombreEmpleado = db.EMPLEADOS.Find(empleadoId).nombre;
            if (Request.IsAjaxRequest())
            {
                return PartialView(modelo);
            }

            return View(modelo);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReasignacionConfirmar(ReasignacionConfirmarViewModel modelo)
        {
            int tareaId1 = int.Parse(modelo.datosArray.Split(';').First());
            var controladorTareas = new TareasJsonController();
            controladorTareas.ReasignarTareas(modelo.datosArray, modelo.minutaId, modelo.empleadoId, modelo.ConRefechas);
            var objMinutaActual = db.TAREAS.Find(tareaId1).MINUTA;
            MostrarMensaje("Minutas Reasignadas Exitosamente!", TipoMensaje.Correcto);
            return RedirectToAction("Reunir", "Meetings", new { id = objMinutaActual.REUNION.departamentoId, tipoId = objMinutaActual.REUNION.tipoId, reunionId = objMinutaActual.reunionId, preguntar = false });
        }

        public ActionResult Periodicas(int minutaId, int reunionId, string nombre)
        {
            ViewBag.reunionId = reunionId;
            ViewBag.minutaId = minutaId;
            ViewBag.nombre = nombre;
            return PartialView();
        }
        private SoloLectura MinutaEsSoloLectura(MINUTA minuta)
        {
            //bool retVal = true;
            var retVal1 = new SoloLectura();
            retVal1.soloLectura = true;
            retVal1.agregaTareas = true;

            if (minuta.Activo)
            {
                if (minuta.abierta)
                {
                    retVal1.agregaTareas = false;
                    if (minuta.EMPLEADO != null)
                    {
                        if (minuta.EMPLEADO.empleadoId == currentUser().empleadoId)
                        {
                            retVal1.soloLectura = false;
                            retVal1.agregaTareas = true;
                        }
                        else
                        {
                            MostrarMensaje("La minuta esta abierta con acceso limitado.; Abierta por el usuario " + minuta.EMPLEADO.nombre + ".", TipoMensaje.Error);
                            MostrarMensaje("Abierta en formato solo lectura.");
                        }
                    }
                    else
                    {
                        MostrarMensaje("La minuta esta abierta por otro usuario.;Abierta en formato solo lectura.");
                    }
                }
                else if (minuta.REUNION.EMPLEADOS.empleadoId == currentUser().empleadoId ||
                    (minuta.REUNION.EMPLEADOS1 != null ? minuta.REUNION.EMPLEADOS1.empleadoId == currentUser().empleadoId : false) ||
                    (minuta.REUNION.EMPLEADOS2 != null ? minuta.REUNION.EMPLEADOS2.empleadoId == currentUser().empleadoId : false))
                {
                    MostrarMensaje("La minuta ha sido abierta por el usuario: " + currentUser().nombre, TipoMensaje.Correcto);
                    minuta.abierta = true;
                    minuta.usuario_abrio = currentUser().empleadoId;
                    db.SaveChanges();
                    retVal1.soloLectura = false;
                }
                else
                {
                    MostrarMensaje("Esta minuta sera abierta con acceso limitado.;Abierta en formato solo lectura.;No eres administrador de la minuta.", TipoMensaje.Advertencia);
                }
            }
            else
            {
                retVal1.agregaTareas = false;
                MostrarMensaje("Esta minuta sera abierta con acceso limitado, pues no es la minuta activa.;Abierta en formato solo lectura.", TipoMensaje.Advertencia);
            }

            return retVal1;

        }
        private void MostrarMensaje(string mensaje, string tipo = TipoMensaje.Informacion)
        {
            if (TempData["MensajeError"] == null)
            {
                TempData["MensajeError"] = tipo + "|" + mensaje;
            }
            else
            {
                TempData["MensajeError"] += ";" + mensaje;
            }
        }
        private EMPLEADOS currentUser()
        {
            string usuario = User.Identity.Name.ToString();
            if (_currentUser == null)
            {
                _currentUser = (from data in db.EMPLEADOS where data.usuario == usuario select data).FirstOrDefault();
            }
            return _currentUser;
        }
        private void CreacionMinuta(MINUTA objMinuta, DateTime? nuevaFecha)
        {
            if (nuevaFecha == null) { nuevaFecha = DateTime.Now; }
            MINUTA item = new MINUTA();
            item.nombre = ((DateTime)nuevaFecha).ToString("dd/MM/yyyy");
            item.reunionId = objMinuta.reunionId;
            item.agenda = objMinuta.agenda;
            item.puntos_pendientes = objMinuta.puntos_pendientes;
            item.fecha = nuevaFecha;
            item.Activo = true;
            item.abierta = false;
            item.AuditoriaCrear(currentUser().usuario);
            objMinuta.Activo = false;
            db.MINUTA.Add(item);

            foreach (var objParticipante in objMinuta.PARTICIPANTES)
            {
                if (objParticipante.EMPLEADOS.Activo)
                {
                    var nuevoParticipante = new PARTICIPANTES();
                    nuevoParticipante.empleadoId = objParticipante.empleadoId;
                    nuevoParticipante.nombre_empleado = objParticipante.nombre_empleado;
                    nuevoParticipante.asistencia = objParticipante.asistencia;
                    item.PARTICIPANTES.Add(nuevoParticipante);
                }
            }
            foreach (var objTarea in objMinuta.TAREAS)
            {
                if (!objTarea.Realizada)
                {
                    var nuevaTarea = new TAREAS();
                    nuevaTarea.tareaId = objTarea.tareaId;
                    nuevaTarea.tarea = objTarea.tarea;
                    nuevaTarea.solicitante = objTarea.solicitante;
                    nuevaTarea.empleadoId = objTarea.empleadoId;
                    nuevaTarea.fecha_asignada = objTarea.fecha_asignada;
                    nuevaTarea.fecha_vence = objTarea.fecha_vence;
                    nuevaTarea.tareaperiodicaId = objTarea.tareaperiodicaId;
                    nuevaTarea.grupo = objTarea.grupo;
                    nuevaTarea.predecesor = objTarea.predecesor;
                    nuevaTarea.subgrupo = objTarea.subgrupo;
                    nuevaTarea.hora_fin = objTarea.hora_fin;
                    nuevaTarea.hora_ini = objTarea.hora_ini;
                    nuevaTarea.avisos = objTarea.avisos;
                    nuevaTarea.Realizada = objTarea.Realizada;
                    nuevaTarea.tareaRecIdEliminar = objTarea.tareaRecId;
                    foreach (var objRefecha in objTarea.REFECHAS)
                    {
                        var nuevaRefecha = new REFECHAS();
                        nuevaRefecha.Fecha = objRefecha.Fecha;
                        nuevaRefecha.activa = objRefecha.activa;
                        nuevaTarea.REFECHAS.Add(nuevaRefecha);
                    }
                    item.TAREAS.Add(nuevaTarea);
                }
            }
            db.SaveChanges();
        }
        private void SetBreadCrumb(string caseId, string departamento, string tipoReunion = "", string minuta = "")
        {
            var node = SiteMaps.Current.CurrentNode;

            switch (caseId)
            {
                case "Departamento":
                    if (node != null)
                    {
                        node.Title = departamento;
                    }
                    break;

                case "Minuta":
                    if (node != null)
                    {
                        node.Title = minuta;
                        if (node != null && node.ParentNode != null)
                        {
                            node.ParentNode.Title = tipoReunion;
                            if (node != null && node.ParentNode.ParentNode != null)
                            {
                                node.ParentNode.ParentNode.Title = departamento;
                            }
                        }
                    }
                    break;

                case "Reuniones":
                    if (node != null)
                    {
                        node.Title = tipoReunion;
                        if (node != null && node.ParentNode != null)
                        {
                            node.ParentNode.Title = departamento;
                        }
                    }
                    break;

                case "Reunir":
                    goto case "Minuta";

                default:
                    break;
            }
        }
    }
}