namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PARAMETROS
    {
        [Key]
        public int Parametros_Id { get; set; }

        [StringLength(10)]
        public string formato_fecha { get; set; }

        [StringLength(50)]
        public string nombre_empresa { get; set; }

        [StringLength(50)]
        public string servidor_email { get; set; }

        [StringLength(50)]
        public string email_operador { get; set; }

        [StringLength(50)]
        public string direccion { get; set; }

        [StringLength(10)]
        public string formato_fecha_display { get; set; }

        public int? puerto_chat { get; set; }

        public int? secuencia_auto { get; set; }

        [StringLength(30)]
        public string usuario_email { get; set; }

        [StringLength(30)]
        public string clave_email { get; set; }

        [StringLength(15)]
        public string Version { get; set; }

        public int? puerto_email { get; set; }

        public DateTime? TareaPeriodicaEjecutada { get; set; }
    }
}
