namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Periodos
    {
        public Periodos()
        {
            Dias = new HashSet<Dias>();
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
        }

        [Key]
        public int PeriodoId { get; set; }

        [Required]
        [StringLength(40)]
        public string Descripcion { get; set; }

        public int Duracion { get; set; }

        [StringLength(100)]
        public string CreadoPor { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(100)]
        public string ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public int? Orden { get; set; }

        public bool Activo { get; set; }

        public virtual ICollection<Dias> Dias { get; set; }

        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
    }
}
