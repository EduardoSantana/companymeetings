namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLEADOS
    {
        public EMPLEADOS()
        {
            DEPARTAMENTO = new HashSet<DEPARTAMENTO>();
            DEPARTAMENTO1 = new HashSet<DEPARTAMENTO>();
            DEPARTAMENTO2 = new HashSet<DEPARTAMENTO>();
            DEPARTAMENTO3 = new HashSet<DEPARTAMENTO>();
            MINUTA = new HashSet<MINUTA>();
            PARTICIPANTES = new HashSet<PARTICIPANTES>();
            REUNION = new HashSet<REUNION>();
            REUNION1 = new HashSet<REUNION>();
            REUNION2 = new HashSet<REUNION>();
            SERVICIOS = new HashSet<SERVICIOS>();
            SERVICIOS1 = new HashSet<SERVICIOS>();
            TAREAS = new HashSet<TAREAS>();
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
            TIEMPO_STANDARD = new HashSet<TIEMPO_STANDARD>();
        }

        [Key]
        [StringLength(8)]
        public string empleadoId { get; set; }

        [Required]
        [StringLength(35)]
        public string nombre { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        [Required]
        [StringLength(20)]
        public string usuario { get; set; }

        public bool accesomant { get; set; }

        [StringLength(20)]
        public string PCid { get; set; }

        public int? codigo_grupo { get; set; }

        [StringLength(200)]
        public string desktop { get; set; }

        [Column(TypeName = "text")]
        public string notas { get; set; }

        public bool modifica_noticias { get; set; }

        public bool muestra_tres_tareas { get; set; }

        public bool chat { get; set; }

        public int? idioma { get; set; }

        public bool Activo { get; set; }

        public int departamentoId { get; set; }

        public int tipoId { get; set; }

        public string PasswordHash { get; set; }

        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO { get; set; }

        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO1 { get; set; }

        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO2 { get; set; }

        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO3 { get; set; }

        public virtual DEPARTAMENTO DEPARTAMENTO4 { get; set; }

        public virtual EMPLEADOS_TIPO EMPLEADOS_TIPO { get; set; }

        public virtual ICollection<MINUTA> MINUTA { get; set; }

        public virtual ICollection<PARTICIPANTES> PARTICIPANTES { get; set; }

        public virtual ICollection<REUNION> REUNION { get; set; }

        public virtual ICollection<REUNION> REUNION1 { get; set; }

        public virtual ICollection<REUNION> REUNION2 { get; set; }

        public virtual ICollection<SERVICIOS> SERVICIOS { get; set; }

        public virtual ICollection<SERVICIOS> SERVICIOS1 { get; set; }

        public virtual ICollection<TAREAS> TAREAS { get; set; }

        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }

        public virtual ICollection<TIEMPO_STANDARD> TIEMPO_STANDARD { get; set; }
    }
}
