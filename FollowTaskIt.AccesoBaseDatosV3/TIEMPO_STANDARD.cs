namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TIEMPO_STANDARD
    {
        public TIEMPO_STANDARD()
        {
            SERVICIOS = new HashSet<SERVICIOS>();
        }

        [Key]
        public int tiempoid { get; set; }

        public int departamentoId { get; set; }

        [Required]
        [StringLength(40)]
        public string descripcion { get; set; }

        public int dias { get; set; }

        [Required]
        [StringLength(8)]
        public string empleadoid { get; set; }

        public int? minutos { get; set; }

        public bool Activo { get; set; }

        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual ICollection<SERVICIOS> SERVICIOS { get; set; }
    }
}
