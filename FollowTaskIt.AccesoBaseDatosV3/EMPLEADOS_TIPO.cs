namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLEADOS_TIPO
    {
        public EMPLEADOS_TIPO()
        {
            EMPLEADOS = new HashSet<EMPLEADOS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmpleadoTipoId { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public virtual ICollection<EMPLEADOS> EMPLEADOS { get; set; }
    }
}
