namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("REUNION")]
    public partial class REUNION
    {
        public REUNION()
        {
            MINUTA = new HashSet<MINUTA>();
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
        }

        public int departamentoId { get; set; }

        public int reunionId { get; set; }

        [Required]
        [StringLength(50)]
        public string nombre_reunion { get; set; }

        [Required]
        [StringLength(8)]
        public string usuario1 { get; set; }

        [StringLength(8)]
        public string usuario2 { get; set; }

        [StringLength(8)]
        public string usuario3 { get; set; }

        public int estatusId { get; set; }

        public int tipoId { get; set; }

        public DateTime? fecha_inicio { get; set; }

        public DateTime? fecha_final { get; set; }

        public DateTime? fecha_cierre { get; set; }

        public int? tiempo { get; set; }

        [StringLength(50)]
        public string password { get; set; }

        public bool usopassword { get; set; }

        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual EMPLEADOS EMPLEADOS1 { get; set; }

        public virtual EMPLEADOS EMPLEADOS2 { get; set; }

        public virtual ICollection<MINUTA> MINUTA { get; set; }

        public virtual REUNION_ESTATUS REUNION_ESTATUS { get; set; }

        public virtual REUNION_TIPO REUNION_TIPO { get; set; }

        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
    }
}
