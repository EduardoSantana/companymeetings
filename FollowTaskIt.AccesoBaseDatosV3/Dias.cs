namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Dias
    {
        public Dias()
        {
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
        }

        [Required]
        [StringLength(2)]
        public string Dia { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(200)]
        public string Descripcion { get; set; }

        public int PeriodoId { get; set; }

        [Key]
        public int DiaId { get; set; }

        [StringLength(100)]
        public string CreadoPor { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(100)]
        public string ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public bool Activo { get; set; }

        public int? Orden { get; set; }

        public virtual Periodos Periodos { get; set; }

        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
    }
}
