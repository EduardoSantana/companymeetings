namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TAREAS_PERIODICAS
    {
        public int reunionId { get; set; }

        [Key]
        public int tareaperiodicaId { get; set; }

        [Required]
        [StringLength(8)]
        public string empleadoId { get; set; }

        [Required]
        [StringLength(300)]
        public string tarea { get; set; }

        public DateTime proxima_ejecucion { get; set; }

        public bool Activa { get; set; }

        public DateTime? fechaInsertar { get; set; }

        public int PeriodoId { get; set; }

        public int DiaId { get; set; }

        [StringLength(100)]
        public string CreadoPor { get; set; }

        public DateTime? FechaCreacion { get; set; }

        [StringLength(100)]
        public string ModificadoPor { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public virtual Dias Dias { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual Periodos Periodos { get; set; }

        public virtual REUNION REUNION { get; set; }
    }
}
