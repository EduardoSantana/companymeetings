namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class REUNION_TIPO
    {
        public REUNION_TIPO()
        {
            REUNION = new HashSet<REUNION>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tipoId { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string iconoBoostrap { get; set; }

        public virtual ICollection<REUNION> REUNION { get; set; }
    }
}
