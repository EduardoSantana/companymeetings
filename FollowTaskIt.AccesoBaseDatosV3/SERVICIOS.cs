namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SERVICIOS
    {
        public int departamentoId { get; set; }

        [Key]
        public int servicioId { get; set; }

        [Required]
        [StringLength(300)]
        public string descripcion { get; set; }

        [StringLength(20)]
        public string pcid { get; set; }

        [Required]
        [StringLength(8)]
        public string solicitanteid { get; set; }

        public DateTime? fecha { get; set; }

        public int tiempoid { get; set; }

        public int estadoId { get; set; }

        [StringLength(2000)]
        public string solucion { get; set; }

        [Required]
        [StringLength(8)]
        public string asignadoid { get; set; }

        public DateTime? fecha_vence { get; set; }

        public DateTime? fecha_cierre { get; set; }

        public DateTime? fecha_solucion { get; set; }

        public int? notificaciones { get; set; }

        public int? satisfaccion { get; set; }

        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual EMPLEADOS EMPLEADOS1 { get; set; }

        public virtual SERVICIOS_ESTADOS SERVICIOS_ESTADOS { get; set; }

        public virtual TIEMPO_STANDARD TIEMPO_STANDARD { get; set; }
    }
}
