namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SERVICIOS_ESTADOS
    {
        public SERVICIOS_ESTADOS()
        {
            SERVICIOS = new HashSet<SERVICIOS>();
        }

        [Key]
        public int estadoId { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public virtual ICollection<SERVICIOS> SERVICIOS { get; set; }
    }
}
