namespace FollowTaskIt.AccesoBaseDatosV3
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SERVICIOS_ATTACH
    {
        [Key]
        public int Recid { get; set; }

        public int? servicioid { get; set; }

        [StringLength(200)]
        public string Archivo { get; set; }
    }
}
