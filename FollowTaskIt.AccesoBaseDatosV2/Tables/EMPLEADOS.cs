namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLEADOS
    {
        public EMPLEADOS()
        {
            DEPARTAMENTO = new HashSet<DEPARTAMENTO>();
            DEPARTAMENTO2 = new HashSet<DEPARTAMENTO>();
            DEPARTAMENTO3 = new HashSet<DEPARTAMENTO>();
            //DEPARTAMENTO4 = new HashSet<DEPARTAMENTO>();
            PARTICIPANTES = new HashSet<PARTICIPANTES>();
            REUNION = new HashSet<REUNION>();
            REUNION1 = new HashSet<REUNION>();
            REUNION2 = new HashSet<REUNION>();
            SERVICIOS = new HashSet<SERVICIOS>();
            SERVICIOS1 = new HashSet<SERVICIOS>();
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
            TIEMPO_STANDARD = new HashSet<TIEMPO_STANDARD>();
            TAREAS = new HashSet<TAREAS>();
            MINUTA = new HashSet<MINUTA>();
        }
        [Key]
        [StringLength(8)]
        [DisplayName("Empleado ID")]
        public string empleadoId { get; set; }
        [DisplayName("Empleado")]
        [Required]
        [StringLength(35)]
        public string nombre { get; set; }
        [StringLength(50)]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [DisplayName("Usuario")]
        [Required]
        [StringLength(20)]
        public string usuario { get; set; }
        [DisplayName("Mantenimientos")]
        public bool accesomant { get; set; }
        [StringLength(20)]
        [DisplayName("PC")]
        public string PCid { get; set; }
        [DisplayName("Codigo Grupo")]
        public int? codigo_grupo { get; set; }
        [StringLength(200)]
        [DisplayName("Desktop")]
        public string desktop { get; set; }
        [Column(TypeName = "text")]
        [DataType(DataType.MultilineText)]
        [DisplayName("Notas")]
        public string notas { get; set; }
        [DisplayName("Noticias")]
        public bool modifica_noticias { get; set; }
        [DisplayName("Tareas")]
        public bool muestra_tres_tareas { get; set; }
        [DisplayName("Chat")]
        public bool chat { get; set; }
        [DisplayName("Idioma")]
        public int? idioma { get; set; }
        public bool Activo { get; set; }
        [DisplayName("Departamento")]
        public int departamentoId { get; set; }
        [DisplayName("Contraseņa")]
        [DataType(DataType.Password)]
        public string PasswordHash { get; set; }
        public int? tipoId { get; set; }
        [NotMapped]
        public bool dataBaseValidated
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["dataBaseValidated"]);
            }
        }
        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO { get; set; }
        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO1 { get; set; }
        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO2 { get; set; }
        public virtual ICollection<DEPARTAMENTO> DEPARTAMENTO3 { get; set; }
        [DisplayName("Departamento")]
        public virtual DEPARTAMENTO DEPARTAMENTO4 { get; set; }
        public virtual ICollection<PARTICIPANTES> PARTICIPANTES { get; set; }
        public virtual ICollection<REUNION> REUNION { get; set; }
        public virtual ICollection<REUNION> REUNION1 { get; set; }
        public virtual ICollection<REUNION> REUNION2 { get; set; }
        public virtual ICollection<SERVICIOS> SERVICIOS { get; set; }
        public virtual ICollection<SERVICIOS> SERVICIOS1 { get; set; }
        public virtual ICollection<MINUTA> MINUTA { get; set; }
        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
        public virtual ICollection<TIEMPO_STANDARD> TIEMPO_STANDARD { get; set; }
        public virtual ICollection<TAREAS> TAREAS { get; set; }

    }
}
