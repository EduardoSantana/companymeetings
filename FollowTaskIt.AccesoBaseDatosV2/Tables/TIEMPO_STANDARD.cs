namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TIEMPO_STANDARD
    {
        public TIEMPO_STANDARD()
        {
            SERVICIOS = new HashSet<SERVICIOS>();
        }

        public virtual ICollection<SERVICIOS> SERVICIOS { get; set; }

        [Key]
        public int tiempoid { get; set; }
        [DisplayName("Departamento")]
        public int departamentoId { get; set; }

        [Required]
        [StringLength(40)]
        [DisplayName("Descripcion")]
        public string descripcion { get; set; }
                [DisplayName("Dias")]
        public int dias { get; set; }
                [DisplayName("Asignado")]
        [Required]
        [StringLength(8)]
        public string empleadoid { get; set; }
                [DisplayName("Minutos")]
        public int? minutos { get; set; }
        public bool Activo { get; set; }
                [DisplayName("Departamento")]
        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }
                [DisplayName("Empleado")]
        public virtual EMPLEADOS EMPLEADOS { get; set; }
    }
}
