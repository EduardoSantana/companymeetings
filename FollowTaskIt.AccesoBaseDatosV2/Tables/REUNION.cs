namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("REUNION")]
    public partial class REUNION
    {
        public REUNION()
        {
            MINUTA = new HashSet<MINUTA>();
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
        }
        [DisplayName("Departamento")]
        public int departamentoId { get; set; }
        [DisplayName("Reunion ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int reunionId { get; set; }
        [DisplayName("Nombre")]
        [Required]
        [StringLength(50)]
        public string nombre_reunion { get; set; }
        [DisplayName("Administrador1")]
        [Required]
        [StringLength(8)]
        public string usuario1 { get; set; }
        [DisplayName("Administrador2")]
        [StringLength(8)]
        public string usuario2 { get; set; }
        [DisplayName("Administrador3")]
        [StringLength(8)]
        public string usuario3 { get; set; }
        [DisplayName("Estatus")]
        public int estatusId { get; set; }
        [DisplayName("Tipo")]
        public int tipoId { get; set; }
        [DisplayName("Fecha Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_inicio { get; set; }
        [DisplayName("Fecha Final")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_final { get; set; }
        [DisplayName("Fecha Cierre")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_cierre { get; set; }
        [DisplayName("Tiempo")]
        public int? tiempo { get; set; }
        [DisplayName("Password")]
        [StringLength(50)]
        public string password { get; set; }
        [DisplayName("Uso Password")]
        public bool usopassword { get; set; }
        [DisplayName("Departamento")]
        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual EMPLEADOS EMPLEADOS1 { get; set; }

        public virtual EMPLEADOS EMPLEADOS2 { get; set; }

        public virtual ICollection<MINUTA> MINUTA { get; set; }

        public virtual REUNION_ESTATUS REUNION_ESTATUS { get; set; }

        public virtual REUNION_TIPO REUNION_TIPO { get; set; }

        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
    }
}
