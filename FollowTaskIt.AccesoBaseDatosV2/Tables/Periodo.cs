﻿namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public partial class Periodos
    {
        public Periodos()
        {
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
            Dias = new HashSet<Dias>();
        }

        [DisplayName("Periodo ID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PeriodoId { get; set; }

        [Required]
        [StringLength(40)]
        public string Descripcion { get; set; }
        [Required]
        public int Duracion { get; set; }
        public int? Orden { get; set; }
        public bool Activo { get; set; }
        [StringLength(100)]
        public string CreadoPor { get; set; }
        public DateTime? FechaCreacion { get; set; }
        [StringLength(100)]
        public string ModificadoPor { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public virtual ICollection<Dias> Dias { get; set; }
        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }

    }

}
