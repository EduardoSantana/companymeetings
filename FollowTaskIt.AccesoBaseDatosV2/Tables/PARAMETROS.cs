namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PARAMETROS
    {
        [DisplayName("Parametro ID")]
        [Key]
        public int Parametros_Id { get; set; }
        [DisplayName("Formato Fecha")]
        [StringLength(10)]
        public string formato_fecha { get; set; }
        [DisplayName("Nombre Empresa")]
        [StringLength(50)]
        public string nombre_empresa { get; set; }
        [DisplayName("Servidor Email")]
        [StringLength(50)]
        public string servidor_email { get; set; }
        [DisplayName("Email Operador")]
        [StringLength(50)]
        public string email_operador { get; set; }
        [DisplayName("Direccion")]
        [StringLength(50)]
        public string direccion { get; set; }
        [DisplayName("Formato Fecha Display")]
        [StringLength(10)]
        public string formato_fecha_display { get; set; }
        [DisplayName("Puerto Chat")]
        public int? puerto_chat { get; set; }
        [DisplayName("Secuencia Auto")]
        public int? secuencia_auto { get; set; }
        [DisplayName("Usuario Email")]
        [StringLength(30)]
        public string usuario_email { get; set; }
        [DisplayName("Clave Email")]
        [StringLength(30)]
        public string clave_email { get; set; }
        [DisplayName("Version")]
        [StringLength(15)]
        public string Version { get; set; }
        [DisplayName("Puerto Email")]
        public int? puerto_email { get; set; }
        public DateTime? TareaPeriodicaEjecutada { get; set; }
    }
}
