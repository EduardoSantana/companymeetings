namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SERVICIOS
    {
        [DisplayName("Departamento")]
        public int? departamentoId { get; set; }
        [DisplayName("Servicio ID")]
        [Key]
        public int servicioId { get; set; }

        [Required]
        [StringLength(300)]
        [DataType(DataType.MultilineText)]
        [DisplayName("Descripcion")]
        public string descripcion { get; set; }

        [StringLength(20)]
        [DisplayName("PC")]
        public string pcid { get; set; }

        [StringLength(8)]
        [DisplayName("Solicitante")]
        public string solicitanteid { get; set; }
        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha { get; set; }
        [DisplayName("Tipo Servicio")]
        public int tiempoid { get; set; }
        [DisplayName("Estado")]
        public int estadoId { get; set; }
        [DisplayName("Soluci�n")]
        [StringLength(2000)]
        [DataType(DataType.MultilineText)]
        public string solucion { get; set; }
        [DisplayName("Asignado")]
        [StringLength(8)]
        public string asignadoid { get; set; }
        [DisplayName("Fecha Vence")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_vence { get; set; }
        [DisplayName("Fecha Cierre")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_cierre { get; set; }
        [DisplayName("Fecha Solucion")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_solucion { get; set; }
        [DisplayName("Notificaciones")]
        public int? notificaciones { get; set; }
        [DisplayName("Satisfaccion")]
        public int? satisfaccion { get; set; }
        [NotMapped]
        public bool Cerrado { get; set; }
        [NotMapped]
        [DisplayName("Solucionado")]
        public bool Solucionado { get; set; }
        [DisplayName("Departamento")]
        public virtual DEPARTAMENTO DEPARTAMENTO { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual EMPLEADOS EMPLEADOS1 { get; set; }

        public virtual TIEMPO_STANDARD TIEMPO_STANDARD { get; set; }

        public virtual SERVICIOS_ESTADOS SERVICIOS_ESTADOS { get; set; }
    }
}
