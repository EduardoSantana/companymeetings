namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MINUTA")]
    public partial class MINUTA
    {
        public MINUTA()
        {
            PARTICIPANTES = new HashSet<PARTICIPANTES>();
            TAREAS = new HashSet<TAREAS>();
        }

        public int reunionId { get; set; }

        public int minutaId { get; set; }
        [DisplayName("Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha { get; set; }

        public string agenda { get; set; }

        public string puntos_pendientes { get; set; }

        public bool abierta { get; set; }

        [StringLength(20)]
        public string usuario_abrio { get; set; }

        [StringLength(20)]
        public string nombre { get; set; }

        public bool Activo { get; set; }

        [StringLength(100)]
        public string CreadoPor { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public virtual REUNION REUNION { get; set; }
        public virtual EMPLEADOS EMPLEADO { get; set; }
        public virtual ICollection<PARTICIPANTES> PARTICIPANTES { get; set; }
        public virtual ICollection<TAREAS> TAREAS { get; set; }
    }
}
