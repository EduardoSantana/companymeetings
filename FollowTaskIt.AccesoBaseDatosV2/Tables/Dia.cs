﻿namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public partial class Dias
    {
        public Dias()
        {
            TAREAS_PERIODICAS = new HashSet<TAREAS_PERIODICAS>();
        }

        [DisplayName("Dia ID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DiaId { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(200)]
        public string Descripcion { get; set; }
        [StringLength(2)]
        [Required]
        public string Dia { get; set; }

        public int? Orden { get; set; }
        public bool Activo { get; set; }

         [DisplayName("Periodo")]
        public int PeriodoId { get; set; }
         [StringLength(100)]
         public string CreadoPor { get; set; }
         public DateTime? FechaCreacion { get; set; }
         [StringLength(100)]
         public string ModificadoPor { get; set; }
         public DateTime? FechaModificacion { get; set; }
        public virtual Periodos Periodos { get; set; }
        public virtual ICollection<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }

    }

}
