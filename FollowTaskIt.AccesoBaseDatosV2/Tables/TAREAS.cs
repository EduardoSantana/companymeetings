namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TAREAS
    {
        public TAREAS()
        {
            REFECHAS = new HashSet<REFECHAS>();
        }

        public int? minutaId { get; set; }

        public int tareaId { get; set; }

        [StringLength(2000)]
        public string tarea { get; set; }

        [StringLength(35)]
        public string solicitante { get; set; }

        [StringLength(8)]
        public string empleadoId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_asignada { get; set; }

        public int? tareaperiodicaId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_vence { get; set; }

        [StringLength(30)]
        public string grupo { get; set; }
        [StringLength(300)]
        public string Notas { get; set; }
        public int? predecesor { get; set; }

        [StringLength(30)]
        public string subgrupo { get; set; }

        public string hora_ini { get; set; }

        public string hora_fin { get; set; }

        public int? avisos { get; set; }

        [Key]
        public int tareaRecId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string ccColorTarea { get; private set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? ccFechaNotificacion { get; private set; }
        public int? tareaRecIdEliminar { get; set; }
        public bool Realizada { get; set; }

        public virtual MINUTA MINUTA { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual ICollection<REFECHAS> REFECHAS { get; set; }
    }
}
