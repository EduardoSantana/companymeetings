namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PARTICIPANTES
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int minutaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(8)]
        public string empleadoId { get; set; }

        [StringLength(35)]
        public string nombre_empleado { get; set; }

        public int? asistencia { get; set; }

        public virtual EMPLEADOS EMPLEADOS { get; set; }

        public virtual MINUTA MINUTA { get; set; }
    }
}
