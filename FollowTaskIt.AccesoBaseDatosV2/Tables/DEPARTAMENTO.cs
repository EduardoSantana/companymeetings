namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DEPARTAMENTO")]
    public partial class DEPARTAMENTO
    {
        public DEPARTAMENTO()
        {
            EMPLEADOS4 = new HashSet<EMPLEADOS>();
            REUNION = new HashSet<REUNION>();
            SERVICIOS1 = new HashSet<SERVICIOS>();
            TIEMPO_STANDARD = new HashSet<TIEMPO_STANDARD>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int departamentoId { get; set; }

        [Required]
        [StringLength(30)]
        [DisplayName("Departamento")]
        public string nombre_depto { get; set; }

        public bool Activo { get; set; }

        [Column(TypeName = "text")]
        [DisplayName("Noticias")]
        [DataType(DataType.MultilineText)]
        public string noticias { get; set; }

        [StringLength(8)]
        [DisplayName("Empleado")]
        public string empleadoid { get; set; }

        [StringLength(50)]
        public string iconoBoostrap { get; set; }
        public bool Servicios { get; set; }
        public string AdministradorId1 { get; set; }
        public string AdministradorId2 { get; set; }
        public string AdministradorId3 { get; set; }
        public virtual EMPLEADOS EMPLEADOS { get; set; }
        public virtual EMPLEADOS EMPLEADOS1 { get; set; }
        public virtual EMPLEADOS EMPLEADOS2 { get; set; }
        public virtual EMPLEADOS EMPLEADOS3 { get; set; }

        public virtual ICollection<EMPLEADOS> EMPLEADOS4 { get; set; }

        public virtual ICollection<REUNION> REUNION { get; set; }

        public virtual ICollection<SERVICIOS> SERVICIOS1 { get; set; }

        public virtual ICollection<TIEMPO_STANDARD> TIEMPO_STANDARD { get; set; }
    }
}
