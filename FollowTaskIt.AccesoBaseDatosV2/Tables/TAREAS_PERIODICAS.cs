namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TAREAS_PERIODICAS
    {
        [DisplayName("Reunion")]
        public int? reunionId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tareaperiodicaId { get; set; }

        [StringLength(300)]
        [DisplayName("Tarea")]
        public string tarea { get; set; }

        [StringLength(8)]
        [DisplayName("Empleado")]
        public string empleadoId { get; set; }
        [DisplayName("Proxima Ejecucion")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime proxima_ejecucion { get; set; }
        [DisplayName("Periodo")]
        public int PeriodoId { get; set; }
        [DisplayName("Dia")]
        public int DiaId { get; set; }
        public DateTime? fechaInsertar { get; set; }
        public bool Activa { get; set; }
        [StringLength(100)]
        public string CreadoPor { get; set; }
        public DateTime? FechaCreacion { get; set; }
        [StringLength(100)]
        public string ModificadoPor { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public virtual EMPLEADOS EMPLEADOS { get; set; }
        public virtual REUNION REUNION { get; set; }
        public virtual Dias Dias { get; set; }
        public virtual Periodos Periodos { get; set; }
    }
}
