﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FollowTaskIt.AccesoBaseDatosV2
{
    public partial class vista_IndicadoresProyectos
    {
        public int? departamentoId { get; set; }
        public int? reunionId { get; set; }
        public string nombre_reunion { get; set; }
        public int? estatusId { get; set; }
        public string empleadoId { get; set; }
        public string NombreEmpleado { get; set; }
        public string Porcentaje { get; set; }
        public int? Realizadas { get; set; }
        public int? Total { get; set; }
        public string PorcentajeEstimadas { get; set; }
        public int? Estimadas { get; set; }
        public string Logro { get; set; }

        public string ccColor { get; set; }

        public static List<vista_IndicadoresProyectos> Load(string empleadoSeleccionado, int estatusId, string empleadoId = "")
        {
            string filtros = " where departamentoId = (select emp.departamentoId From EMPLEADOS emp where emp.empleadoId = '" + empleadoSeleccionado + "') ";
            filtros = filtros + " and estatusId = " + estatusId.ToString() + " ";
            if (!String.IsNullOrEmpty(empleadoId))
            {
                filtros = filtros + " and empleadoId = '" + empleadoId + "' ";
            }

            ModelFollowTaskIt dbContext = new ModelFollowTaskIt();
            return dbContext.Database.SqlQuery<vista_IndicadoresProyectos>(" SELECT * FROM [dbo].[vista_IndicadoresProyectos] " + filtros).ToList();
        }
    }
}
