﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FollowTaskIt.AccesoBaseDatosV2
{
    public partial class vista_IndicadoresReuniones
    {
        public int? FechaAno { get; set; }
        public int? FechaMes { get; set; }
        public string FechaMesNombre { get; set; }
        public string empleadoId { get; set; }
        public int? Realizadas { get; set; }
        public int? Total { get; set; }
        public string Porcentaje { get; set; }
        public int? Refechadas { get; set; }
        public string ccColor { get; set; }
        public static List<vista_IndicadoresReuniones> Load(int anoNumero, string empleadoId)
        {
            string filtros = " WHERE  empleadoId = '" + empleadoId + "'";
            string orderBy = " order by FechaAno Desc, FechaMes Desc ";
            ModelFollowTaskIt dbContext = new ModelFollowTaskIt();
            return dbContext.Database.SqlQuery<vista_IndicadoresReuniones>(" SELECT * FROM [dbo].[vista_IndicadoresReuniones] " + filtros + orderBy).ToList();
        }
    }
}
