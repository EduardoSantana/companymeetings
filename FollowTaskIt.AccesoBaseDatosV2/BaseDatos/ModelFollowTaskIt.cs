namespace FollowTaskIt.AccesoBaseDatosV2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelFollowTaskIt : DbContext
    {
        public ModelFollowTaskIt()
            : base("name=ModelFollowTaskIt")
        {
        }

        public virtual DbSet<DEPARTAMENTO> DEPARTAMENTO { get; set; }
        public virtual DbSet<EMPLEADOS> EMPLEADOS { get; set; }
        public virtual DbSet<MINUTA> MINUTA { get; set; }
        public virtual DbSet<PARAMETROS> PARAMETROS { get; set; }
        public virtual DbSet<PARTICIPANTES> PARTICIPANTES { get; set; }
        public virtual DbSet<REFECHAS> REFECHAS { get; set; }
        public virtual DbSet<REUNION> REUNION { get; set; }
        public virtual DbSet<REUNION_ESTATUS> REUNION_ESTATUS { get; set; }
        public virtual DbSet<REUNION_TIPO> REUNION_TIPO { get; set; }
        public virtual DbSet<EMPLEADOS_TIPO> EMPLEADOS_TIPO { get; set; }
        public virtual DbSet<SERVICIOS> SERVICIOS { get; set; }
        public virtual DbSet<SERVICIOS_ESTADOS> SERVICIOS_ESTADOS { get; set; }
        public virtual DbSet<TAREAS> TAREAS { get; set; }
        public virtual DbSet<TAREAS_PERIODICAS> TAREAS_PERIODICAS { get; set; }
        public virtual DbSet<TIEMPO_STANDARD> TIEMPO_STANDARD { get; set; }
        public virtual DbSet<Dias> Dias { get; set; }
        public virtual DbSet<Periodos> Periodos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Periodos>()
            .Property(e => e.Descripcion)
            .IsUnicode(false);

            modelBuilder.Entity<Periodos>()
              .Property(e => e.CreadoPor)
              .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
              .Property(e => e.nombre_depto)
              .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
                .Property(e => e.noticias)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
                .Property(e => e.empleadoid)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
                .Property(e => e.iconoBoostrap)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
            .Property(e => e.AdministradorId1)
            .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
            .Property(e => e.AdministradorId2)
            .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
               .Property(e => e.AdministradorId3)
               .IsUnicode(false);

            modelBuilder.Entity<DEPARTAMENTO>()
               .HasMany(e => e.EMPLEADOS4)
               .WithRequired(e => e.DEPARTAMENTO4)
               .HasForeignKey(e => e.departamentoId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<DEPARTAMENTO>()
            .HasMany(e => e.REUNION)
            .WithRequired(e => e.DEPARTAMENTO)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<DEPARTAMENTO>()
            .HasMany(e => e.SERVICIOS1)
            .WithRequired(e => e.DEPARTAMENTO)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<DEPARTAMENTO>()
              .HasMany(e => e.TIEMPO_STANDARD)
              .WithRequired(e => e.DEPARTAMENTO)
              .WillCascadeOnDelete(false);

            //modelBuilder.Entity<DEPARTAMENTO>()
            //    .HasMany(e => e.EMPLEADOS1)
            //    .WithRequired(e => e.DEPARTAMENTO1)
            //    .HasForeignKey(e => e.departamentoId)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<MINUTA>()
           .Property(e => e.CreadoPor)
           .IsUnicode(false);

            modelBuilder.Entity<Periodos>()
              .Property(e => e.ModificadoPor)
              .IsUnicode(false);

            modelBuilder.Entity<Dias>()
             .Property(e => e.Dia)
             .IsUnicode(false);

            modelBuilder.Entity<Dias>()
             .Property(e => e.Nombre)
             .IsUnicode(false);

            modelBuilder.Entity<Dias>()
             .Property(e => e.Descripcion)
             .IsUnicode(false);

          
            modelBuilder.Entity<Periodos>()
                .HasMany(e => e.Dias)
                .WithRequired(e => e.Periodos)
                .HasForeignKey(e => e.PeriodoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Dias>()
                .HasMany(e => e.TAREAS_PERIODICAS)
                .WithRequired(e => e.Dias)
                .HasForeignKey(e => e.DiaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Periodos>()
               .HasMany(e => e.TAREAS_PERIODICAS)
               .WithRequired(e => e.Periodos)
               .HasForeignKey(e => e.PeriodoId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<DEPARTAMENTO>()
                .HasMany(e => e.TIEMPO_STANDARD)
                .WithRequired(e => e.DEPARTAMENTO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.empleadoId)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.PCid)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.desktop)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .Property(e => e.notas)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
            .Property(e => e.PasswordHash)
            .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.DEPARTAMENTO)
                .WithOptional(e => e.EMPLEADOS)
                .HasForeignKey(e => e.empleadoid);

            modelBuilder.Entity<EMPLEADOS>()
            .HasMany(e => e.DEPARTAMENTO1)
            .WithOptional(e => e.EMPLEADOS1)
            .HasForeignKey(e => e.AdministradorId1);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.DEPARTAMENTO2)
                .WithOptional(e => e.EMPLEADOS2)
                .HasForeignKey(e => e.AdministradorId1);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.DEPARTAMENTO3)
                .WithOptional(e => e.EMPLEADOS3)
                .HasForeignKey(e => e.AdministradorId2);

            modelBuilder.Entity<EMPLEADOS>()
               .HasMany(e => e.MINUTA)
               .WithOptional(e => e.EMPLEADO)
               .HasForeignKey(e => e.usuario_abrio)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.PARTICIPANTES)
                .WithRequired(e => e.EMPLEADOS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.REUNION)
                .WithRequired(e => e.EMPLEADOS)
                .HasForeignKey(e => e.usuario1)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.REUNION1)
                .WithOptional(e => e.EMPLEADOS1)
                .HasForeignKey(e => e.usuario2);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.REUNION2)
                .WithOptional(e => e.EMPLEADOS2)
                .HasForeignKey(e => e.usuario3);


            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.SERVICIOS)
                .WithRequired(e => e.EMPLEADOS)
                .HasForeignKey(e => e.solicitanteid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
              .HasMany(e => e.SERVICIOS1)
              .WithRequired(e => e.EMPLEADOS1)
              .HasForeignKey(e => e.asignadoid)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
              .HasMany(e => e.TAREAS)
              .WithRequired(e => e.EMPLEADOS)
              .HasForeignKey(e => e.empleadoId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
              .HasMany(e => e.TAREAS_PERIODICAS)
              .WithRequired(e => e.EMPLEADOS)
              .HasForeignKey(e => e.empleadoId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<EMPLEADOS>()
                .HasMany(e => e.TIEMPO_STANDARD)
                .WithRequired(e => e.EMPLEADOS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MINUTA>()
                .Property(e => e.agenda)
                .IsUnicode(false);

            modelBuilder.Entity<MINUTA>()
                .Property(e => e.puntos_pendientes)
                .IsUnicode(false);

            modelBuilder.Entity<MINUTA>()
                .Property(e => e.usuario_abrio)
                .IsUnicode(false);

            modelBuilder.Entity<MINUTA>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<MINUTA>()
                .HasMany(e => e.PARTICIPANTES)
                .WithRequired(e => e.MINUTA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.formato_fecha)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.nombre_empresa)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.servidor_email)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.email_operador)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.formato_fecha_display)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.usuario_email)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.clave_email)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETROS>()
                .Property(e => e.Version)
                .IsUnicode(false);

            modelBuilder.Entity<PARTICIPANTES>()
                .Property(e => e.empleadoId)
                .IsUnicode(false);

            modelBuilder.Entity<PARTICIPANTES>()
                .Property(e => e.nombre_empleado)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
                .Property(e => e.nombre_reunion)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
                .Property(e => e.usuario1)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
                .Property(e => e.usuario2)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
                .Property(e => e.usuario3)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION>()
              .HasMany(e => e.MINUTA)
              .WithRequired(e => e.REUNION)
              .HasForeignKey(e => e.reunionId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<REUNION_ESTATUS>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION_ESTATUS>()
                .HasMany(e => e.REUNION)
                .WithRequired(e => e.REUNION_ESTATUS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<REUNION>()
             .HasMany(e => e.TAREAS_PERIODICAS)
             .WithRequired(e => e.REUNION)
             .HasForeignKey(e => e.reunionId)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<REUNION_TIPO>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLEADOS_TIPO>()
              .Property(e => e.Nombre)
              .IsUnicode(false);

            modelBuilder.Entity<REUNION_TIPO>()
                .Property(e => e.iconoBoostrap)
                .IsUnicode(false);

            modelBuilder.Entity<REUNION_TIPO>()
                .HasMany(e => e.REUNION)
                .WithRequired(e => e.REUNION_TIPO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SERVICIOS>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS>()
                .Property(e => e.pcid)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS>()
                .Property(e => e.solicitanteid)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS>()
                .Property(e => e.solucion)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS>()
                .Property(e => e.asignadoid)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS_ESTADOS>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<SERVICIOS_ESTADOS>()
                .HasMany(e => e.SERVICIOS)
                .WithRequired(e => e.SERVICIOS_ESTADOS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TIEMPO_STANDARD>()
               .HasMany(e => e.SERVICIOS)
               .WithRequired(e => e.TIEMPO_STANDARD)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<TAREAS>()
                .Property(e => e.tarea)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
            .Property(e => e.hora_fin)
            .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
            .Property(e => e.hora_ini)
            .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
                .Property(e => e.solicitante)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
                .Property(e => e.empleadoId)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
                .Property(e => e.grupo)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
             .Property(e => e.Notas)
             .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
                .Property(e => e.subgrupo)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS>()
                .HasMany(e => e.REFECHAS)
                .WithRequired(e => e.TAREAS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TAREAS_PERIODICAS>()
                .Property(e => e.tarea)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS_PERIODICAS>()
                .Property(e => e.empleadoId)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS_PERIODICAS>()
                .Property(e => e.CreadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<TAREAS_PERIODICAS>()
                .Property(e => e.ModificadoPor)
                .IsUnicode(false);

            modelBuilder.Entity<TIEMPO_STANDARD>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TIEMPO_STANDARD>()
                .Property(e => e.empleadoid)
                .IsUnicode(false);
        }


    }
}
