﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct ServiciosEstados
    {
        public const int Cerrada = 4;
        public const int Pendiente = 5;
        public const int Completada = 3;
    }
}
