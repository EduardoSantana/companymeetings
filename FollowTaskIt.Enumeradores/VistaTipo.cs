﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{

    public struct VistaTipo
    {
        public const int Minuta = 1;
        public const int Reunir = 2;
    }
}
