﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct TareaEstatus
    {
        public const int SinFecha = 1;
        public const int Vencida = 2;
        public const int NoVencida = 3;
        public const int Realizada = 4;
    }
}
