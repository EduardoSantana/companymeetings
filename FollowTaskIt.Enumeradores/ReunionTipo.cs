﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct ReunionTipo
    {
        public const int Reunion = 1;
        public const int Proyecto = 2;
        public const int Otros = 3;
        public const int Servicio = 4;
        public const int Plantilla = 5;
    }
}
