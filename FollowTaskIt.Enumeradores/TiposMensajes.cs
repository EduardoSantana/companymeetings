﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct TipoMensaje
    {
        public const string Informacion = "info";
        public const string Correcto = "ok";
        public const string Error = "error";
        public const string Advertencia = "warning";
    }
}
