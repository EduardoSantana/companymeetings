﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct MinutaEstatus
    {
        public const int Estado0 = 0;
        public const int Estado1 = 1;
        public const int Estado2 = 2;
    }
}
