﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct TipoEmpleado
    {
        public const int Empleado = 1;
        public const int Invitado = 2;
    }
}
