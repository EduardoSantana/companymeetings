﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct RutaArchivoCorreo
    {
        public const int Solictud = 1;
        public const int Cerrada = 2;
        public const int Completada = 3;
    }
}
