﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct ReunionEstatus
    {
        public const int Activo = 0;
        public const int Inactivo = 1;
        public const int NoAprobado = 2;
    }
}
