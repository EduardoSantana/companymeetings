﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Enumeradores
{
    public struct PeriodoTipo
    {

        public const int DIARIO = 1;
        public const int MENSUAL = 2;
        public const int SEMANAL = 4;
        public const int BISEMANAL = 5;
        public const int BIMESTRAL = 6;
        public const int TRIMESTRAL = 7;
        public const int CUATRIMESTRAL = 8;
        public const int SEMESTRAL = 9;
        public const int ANUAL = 10;

    }
}
