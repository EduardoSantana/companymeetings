﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class CreacionEmpleadosViewModel 
    {
        public EMPLEADOS empleado { get; set; }
        public int minutaId { get; set; }
        public int Asistencia { get; set; }
        public bool esParticipante { get; set; }
    }
}
