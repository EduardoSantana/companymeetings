﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoEmpleadosViewModel
    {
        public IEnumerable<EMPLEADOS> Listado { get; set; }
        public int TipoId { get; set; }
        public bool Activo { get; set; }
    }
}
