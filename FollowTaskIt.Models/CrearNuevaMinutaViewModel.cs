﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class CrearNuevaMinutaViewModel
    {
        public bool Confirmar { get; set; }
        public bool esPopUp { get; set; }
        public string NombreReunion { get; set; }
        public bool preguntar { get; set; }
        [Key]
        [Required]
        public int MinutaId { get; set; }
        [DisplayName("Nueva Fecha")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime NuevaFecha { get; set; }
    }
}
