﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ReasignacionConfirmarViewModel
    {
        //ReasignarTareas(urlBase, datosArray, $('#minutaIdSeleccion').val(), $('#empleadoIdHasta').val());
        public int minutaId { get; set; }
        public string datosArray { get; set; }
        public string empleadoId { get; set; }
        public string nombreEmpleado { get; set; }
        public bool ConRefechas { get; set; }
    }
}
