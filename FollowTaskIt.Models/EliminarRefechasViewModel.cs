﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class EliminarRefechasViewModel
    {
        public int tareaRecId { get; set; }
        public bool Confirmar { get; set; }
        public string Tarea { get; set; }
        public string Reunion { get; set; }
        public string Minuta { get; set; }

    }
}
