﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoDepartamentosViewModel
    {
        public IEnumerable<DEPARTAMENTO> Listado { get; set; }
        public bool Activo { get; set; }
    }
}
