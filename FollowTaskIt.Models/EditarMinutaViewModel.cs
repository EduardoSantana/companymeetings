﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FollowTaskIt.AccesoBaseDatosV2;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FollowTaskIt.Models
{
    public class EditarMinutaViewModel
    {
        public REUNION reunion { get; set; }
        public MINUTA minuta { get; set; }
        [Required]
        [DisplayName("Solicitante")]
        [Compare("empleadoId", ErrorMessage = "No pueden ser iguales.")]
        public string solicitante { get; set; }
        //[CompareAttribute("solicitante", ErrorMessage = "No pueden ser iguales.")]
        [Required]
        [Compare("solicitante", ErrorMessage = "No pueden ser iguales.")]
        [DisplayName("Asignado a")]
        public string empleadoId { get; set; }
        [DataType(DataType.MultilineText)]
        [Required]
        public string textoTarea { get; set; }
        public SoloLectura soloLectura { get; set; }
        public int vistaTipo { get; set; }
        public string currentUserId { get; set; }
        public int minutos { get; set; }
        [DisplayName("Fecha Final")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? fechaVence { get; set; }
        public bool mostrarPopUp { get; set; }
    }
    public class SoloLectura { public bool soloLectura { get; set; } public bool agregaTareas { get; set; } }
}
