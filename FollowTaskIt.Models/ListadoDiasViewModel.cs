﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoDiasViewModel
    {
        public IEnumerable<Dias> listado { get; set; }
        public int periodoId { get; set; }
    }
}
