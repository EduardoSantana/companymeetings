﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoServiciosViewModel
    {
        public IEnumerable<SERVICIOS> listado { get; set; }
        [DisplayName("Estado")]
        public int estadoId { get; set; }
        public string usuarioLogeado { get; set; }
    }
}
