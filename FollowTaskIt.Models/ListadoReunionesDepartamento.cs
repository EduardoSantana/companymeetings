﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoReunionesDepartamento 
    {
        public IEnumerable<objReuniones> Reuniones { get; set; }
        //public IEnumerable<objMinuta> MinutasDeReunion(int _minutaId)
        //{
        //    return this.Minutas.Where(p => p.minutaId == _minutaId);
        //}
        //public IEnumerable<objMinuta> Minutas { get; set; }
    }
    public class objReuniones
    {
        public int contador { get; set; }
        public int NuevaMinutaId { get; set; }
        public int departamentoId { get; set; }
        public int reunionId { get; set; }
        public string nombre_reunion { get; set; }
        public string usuario1 { get; set; }
        public string usuario2 { get; set; }
        public string usuario3 { get; set; }
        public int estatusId { get; set; }
        public int tipoId { get; set; }
        public string iconoBoostrap { get; set; }
        public IEnumerable<objMinuta> Minutas { get; set; }
       
    }
    public class objMinuta 
    {
        public int departamentoId { get; set; }
        public int tipoId { get; set; }
        public int minutaId { get; set; }
        public string nombre { get; set; }
        public int reunionId { get; set; }
    }
}
