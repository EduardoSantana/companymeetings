﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class EditarContadorViewModel
    {
        [Key]
        public int id { get; set; }
        public string Titulo { get; set; }
        public int Minutos { get; set; }
    }
}
