﻿using FollowTaskIt.AccesoBaseDatosV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ListadoTiempoStandardViewModel
    {
        public IEnumerable<TIEMPO_STANDARD> listado { get; set; }
        public int departamentoId { get; set; }
    }
}
