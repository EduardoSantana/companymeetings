﻿using FollowTaskIt.AccesoBaseDatosV2;
using FollowTaskIt.Enumeradores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FollowTaskIt.Models
{
    public class ViewModelBase
    {
        private ModelFollowTaskIt db = new ModelFollowTaskIt();
        //private Entities db = new Entities();
        private string empleadoId { get; set; }
    
        public List<DEPARTAMENTO> listaDepartamento
        {
            get 
            {
                return db.DEPARTAMENTO.Where(p => p.Activo == true).ToList();
            }
        }
        public int cantidadServicios(string currentUserId)
        {
            if (String.IsNullOrEmpty(empleadoId))
            {
                empleadoId = db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.empleadoId).FirstOrDefault();
            }
            return db.SERVICIOS.Where(p => p.asignadoid == empleadoId && ServiciosEstados.Pendiente == p.estadoId).Count();
            
        }
        public int cantidadServiciosConsulta(string currentUserId)
        {
            if (String.IsNullOrEmpty(empleadoId))
            {
                empleadoId = db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.empleadoId).FirstOrDefault();
            }
            return db.SERVICIOS.Where(p => p.solicitanteid == empleadoId && ServiciosEstados.Pendiente == p.estadoId).Count();

        }
        public int cantidadServiciosCompletados(string currentUserId)
        {
            if (String.IsNullOrEmpty(empleadoId))
            {
                empleadoId = db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.empleadoId).FirstOrDefault();
            }
            return db.SERVICIOS.Where(p => p.solicitanteid == empleadoId && ServiciosEstados.Completada == p.estadoId).Count();

        }
        public bool accesoMantenimiento(string currentUserId)
        {
            return (db.EMPLEADOS.Where(p => p.usuario == currentUserId).Select(p => p.accesomant).FirstOrDefault());
        }
    }

}
