﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FollowTaskIt.AccesoBaseDatosV2;

namespace FollowTaskIt.Models
{
    public class ListadoReunionesViewModel
    {
        public IEnumerable<REUNION> listado { get; set; }
        [DisplayName("Estado")]
        public int estatusId { get; set; }
    }
}
